package com.labs247.telyu.bean.mapreduce2;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;

import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.jboss.logging.Logger;

import com.labs247.telyu.bean.mapreduce2.MAPREDUCERest;
import com.labs247.telyu.bean.mapreduce2.MAPREDUCEUtil;
import com.labs247.telyu.model.mapreduce2.Configuration;
import com.labs247.telyu.model.mapreduce2.Item;
import com.labs247.telyu.model.mapreduce2.Mapreduce;
import com.labs247.telyu.model.mapreduce2.Properties;

@Singleton
@Startup
public class MapreduceVIEW {

	public enum States {
		BEFORESTARTED, STARTED, PAUSED, SHUTTINGDOWN
	};

	private States state;
	private Configuration configuration;

	static String host = "192.168.3.132";
	static String port = "8080";
	static Mapreduce mapreduce = null;
	static long fire_time = System.currentTimeMillis();
	static Logger logger = Logger.getLogger(MapreduceVIEW.class);

	public MapreduceVIEW() {

	}

	@PostConstruct
	public void initialize() {
		logger.info("............................................................");
		System.out.println("............................................................");
		state = States.BEFORESTARTED;
		// Perform intialization
		get_de_data();
		state = States.STARTED;
	}

	// fire every 5 minut

	// @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
	@Schedule(minute = "*/5", hour = "*", persistent = false)
	public void atSchedule() throws InterruptedException {

		get_de_data();

		System.out.println("DeclarativeScheduler:: In atSchedule() fired get_jmxdata()");

	}

	public void get_de_data() {

		try {
			System.out.println(".......................Rest Dimulai ..................................");
			MAPREDUCERest _client = new MAPREDUCERest(host, port);
			String URL = "http://192.168.3.132:8080/api/v1/clusters/C10H16/configurations/service_config_versions/?service_name=MAPREDUCE2";

			fire_time = System.currentTimeMillis();
			String json = _client.ambari_configurtion_rest_client(URL);

			mapreduce = MAPREDUCEUtil.json2DataNode(json);

			System.out.println("..........................MAPREDUCE Data : ...................");
//			System.out.println(getPropertiesData());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@PreDestroy
	public void terminate() {
		state = States.SHUTTINGDOWN;
		// Perform termination
		System.out.println("JMXRestClient shutdown in progress");
	}

	public States getState() {
		return state;
	}

	public void setState(States state) {
		this.state = state;
	}

	public long getFiretime() {
		return fire_time;
	}

	public String getHref() {
		return mapreduce.getHref();
	}

	public Item getItemData() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = mapreduce.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);
		return _de_bean;
	}

	public Configuration getConfigMapreduce() {

		Item _de_bean = mapreduce.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream().filter(config -> "mapred-env".equals(config.getType()))
				.findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesData() {
		Item _de_bean = mapreduce.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream().filter(config -> "mapred-env".equals(config.getType()))
				.findAny().orElse(null);

		return configuration.getProperties();
	}

}