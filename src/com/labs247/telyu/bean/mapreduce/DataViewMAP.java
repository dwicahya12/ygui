package com.labs247.telyu.bean.mapreduce;

import com.labs247.telyu.bean.mapreduce.Client;

import java.io.Serializable;

import javax.annotation.PostConstruct;
//import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class DataViewMAP implements Serializable{
	
	String jobsize, mapred, history, logDir, pid, nofile, nproc, content, mapchild,reducechild, adminuser, max,
	classpath, framework, acls, admin, fname, modifyjob, viewjob, counter, timeline, queue, slow, acl, bindhost,
	done, policy, inter, recovery, store, enable, jobre, webapp, javaopts, loglevel, outcom, spill, speculative, compress,
	comtype, buffer, javasize, reenable, rein, retime, bpercent, mpercent, shuffel, spec, port, sort, timeout, command,
	appcommand, amlog, amstaging;
	
//	@EJB Client c;
	Client c = new Client();
	
	 @PostConstruct
	    public void init() {
		c.get_de_data(); 
		mapred = c.getMapredEnv().getMapredUserNprocLimit();
		jobsize = c.getMapredEnv().getJobhistoryHeapsize();
		logDir = c.getMapredEnv().getMapredLogDirPrefix();
		pid = c.getMapredEnv().getMapredPidDirPrefix();
		nofile = c.getMapredEnv().getMapredUserNofileLimit();
		nproc = c.getMapredEnv().getMapredUserNprocLimit();
		content = c.getMapredEnv().getContent();
		
		history = c.getMapredSite().getMapreduceJobhistoryAddress();
		mapchild = c.getMapredSite().getMapreduceAdminMapChildJavaOpts();
		reducechild = c.getMapredSite().getMapreduceAdminReduceChildJavaOpts();
		adminuser = c.getMapredSite().getMapreduceAdminUserEnv();
		max = c.getMapredSite().getMapreduceAmMaxAttempts();
		classpath = c.getMapredSite().getMapreduceApplicationClasspath();
		framework = c.getMapredSite().getMapreduceApplicationFrameworkPath();
		acls = c.getMapredSite().getMapreduceClusterAclsEnabled();
		admin = c.getMapredSite().getMapreduceClusterAdministrators();
		fname = c.getMapredSite().getMapreduceFrameworkName();
		modifyjob = c.getMapredSite().getMapreduceJobAclModifyJob();
		viewjob = c.getMapredSite().getMapreduceJobAclViewJob();
		counter = c.getMapredSite().getMapreduceJobCountersMax();
		timeline = c.getMapredSite().getMapreduceJobEmitTimelineData();
		queue = c.getMapredSite().getMapreduceJobQueuename();
		slow = c.getMapredSite().getMapreduceJobReduceSlowstartCompletedmaps();
		acl = c.getMapredEnv().getMapreduceJobhistoryAdminAcl();
		bindhost = c.getMapredSite().getMapreduceJobhistoryBindHost();
		done = c.getMapredSite().getMapreduceJobhistoryDoneDir();
		policy = c.getMapredSite().getMapreduceJobhistoryHttpPolicy();
		inter = c.getMapredSite().getMapreduceJobhistoryIntermediateDoneDir();
		recovery = c.getMapredSite().getMapreduceJobhistoryRecoveryEnable();
		store = c.getMapredSite().getMapreduceJobhistoryRecoveryStoreClass();
		enable = c.getMapredSite().getMapreduceJobhistoryRecoveryEnable();
		jobre = c.getMapredSite().getMapreduceJobhistoryRecoveryStoreLeveldbPath();
		webapp = c.getMapredSite().getMapreduceJobhistoryWebappAddress();
		javaopts = c.getMapredSite().getMapreduceMapJavaOpts();
		loglevel = c.getMapredSite().getMapreduceMapLogLevel();
		outcom = c.getMapredSite().getMapreduceMapOutputCompress();
		spill = c.getMapredSite().getMapreduceMapSortSpillPercent();
		speculative = c.getMapredSite().getMapreduceMapSpeculative();
		compress = c.getMapredSite().getMapreduceOutputFileoutputformatCompress();
		comtype = c.getMapredSite().getMapreduceOutputFileoutputformatCompressType();
		buffer = c.getMapredSite().getMapreduceReduceInputBufferPercent();
		javasize = c.getMapredSite().getMapreduceReduceJavaOpts();
		reenable = c.getMapredSite().getMapreduceReduceShuffleFetchRetryEnabled();
		rein = c.getMapredSite().getMapreduceReduceShuffleFetchRetryIntervalMs();
		retime = c.getMapredSite().getMapreduceReduceShuffleFetchRetryTimeoutMs();
		bpercent = c.getMapredSite().getMapreduceReduceShuffleInputBufferPercent();
		mpercent = c.getMapredSite().getMapreduceReduceShuffleMergePercent();
		shuffel = c.getMapredSite().getMapreduceReduceShuffleParallelcopies();
		spec = c.getMapredSite().getMapreduceReduceSpeculative();
		port = c.getMapredSite().getMapreduceShufflePort();
		sort = c.getMapredSite().getMapreduceTaskIoSortFactor();
		timeout = c.getMapredSite().getMapreduceTaskTimeout();
		command = c.getMapredSite().getYarnAppMapreduceAmAdminCommandOpts();
		appcommand = c.getMapredSite().getYarnAppMapreduceAmCommandOpts();
		amlog = c.getMapredSite().getYarnAppMapreduceAmLogLevel();
		amstaging = c.getMapredSite().getYarnAppMapreduceAmStagingDir();
	 }
	 
	 public String getJobSize() {
	        return jobsize;
	    }
	 
	 public String getMapred() {
	        return mapred;
	    }
	 
	 public String getHistory() {
	        return history;
	    }
	 
	 public String getLogDir() {
	        return logDir;
	    }
	 
	 public String getPid() {
	        return pid;
	    }
	 
	 public String getNoFile() {
	        return nofile;
	    }
	 
	 public String getNproc() {
	        return nproc;
	    }
	 
	 public String getContent() {
	        return content;
	    }
	 
	 public String getMapChild() {
	        return mapchild;
	    }
	 
	 public String getReduceChild() {
	        return reducechild;
	    }
	 
	 public String getAdminUser() {
	        return adminuser;
	    }
	 
	 public String getMax() {
	        return max;
	    }
	 
	 public String getClasspath() {
	        return classpath;
	    }
	 
	 public String getFramework() {
	        return framework;
	    }
	 
	 public String getACLS() {
	        return acls;
	    }
	 
	 public String getAdmin() {
	        return admin;
	    }
	 
	 public String getFname() {
	        return fname;
	    }
	 
	 public String getModifyJob() {
	        return modifyjob;
	    }
	 
	 public String getViewJob() {
	        return viewjob;
	    }
	 
	 public String getCounter() {
	        return counter;
	    }
	 
	 public String getTimeline() {
	        return timeline;
	    }
	 
	 public String getQueue() {
	        return queue;
	    }
	 
	 public String getSlow() {
	        return slow;
	    }
	 
	 public String getAcl() {
	        return acl;
	    }
	 
	 public String getBindHost() {
	        return bindhost;
	    }
	 
	 public String getDone() {
	        return done;
	    }
	 
	 public String getPolicy() {
	        return policy;
	    }
	 
	 public String getInter() {
	        return inter;
	    }
	 
	 public String getRecovery() {
	        return recovery;
	    }
	 
	 public String getStore() {
	        return store;
	    }
	 
	 public String getEnable() {
	        return enable;
	    }
	 
	 public String getJobRe() {
	        return jobre;
	    }
	 
	 public String getWebapp() {
	        return webapp;
	    }
	 
	 public String getJavaOpts() {
	        return javaopts;
	    }
	 
	 public String getLogLevel() {
	        return loglevel;
	    }
	 
	 public String getOutcom() {
	        return outcom;
	    }
	 
	 public String getSpill() {
	        return spill;
	    }
	 
	 public String getSpeculative() {
	        return speculative;
	    }
	 
	 public String getCompress() {
	        return compress;
	    }
	 
	 public String getComType() {
	        return comtype;
	    }
	 
	 public String getBuffer() {
	        return buffer;
	    }
	 
	 public String getJavaSize() {
	        return javasize;
	    }
	 
	 public String getReEnable() {
	        return reenable;
	    }
	 
	 public String getReIn() {
	        return rein;
	    }
	 
	 public String getReTime() {
	        return retime;
	    }
	 
	 public String getBPercent() {
	        return bpercent;
	    }
	 
	 public String getMPercent() {
	        return mpercent;
	    }
	 
	 public String getShuffel() {
	        return shuffel;
	    }
	 
	 public String getSpec() {
	        return spec;
	    }
	 
	 public String getPort() {
	        return port;
	    }
	 
	 public String getSort() {
	        return sort;
	    }
	 
	 public String getTimeout() {
	        return timeout;
	    }
	 
	 public String getCommand() {
	        return command;
	    }
	 
	 public String getAppCommand() {
	        return appcommand;
	    }
	 
	 public String getAmLog() {
	        return amlog;
	    }
	 
	 public String getAmStaging() {
	        return amstaging;
	    }
}
