package com.labs247.telyu.bean.mapreduce;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.labs247.telyu.model.mapreduce.Mapreduce;

public class Parsing {
	public static Mapreduce json2DataNode(String json) {

		JsonParser parser = new JsonParser();

		Mapreduce _datanode = null;

		JsonElement jsonTree = parser.parse(json);

		if (jsonTree.isJsonObject()) {

			JsonObject jsonObject = jsonTree.getAsJsonObject();

			Gson gson = new Gson();

			_datanode = gson.fromJson(jsonObject, Mapreduce.class);

		}

		return _datanode;
	}

}
