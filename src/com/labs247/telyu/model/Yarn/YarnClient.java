package com.labs247.telyu.model.Yarn;

//import com.verzinen.ugm.model.datanode.Datanode;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.jboss.logging.Logger;

import com.labs247.telyu.model.Yarn.Configuration;
import com.labs247.telyu.model.Yarn.Item;
import com.labs247.telyu.model.Yarn.Properties;
import com.labs247.telyu.model.Yarn.Yarn;

 
@Singleton
@Startup
public class YarnClient {
	
	public enum States {BEFORESTARTED, STARTED, PAUSED, SHUTTINGDOWN};
	    
	private States state;
	private Configuration configuration;
	
	static String host = "192.168.3.132";
	static String port = "8080";
	static Yarn yarn = null;
	static long fire_time = System.currentTimeMillis();
//	static Logger logger = Logger.getLogger(KafkaClient.class);
	static Logger logger = Logger.getLogger(YarnClient.class);
	
	public YarnClient() {
		
	}
	
	@PostConstruct
	public void initialize() {
		logger.info(".......................Rest Dimulai ..................................");
		System.out.println(".......................Rest Dimulai ..................................");
		state = States.BEFORESTARTED;
		
		get_de_data();
		state = States.STARTED;
	}

	
	//@Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
	@Schedule(minute = "*/5", hour = "*", persistent = false)
    public void atSchedule() throws InterruptedException {
		
		get_de_data();
		
        System.out.println("DeclarativeScheduler:: In atSchedule() fired get_de_data()");
        
    }
	
	

	public void get_de_data(){

		try {
			System.out.println(".......................Rest Dimulai ..................................");
			YarnRestClient _client = new YarnRestClient(host, port);
			String URL = "http://192.168.3.132:8080/api/v1/clusters/C10H16/configurations/service_config_versions/?service_name=YARN";

			fire_time = System.currentTimeMillis();
			String json = _client.basic_auth_client(URL);

			yarn = JsonParsingUtil.json2DataNode(json);
			
			System.out.println("..........................Yarn Data : ...................");

			System.out.println(getPropertiesData());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@PreDestroy
	public void terminate() {
		state = States.SHUTTINGDOWN;
		// Perform termination
		System.out.println("JMXRestClient shutdown in progress");
	}

	public States getState() {
		return state;
	}

	public void setState(States state) {
		this.state = state;
	}

	public long getFiretime() {
		return fire_time;
	}

	public String getHref() {
		return yarn.getHref();
	}

	public Item getItemData() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);
		return _de_bean;
	}

	public Configuration getConfigurationKafkaBroker() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "container-executor".equals(configurationku.getType())).findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesData() {
		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "container-executor".equals(configurationku.getType())).findAny().orElse(null);
		
		return configuration.getProperties();
	}
	
	public Configuration getConfigurationYarnEnv() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-env".equals(configurationku.getType())).findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesYarnEnv() {
		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-env".equals(configurationku.getType())).findAny().orElse(null);
		
		return configuration.getProperties();
	}
	
//	Container Executor
	
	public Configuration getConfigurationContainerExecutor() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "container-executor".equals(configurationku.getType())).findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesContainerExecutor() {
		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "container-executor".equals(configurationku.getType())).findAny().orElse(null);
		
		return configuration.getProperties();
	}

//Yarn Hbase-env
	
	public Configuration getConfigurationYarnHbaseEnv() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-hbase-env".equals(configurationku.getType())).findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesYarnHbaseEnv() {
		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-hbase-env".equals(configurationku.getType())).findAny().orElse(null);
		
		return configuration.getProperties();
	}
	
//Yarn Hbase-log4j
	public Configuration getConfigurationYarnHbaselog4j() {

		// i.e java.lang:type=OperatingSystem

		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-hbase-log4j".equals(configurationku.getType())).findAny().orElse(null);
		return configuration;
	}

	public Properties getPropertiesYarnHbaselog4j() {
		Item _de_bean = yarn.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
				.orElse(null);

		configuration = _de_bean.getConfigurations().stream()
				.filter(configurationku -> "yarn-hbase-log4j".equals(configurationku.getType())).findAny().orElse(null);
		
		return configuration.getProperties();
	}
}