package com.labs247.telyu.model.Yarn.yarnBlauwe;

import java.util.ArrayList;
import java.util.List;

public class PojoConfBlauwe{

    private String href;
    private List<ItemBlauwe> items = new ArrayList<ItemBlauwe>();

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<ItemBlauwe> getItems() {
        return items;
    }

    public void setItems(List<ItemBlauwe> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(PojoConfBlauwe.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("href");
        sb.append('=');
        sb.append(((this.href == null)?"<null>":this.href));
        sb.append(',');
        sb.append("items");
        sb.append('=');
        sb.append(((this.items == null)?"<null>":this.items));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.items == null)? 0 :this.items.hashCode()));
        result = ((result* 31)+((this.href == null)? 0 :this.href.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PojoConfBlauwe) == false) {
            return false;
        }
        PojoConfBlauwe rhs = ((PojoConfBlauwe) other);
        return (((this.items == rhs.items)||((this.items!= null)&&this.items.equals(rhs.items)))&&((this.href == rhs.href)||((this.href!= null)&&this.href.equals(rhs.href))));
    }

}
