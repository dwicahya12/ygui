package com.labs247.telyu.model.Yarn;

import com.labs247.telyu.model.Yarn.YarnClient;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.ejb.EJB;
import javax.ejb.Stateful;

@Stateful
@Named
public class cobaYa {
	
	String MinimumUser;
	
	@EJB
	YarnClient data;

	@PostConstruct
	public void init() {
		data.get_de_data();
		MinimumUser = data.getPropertiesContainerExecutor().getMinUserId();

	}

	public String getMinimumUser() {
		return MinimumUser;
	}

}
