package com.labs247.telyu.model.Yarn;

import com.labs247.telyu.model.Yarn.YarnClient;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class DataView implements Serializable {
	
	String type, tag, version, supported, check, heapsize, registry, yarnPid, yarnUser, userID, yarnHeapsize, yarnAts,
			nodemanager, yarnLog, yarnpid, nofileLimit, nprocLimit;
	String docker_privileged, cGroup, DockerDevices, DockerReadOnly, DockerReadWriter, DockerVolumeDrivers,
			DockerBinary, MinimumUser, YarnCGroup;
	String hbaseTmpdir, hbaseSystemServiceLaunch, hbaseContainers, hbaseCPU, hbaseClientMemory, hbaseHeap,
			hbaseMasterMemory, hBasePIDDir, hbaseRegionserverContainers, hbaseRegionserverCpu, hbaseRegionserverMemory,
			hbaseSSLaunchMode, hbaseSSQueueName, hbaseEnvContent, hbaseMasterContainers, hbaseMasterCpu;
	String hBaseLogBfiles, hBaseLogBfs, hBaseSecurityLogBfiles,hBaseSecurityLogBfs, contentHbaselog4j;
	YarnClient a = new YarnClient();

	@PostConstruct
	public void init() {
		a.get_de_data();
		type = a.getConfigurationYarnEnv().getType();
		tag = a.getConfigurationYarnEnv().getTag();
		supported = a.getPropertiesYarnEnv().getIsSupportedYarnRanger();
		check = a.getPropertiesYarnEnv().getServiceCheckQueueName();
		yarnLog = a.getPropertiesYarnEnv().getYarnLogDirPrefix();
		yarnPid = a.getPropertiesYarnEnv().getYarnPidDirPrefix();
		nofileLimit = a.getPropertiesYarnEnv().getYarnUserNofileLimit();
		nprocLimit = a.getPropertiesYarnEnv().getYarnUserNprocLimit();

		docker_privileged = a.getPropertiesData().getDockerPrivilegedContainersEnabled();
		cGroup = a.getPropertiesContainerExecutor().getCgroupRoot();
		DockerDevices = a.getPropertiesContainerExecutor().getDockerAllowedDevices();
		DockerReadOnly = a.getPropertiesContainerExecutor().getDockerAllowedRoMounts();
		DockerReadWriter = a.getPropertiesContainerExecutor().getDockerAllowedRwMounts();
		DockerVolumeDrivers = a.getPropertiesContainerExecutor().getDockerAllowedVolumeDrivers();
		DockerBinary = a.getPropertiesContainerExecutor().getDockerBinary();
		MinimumUser = a.getPropertiesContainerExecutor().getMinUserId();
		YarnCGroup = a.getPropertiesContainerExecutor().getYarnCgroupsEnabled();

		hbaseTmpdir = a.getPropertiesYarnHbaseEnv().getHbaseJavaIoTmpdir();
		hbaseSystemServiceLaunch = a.getPropertiesYarnHbaseEnv().getIsHbaseSystemServiceLaunch();
		hbaseContainers = a.getPropertiesYarnHbaseEnv().getYarnHbaseClientContainers();
		hbaseCPU = a.getPropertiesYarnHbaseEnv().getYarnHbaseClientCpu();
		hbaseClientMemory = a.getPropertiesYarnHbaseEnv().getYarnHbaseClientMemory();
		hbaseHeap = a.getPropertiesYarnHbaseEnv().getYarnHbaseHeapMemoryFactor();
		hbaseMasterContainers = a.getPropertiesYarnHbaseEnv().getYarnHbaseMasterContainers();
		hbaseMasterCpu = a.getPropertiesYarnHbaseEnv().getYarnHbaseMasterCpu();
		hbaseMasterMemory = a.getPropertiesYarnHbaseEnv().getYarnHbaseMasterMemory();
		hBasePIDDir = a.getPropertiesYarnHbaseEnv().getYarnHbasePidDirPrefix();
		hbaseRegionserverContainers = a.getPropertiesYarnHbaseEnv().getYarnHbaseRegionserverContainers();
		hbaseRegionserverCpu = a.getPropertiesYarnHbaseEnv().getYarnHbaseRegionserverCpu();
		hbaseRegionserverMemory = a.getPropertiesYarnHbaseEnv().getYarnHbaseRegionserverMemory();
		hbaseSSLaunchMode = a.getPropertiesYarnHbaseEnv().getYarnHbaseSystemServiceLaunchMode();
		hbaseSSQueueName = a.getPropertiesYarnHbaseEnv().getYarnHbaseSystemServiceQueueName();
		hbaseEnvContent = a.getPropertiesYarnHbaseEnv().getContent();

		hBaseLogBfiles = a.getPropertiesYarnHbaselog4j().getHbaseLogMaxbackupindex();
		hBaseLogBfs = a.getPropertiesYarnHbaselog4j().getHbaseLogMaxfilesize();
		hBaseSecurityLogBfiles = a.getPropertiesYarnHbaselog4j().getHbaseSecurityLogMaxbackupindex();
		hBaseSecurityLogBfs = a.getPropertiesYarnHbaselog4j().getHbaseSecurityLogMaxfilesize();
		contentHbaselog4j = a.getPropertiesYarnHbaselog4j().getContent();
	}

	public String getDocker() {
		return docker_privileged;
	}

	public String getType() {
		return type;
	}

	public String getTag() {
		return tag;
	}

	public String getSupported() {
		return supported;
	}

	public String getCheck() {
		return check;
	}

	public String getYarnLog() {
		return yarnLog;
	}

	public String getYarnPid() {
		return yarnPid;
	}

	public String getNofileLimit() {
		return nofileLimit;
	}

	public String getNprocLimit() {
		return nprocLimit;
	}

	public String getCGroup() {
		return cGroup;
	}

	public String getDockerDevices() {
		return DockerDevices;
	}

	public String getDockerReadOnly() {
		return DockerReadOnly;
	}

	public String getDockerReadWriter() {
		return DockerReadWriter;
	}

	public String getDockerVolumeDrivers() {
		return DockerVolumeDrivers;
	}

	public String getDockerBinary() {
		return DockerBinary;
	}

	public String getMinimumUser() {
		return MinimumUser;
	}

	public String getYarnCGroup() {
		return YarnCGroup;
	}

	public String getHbaseTmpdir() {
		return hbaseTmpdir;
	}

	public String getHbaseSystemServiceLaunch() {
		return hbaseSystemServiceLaunch;

	}

	public String getHbaseContainers() {
		return hbaseContainers;

	}

	public String getHbaseCPU() {
		return hbaseCPU;

	}

	public String getHbaseClientMemory() {
		return hbaseClientMemory;

	}

	public String getHbaseHeap() {
		return hbaseHeap;

	}

	public String getHbaseMasterMemory() {
		return hbaseMasterMemory;

	}

	public String getHBasePIDDir() {
		return hBasePIDDir;

	}

	public String getHbaseRegionserverContainers() {
		return hbaseRegionserverContainers;

	}

	public String getHbaseRegionserverCpu() {
		return hbaseRegionserverCpu;

	}

	public String getHbaseRegionserverMemory() {
		return hbaseRegionserverMemory;

	}

	public String getHbaseSSLaunchMode() {
		return hbaseSSLaunchMode;

	}

	public String getHbaseSSQueueName() {
		return hbaseSSQueueName;

	}

	public String gethbaseEnvContent() {
		return hbaseEnvContent;

	}

	public String getHbaseMasterContainers() {
		return hbaseMasterContainers;

	}

	public String getHbaseMasterCpu() {
		return hbaseMasterCpu;

	}
	
	public String getHBaseLogBfiles() {
		return hBaseLogBfiles;

	}
	
	public String getHBaseLogBfs() {
		return hBaseLogBfs;

	}
	
	public String getHBaseSecurityLogBfiles() {
		return hBaseSecurityLogBfiles;

	}
	
	public String getHBaseSecurityLogBfs() {
		return hBaseSecurityLogBfs;

	}
	
	public String getContentHbaselog4j() {
		return contentHbaselog4j;

	}
}