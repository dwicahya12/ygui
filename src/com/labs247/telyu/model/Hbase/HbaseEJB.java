package com.labs247.telyu.model.Hbase;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.inject.Named;

@Named
@Stateful
public class HbaseEJB {
	
	@EJB HbaseClient data;
	
	//hbase-env
	String hbaseUse,ratio,user,atlashook,heapsize, content,temp,nproc,pidir,shutdown,logdir,serverHeap,nofile,sql;
	//hbase-site
	String type, cluster,stackid,version,tag,masterClass,cache,mport,alowUser,local,query,wal,control,retries,hstore,
	quorum,infoPort,rootdir,Cluster,major,memstore,secAuth,Auth,executor,blocking,useMulti,socket,scheduler,session,
	bindAddress,readonly,jitter,flush,filesize,recovery,global,keyvalue,tmp,scanner,Threshold,wait,init,timeout,
	superuser,coprocessor,handler,staging,Regionport,property,skip,multiplier;
	//hbase-policy
	String master,admin,client;
	//hbase-atlas
	String keep,max,min,queue,num,synchronous;
	//hbase-log4j
	String secMax,maxFile,maxBack,secMaxBack,Content;
	
	@PostConstruct
	public void init() {
//		data.get_de_data();
		hbaseUse = data.getPropertiesData().getHbaseRegionserverXmnMax();
		ratio = data.getPropertiesData().getHbaseRegionserverXmnRatio();
		user = data.getPropertiesData().getHbaseUser();
		atlashook = data.getPropertiesData().getHbaseAtlasHook();
		heapsize = data.getPropertiesData().getHbaseMasterHeapsize();		
		content = data.getPropertiesData().getContent();
		temp = data.getPropertiesData().getHbaseJavaIoTmpdir();
		nproc = data.getPropertiesData().getHbaseUserNprocLimit();
		pidir = data.getPropertiesData().getHbasePidDir();
		shutdown = data.getPropertiesData().getHbaseRegionserverShutdownTimeout();
		logdir = data.getPropertiesData().getHbaseLogDir();
		serverHeap = data.getPropertiesData().getHbaseRegionserverHeapsize();
		nofile = data.getPropertiesData().getHbaseUserNofileLimit();
		sql = data.getPropertiesData().getPhoenixSqlEnabled();
		cluster = data.getItemData().getClusterName();
		stackid = data.getItemData().getStackId();
		
		version = Integer.toString(data.getConfigurationHbaseSite().getVersion());
		type= data.getConfigurationHbaseSite().getType();
		tag = data.getConfigurationHbaseSite().getTag();
		masterClass = data.getPropertiesHbaseSite().getHbaseCoprocessorMasterClasses();
		cache = data.getPropertiesHbaseSite().getHfileBlockCacheSize();
		mport = data.getPropertiesHbaseSite().getHbaseMasterPort();
		alowUser = data.getPropertiesHbaseSite().getPhoenixFunctionsAllowUserDefinedFunctions();
		local = data.getPropertiesHbaseSite().getHbaseLocalDir();
		query = data.getPropertiesHbaseSite().getPhoenixQueryTimeoutMs();
		wal = data.getPropertiesHbaseSite().getHbaseRegionserverWalCodec();
		control = data.getPropertiesHbaseSite().getHbaseRpcControllerfactoryClass();
		retries = data.getPropertiesHbaseSite().getHbaseClientRetriesNumber();
		hstore = data.getPropertiesHbaseSite().getHbaseHstoreCompactionMax();
		quorum = data.getPropertiesHbaseSite().getHbaseZookeeperQuorum();
		infoPort = data.getPropertiesHbaseSite().getHbaseMasterInfoPort();
		rootdir = data.getPropertiesHbaseSite().getHbaseRootdir();
		Cluster = data.getPropertiesHbaseSite().getHbaseClusterDistributed();
		major = data.getPropertiesHbaseSite().getHbaseHregionMajorcompaction();
		memstore = data.getPropertiesHbaseSite().getHbaseHregionMemstoreMslabEnabled();
		secAuth = data.getPropertiesHbaseSite().getHbaseSecurityAuthentication();
		Auth = data.getPropertiesHbaseSite().getHbaseSecurityAuthorization();
		executor = data.getPropertiesHbaseSite().getHbaseRegionserverExecutorOpenregionThreads();
		blocking = data.getPropertiesHbaseSite().getHbaseHstoreBlockingStoreFiles();
		useMulti = data.getPropertiesHbaseSite().getHbaseZookeeperUseMulti();
		socket = data.getPropertiesHbaseSite().getDfsDomainSocketPath();
		scheduler = data.getPropertiesHbaseSite().getHbaseRegionServerRpcSchedulerFactoryClass();
		session = data.getPropertiesHbaseSite().getZookeeperSessionTimeout();
		bindAddress = data.getPropertiesHbaseSite().getHbaseMasterInfoBindAddress();
		readonly = data.getPropertiesHbaseSite().getHbaseMasterUiReadonly();
		jitter = data.getPropertiesHbaseSite().getHbaseHregionMajorcompactionJitter();
		flush = data.getPropertiesHbaseSite().getHbaseHregionMemstoreFlushSize();
		filesize = data.getPropertiesHbaseSite().getHbaseHregionMaxFilesize();
		recovery = data.getPropertiesHbaseSite().getZookeeperRecoveryRetry();
		global = data.getPropertiesHbaseSite().getHbaseRegionserverGlobalMemstoreSize();
		keyvalue = data.getPropertiesHbaseSite().getHbaseClientKeyvalueMaxsize();
		tmp = data.getPropertiesHbaseSite().getHbaseTmpDir();
		scanner = data.getPropertiesHbaseSite().getHbaseClientScannerCaching();
		Threshold = data.getPropertiesHbaseSite().getHbaseHstoreCompactionThreshold();
		wait = data.getPropertiesHbaseSite().getHbaseMasterWaitOnRegionserversTimeout();
		init = data.getPropertiesHbaseSite().getHbaseMasterNamespaceInitTimeout();
		timeout = data.getPropertiesHbaseSite().getHbaseRpcTimeout();
		superuser = data.getPropertiesHbaseSite().getHbaseSuperuser();
		coprocessor = data.getPropertiesHbaseSite().getHbaseCoprocessorRegionClasses();
		handler = data.getPropertiesHbaseSite().getHbaseRegionserverHandlerCount();
		staging = data.getPropertiesHbaseSite().getHbaseBulkloadStagingDir();
		Regionport = data.getPropertiesHbaseSite().getHbaseRegionserverPort();
		property = data.getPropertiesHbaseSite().getHbaseZookeeperPropertyClientPort();
		skip = data.getPropertiesHbaseSite().getHbaseDefaultsForVersionSkip();
		multiplier = data.getPropertiesHbaseSite().getHbaseHregionMemstoreBlockMultiplier();
		
		master = data.getPropertiesHbasePolicy().getSecurityMasterregionProtocolAcl();
		admin = data.getPropertiesHbasePolicy().getSecurityAdminProtocolAcl();
		client = data.getPropertiesHbasePolicy().getSecurityClientProtocolAcl();
		
		keep = data.getPropertiesHbaseAtlas().getAtlasHookHbaseKeepAliveTime();
		max = data.getPropertiesHbaseAtlas().getAtlasHookHbaseMaxThreads();
		min = data.getPropertiesHbaseAtlas().getAtlasHookHbaseMinThreads();
		queue = data.getPropertiesHbaseAtlas().getAtlasHookHbaseQueueSize();
		num = data.getPropertiesHbaseAtlas().getAtlasHookHbaseNumRetries();
		synchronous = data.getPropertiesHbaseAtlas().getAtlasHookHbaseSynchronous();
		
		secMax = data.getPropertiesHbaseLog().getHbaseSecurityLogMaxfilesize();
		maxFile = data.getPropertiesHbaseLog().getHbaseLogMaxfilesize();
		maxBack = data.getPropertiesHbaseLog().getHbaseLogMaxbackupindex();
		secMaxBack = data.getPropertiesHbaseLog().getHbaseSecurityLogMaxbackupindex();
		Content = data.getPropertiesHbaseLog().getContent();
	}
	public String getHbaseUse() {
		return hbaseUse;
	}
	
	public String getRatio() {
		return ratio;
	}
	public String getAtlasHook() {
		return atlashook;
	}
	public String getUser() {
		return user;
	}
	public String getHeapsize() {
		return heapsize;
	}
	public String getContent() {
		return content;
	}
	public String getTemp() {
		return temp;
	}
	public String getNproc() {
		return nproc;
	}
	public String getPidir() {
		return pidir;
	}
	public String getShutdown() {
		return shutdown;
	}
	public String getLogdir() {
		return logdir;
	}
	public String getServerHeap() {
		return serverHeap;
	}
	public String getNofile() {
		return nofile;
	}
	public String getSqlPhoenix() {
		return sql;
	}
	public String getType() {
		return type;
	}
	public String getCluster() {
		return cluster;
	}
	public String getStackid() {
		return stackid;
	}
	public String getVersion() {
		return version;
	}
	public String getTag() {
		return tag;
	}
	public String getMasterClasses() {
		return masterClass;
	}
	public String getCache() {
		return cache;
	}
	public String getMport() {
		return mport;
	}
	public String getAllowUser() {
		return alowUser;
	}
	public String getLocal() {
		return local;
	}
	public String getQuery() {
		return query;
	}
	public String getWal() {
		return wal;
	}
	public String getControl() {
		return control;
	}
	public String getRetries() {
		return retries;
	}
	public String getHstoreMax() {
		return hstore;
	}
	public String getQuorum() {
		return quorum;
	}
	public String getInfoPort() {
		return infoPort;
	}
	public String getRootDir() {
		return rootdir;
	}
	public String getClusterDis() {
		return Cluster;
	}
	public String getMajor() {
		return major;
	}
	public String getMemstore() {
		return memstore;
	}
	public String getSecAuth() {
		return secAuth;
	}
	public String getAuth() {
		return Auth;
	}
	public String getExecutor() {
		return executor;
	}
	public String getBlocking() {
		return blocking;
	}
	public String getUseMulti() {
		return useMulti;
	}
	public String getSocket() {
		return socket;
	}
	public String getScheduler() {
		return scheduler;
	}
	public String getSession() {
		return session;
	}
	public String getBindAddress() {
		return bindAddress;
	}
	public String getReadOnly() {
		return readonly;
	}
	public String getJitter() {
		return jitter;
	}
	public String getFlush() {
		return flush;
	}
	public String getFileSize() {
		return filesize;
	}
	public String getRecovery() {
		return recovery;
	}
	public String getGlobal() {
		return global;
	}
	public String getKeyValue() {
		return keyvalue;
	}
	public String getTmp() {
		return tmp;
	}
	public String getScanner() {
		return scanner;
	}
	public String getThreshold() {
		return Threshold;
	}
	public String getWait() {
		return wait;
	}
	public String getInitTime() {
		return init;
	}
	public String getTimeout() {
		return timeout;
	}
	public String getSuperUser() {
		return superuser;
	}
	public String getCoprocessorRegion() {
		return coprocessor;
	}
	public String getHandler() {
		return handler;
	}
	public String getStaging() {
		return staging;
	}
	public String getRegionPort() {
		return Regionport;
	}
	public String getProperty() {
		return property;
	}
	public String getSkip() {
		return skip;
	}
	public String getMultiplier() {
		return multiplier;
	}
	
	public String getMaster() {
		return master;
	}
	public String getAdmin() {
		return admin;
	}
	public String getClient() {
		return client;
	}
	public String getKeep() {
		return keep;
	}
	public String getMax() {
		return max;
	}
	public String getMin() {
		return min;
	}
	public String getNum() {
		return num;
	}
	public String getQueue() {
		return queue;
	}
	public String getSynchronous() {
		return synchronous;
	}
	public String getSecMax() {
		return secMax;
	}
	public String getMaxFile() {
		return maxFile;
	}
	public String getMaxBack() {
		return maxBack;
	}
	public String getSecMaxBack() {
		return secMaxBack;
	}
	public String getContentLog() {
		return Content;
	}
	public HbaseClient getData() {
		return data;
	}

}
