package com.labs247.telyu.model.Hbase;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class HbaseDataView implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//hbase-env
	String hbaseUse,ratio,user,atlashook,heapsize, content,temp,nproc,pidir,shutdown,logdir,serverHeap,nofile,sql;
	//hbase-site
	String type, cluster,stackid,version,tag,masterClass,cache,mport,alowUser,local,query,wal,control,retries,hstore,
	quorum,infoPort,rootdir,Cluster,major,memstore,secAuth,Auth,executor,blocking,useMulti,socket,scheduler,session,
	bindAddress,readonly,jitter,flush,filesize,recovery,global,keyvalue,tmp,scanner,Threshold,wait,init,timeout,
	superuser,coprocessor,handler,staging,Regionport,property,skip,multiplier;
	//hbase-policy
	String master,admin,client;
	//hbase-atlas
	String keep,max,min,queue,num,synchronous;
	//hbase-log4j
	String secMax,maxFile,maxBack,secMaxBack,Content;
	HbaseClient a = new HbaseClient();
	
	@PostConstruct
	public void init() {
		a.get_de_data();
		hbaseUse = a.getPropertiesData().getHbaseRegionserverXmnMax();
		ratio = a.getPropertiesData().getHbaseRegionserverXmnRatio();
		user = a.getPropertiesData().getHbaseUser();
		atlashook = a.getPropertiesData().getHbaseAtlasHook();
		heapsize = a.getPropertiesData().getHbaseMasterHeapsize();		
		content = a.getPropertiesData().getContent();
		temp = a.getPropertiesData().getHbaseJavaIoTmpdir();
		nproc = a.getPropertiesData().getHbaseUserNprocLimit();
		pidir = a.getPropertiesData().getHbasePidDir();
		shutdown = a.getPropertiesData().getHbaseRegionserverShutdownTimeout();
		logdir = a.getPropertiesData().getHbaseLogDir();
		serverHeap = a.getPropertiesData().getHbaseRegionserverHeapsize();
		nofile = a.getPropertiesData().getHbaseUserNofileLimit();
		sql = a.getPropertiesData().getPhoenixSqlEnabled();
		cluster = a.getItemData().getClusterName();
		stackid = a.getItemData().getStackId();
		
		version = Integer.toString(a.getConfigurationHbaseSite().getVersion());
		type= a.getConfigurationHbaseSite().getType();
		tag = a.getConfigurationHbaseSite().getTag();
		masterClass = a.getPropertiesHbaseSite().getHbaseCoprocessorMasterClasses();
		cache = a.getPropertiesHbaseSite().getHfileBlockCacheSize();
		mport = a.getPropertiesHbaseSite().getHbaseMasterPort();
		alowUser = a.getPropertiesHbaseSite().getPhoenixFunctionsAllowUserDefinedFunctions();
		local = a.getPropertiesHbaseSite().getHbaseLocalDir();
		query = a.getPropertiesHbaseSite().getPhoenixQueryTimeoutMs();
		wal = a.getPropertiesHbaseSite().getHbaseRegionserverWalCodec();
		control = a.getPropertiesHbaseSite().getHbaseRpcControllerfactoryClass();
		retries = a.getPropertiesHbaseSite().getHbaseClientRetriesNumber();
		hstore = a.getPropertiesHbaseSite().getHbaseHstoreCompactionMax();
		quorum = a.getPropertiesHbaseSite().getHbaseZookeeperQuorum();
		infoPort = a.getPropertiesHbaseSite().getHbaseMasterInfoPort();
		rootdir = a.getPropertiesHbaseSite().getHbaseRootdir();
		Cluster = a.getPropertiesHbaseSite().getHbaseClusterDistributed();
		major = a.getPropertiesHbaseSite().getHbaseHregionMajorcompaction();
		memstore = a.getPropertiesHbaseSite().getHbaseHregionMemstoreMslabEnabled();
		secAuth = a.getPropertiesHbaseSite().getHbaseSecurityAuthentication();
		Auth = a.getPropertiesHbaseSite().getHbaseSecurityAuthorization();
		executor = a.getPropertiesHbaseSite().getHbaseRegionserverExecutorOpenregionThreads();
		blocking = a.getPropertiesHbaseSite().getHbaseHstoreBlockingStoreFiles();
		useMulti = a.getPropertiesHbaseSite().getHbaseZookeeperUseMulti();
		socket = a.getPropertiesHbaseSite().getDfsDomainSocketPath();
		scheduler = a.getPropertiesHbaseSite().getHbaseRegionServerRpcSchedulerFactoryClass();
		session = a.getPropertiesHbaseSite().getZookeeperSessionTimeout();
		bindAddress = a.getPropertiesHbaseSite().getHbaseMasterInfoBindAddress();
		readonly = a.getPropertiesHbaseSite().getHbaseMasterUiReadonly();
		jitter = a.getPropertiesHbaseSite().getHbaseHregionMajorcompactionJitter();
		flush = a.getPropertiesHbaseSite().getHbaseHregionMemstoreFlushSize();
		filesize = a.getPropertiesHbaseSite().getHbaseHregionMaxFilesize();
		recovery = a.getPropertiesHbaseSite().getZookeeperRecoveryRetry();
		global = a.getPropertiesHbaseSite().getHbaseRegionserverGlobalMemstoreSize();
		keyvalue = a.getPropertiesHbaseSite().getHbaseClientKeyvalueMaxsize();
		tmp = a.getPropertiesHbaseSite().getHbaseTmpDir();
		scanner = a.getPropertiesHbaseSite().getHbaseClientScannerCaching();
		Threshold = a.getPropertiesHbaseSite().getHbaseHstoreCompactionThreshold();
		wait = a.getPropertiesHbaseSite().getHbaseMasterWaitOnRegionserversTimeout();
		init = a.getPropertiesHbaseSite().getHbaseMasterNamespaceInitTimeout();
		timeout = a.getPropertiesHbaseSite().getHbaseRpcTimeout();
		superuser = a.getPropertiesHbaseSite().getHbaseSuperuser();
		coprocessor = a.getPropertiesHbaseSite().getHbaseCoprocessorRegionClasses();
		handler = a.getPropertiesHbaseSite().getHbaseRegionserverHandlerCount();
		staging = a.getPropertiesHbaseSite().getHbaseBulkloadStagingDir();
		Regionport = a.getPropertiesHbaseSite().getHbaseRegionserverPort();
		property = a.getPropertiesHbaseSite().getHbaseZookeeperPropertyClientPort();
		skip = a.getPropertiesHbaseSite().getHbaseDefaultsForVersionSkip();
		multiplier = a.getPropertiesHbaseSite().getHbaseHregionMemstoreBlockMultiplier();
		
		master = a.getPropertiesHbasePolicy().getSecurityMasterregionProtocolAcl();
		admin = a.getPropertiesHbasePolicy().getSecurityAdminProtocolAcl();
		client = a.getPropertiesHbasePolicy().getSecurityClientProtocolAcl();
		
		keep = a.getPropertiesHbaseAtlas().getAtlasHookHbaseKeepAliveTime();
		max = a.getPropertiesHbaseAtlas().getAtlasHookHbaseMaxThreads();
		min = a.getPropertiesHbaseAtlas().getAtlasHookHbaseMinThreads();
		queue = a.getPropertiesHbaseAtlas().getAtlasHookHbaseQueueSize();
		num = a.getPropertiesHbaseAtlas().getAtlasHookHbaseNumRetries();
		synchronous = a.getPropertiesHbaseAtlas().getAtlasHookHbaseSynchronous();
		
		secMax = a.getPropertiesHbaseLog().getHbaseSecurityLogMaxfilesize();
		maxFile = a.getPropertiesHbaseLog().getHbaseLogMaxfilesize();
		maxBack = a.getPropertiesHbaseLog().getHbaseLogMaxbackupindex();
		secMaxBack = a.getPropertiesHbaseLog().getHbaseSecurityLogMaxbackupindex();
		Content = a.getPropertiesHbaseLog().getContent();
		
	}

	public String getHbaseUse() {
		return hbaseUse;
	}
	
	public String getRatio() {
		return ratio;
	}
	public String getAtlasHook() {
		return atlashook;
	}
	public String getUser() {
		return user;
	}
	public String getHeapsize() {
		return heapsize;
	}
	public String getContent() {
		return content;
	}
	public String getTemp() {
		return temp;
	}
	public String getNproc() {
		return nproc;
	}
	public String getPidir() {
		return pidir;
	}
	public String getShutdown() {
		return shutdown;
	}
	public String getLogdir() {
		return logdir;
	}
	public String getServerHeap() {
		return serverHeap;
	}
	public String getNofile() {
		return nofile;
	}
	public String getSqlPhoenix() {
		return sql;
	}
	public String getType() {
		return type;
	}
	public String getCluster() {
		return cluster;
	}
	public String getStackid() {
		return stackid;
	}
	public String getVersion() {
		return version;
	}
	public String getTag() {
		return tag;
	}
	public String getMasterClasses() {
		return masterClass;
	}
	public String getCache() {
		return cache;
	}
	public String getMport() {
		return mport;
	}
	public String getAllowUser() {
		return alowUser;
	}
	public String getLocal() {
		return local;
	}
	public String getQuery() {
		return query;
	}
	public String getWal() {
		return wal;
	}
	public String getControl() {
		return control;
	}
	public String getRetries() {
		return retries;
	}
	public String getHstoreMax() {
		return hstore;
	}
	public String getQuorum() {
		return quorum;
	}
	public String getInfoPort() {
		return infoPort;
	}
	public String getRootDir() {
		return rootdir;
	}
	public String getClusterDis() {
		return Cluster;
	}
	public String getMajor() {
		return major;
	}
	public String getMemstore() {
		return memstore;
	}
	public String getSecAuth() {
		return secAuth;
	}
	public String getAuth() {
		return Auth;
	}
	public String getExecutor() {
		return executor;
	}
	public String getBlocking() {
		return blocking;
	}
	public String getUseMulti() {
		return useMulti;
	}
	public String getSocket() {
		return socket;
	}
	public String getScheduler() {
		return scheduler;
	}
	public String getSession() {
		return session;
	}
	public String getBindAddress() {
		return bindAddress;
	}
	public String getReadOnly() {
		return readonly;
	}
	public String getJitter() {
		return jitter;
	}
	public String getFlush() {
		return flush;
	}
	public String getFileSize() {
		return filesize;
	}
	public String getRecovery() {
		return recovery;
	}
	public String getGlobal() {
		return global;
	}
	public String getKeyValue() {
		return keyvalue;
	}
	public String getTmp() {
		return tmp;
	}
	public String getScanner() {
		return scanner;
	}
	public String getThreshold() {
		return Threshold;
	}
	public String getWait() {
		return wait;
	}
	public String getInitTime() {
		return init;
	}
	public String getTimeout() {
		return timeout;
	}
	public String getSuperUser() {
		return superuser;
	}
	public String getCoprocessorRegion() {
		return coprocessor;
	}
	public String getHandler() {
		return handler;
	}
	public String getStaging() {
		return staging;
	}
	public String getRegionPort() {
		return Regionport;
	}
	public String getProperty() {
		return property;
	}
	public String getSkip() {
		return skip;
	}
	public String getMultiplier() {
		return multiplier;
	}
	
	public String getMaster() {
		return master;
	}
	public String getAdmin() {
		return admin;
	}
	public String getClient() {
		return client;
	}
	public String getKeep() {
		return keep;
	}
	public String getMax() {
		return max;
	}
	public String getMin() {
		return min;
	}
	public String getNum() {
		return num;
	}
	public String getQueue() {
		return queue;
	}
	public String getSynchronous() {
		return synchronous;
	}
	public String getSecMax() {
		return secMax;
	}
	public String getMaxFile() {
		return maxFile;
	}
	public String getMaxBack() {
		return maxBack;
	}
	public String getSecMaxBack() {
		return secMaxBack;
	}
	public String getContentLog() {
		return Content;
	}
}
