package com.labs247.telyu.model.Hbase;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.labs247.telyu.model.Hbase.JsonParsingUtil;
import com.labs247.telyu.model.Hbase.Configuration;
import com.labs247.telyu.model.Hbase.Item;
import com.labs247.telyu.model.Hbase.Properties;


@Singleton
@Startup
public class HbaseClient {
	public enum States {BEFORESTARTED, STARTED, PAUSED, SHUTTINGDOWN};
    
	private States state;

	static String host = "192.168.3.132";
	static String port = "8080";
	
	static Hbase hbase = null;
	
	static Item currentItem;
	
	static long fire_time = System.currentTimeMillis();
	
	public HbaseClient() {
		System.out.println("HbaseClient init ...");
		
		System.out.println("HbaseClient init ... done");
	}
	
	public Hbase getHbasePojo() {
		System.out.println("getHbasePojo invoked ...");
		
		return hbase;
	}
	
	@PostConstruct
	public void initialize() {
		state = States.BEFORESTARTED;
		// Perform intialization
		get_de_data();
		state = States.STARTED;
		System.out.println("Service Started");
	}
	
	//@Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
		@Schedule(minute = "*/5", hour = "*", persistent = false)
	    public void atSchedule() throws InterruptedException {
			
			get_de_data();
			
	        System.out.println("DeclarativeScheduler:: In atSchedule() fired get_de_data()");
	        
	    }
		
		public void get_de_data() {
			
			try {
					System.out.println(".......................Rest Dimulai ..................................");
					HbaseRestClient _client = new HbaseRestClient(host, port);
					String URL = "http://192.168.3.132:8080/api/v1/clusters/C10H16/configurations/service_config_versions/?service_name=HBASE";
					fire_time = System.currentTimeMillis();
					String json = _client.basic_auth_client(URL);

					hbase = JsonParsingUtil.json2DataNode(json);
					
					
					getItemData();
					
					System.out.println("..........................Hbase Data : ...................");
					
					
//					System.out.println(getPropertiesData());

				} catch (Exception e) {
					e.printStackTrace();
				}
		}

		@PreDestroy
		public void terminate() {
			state = States.SHUTTINGDOWN;
			// Perform termination
			System.out.println("HbaseCLient shutdown in progress");
		}

		public States getState() {
			return state;
		}

		public void setState(States state) {
			this.state = state;
		}
		
		public long getFiretime() {
			return fire_time;
		}
		
		
		public String getHref() {
			
			return hbase.getHref();
			
		}
		//Type Hbase-Env
		
		public Item getItemData() {
			 currentItem = hbase.getItems().stream()
					.filter(e -> e.getIsCurrent())
					.findAny()
					.orElse(null);
			
			return currentItem;
		}
		
		public Configuration getConfigurationData() {

			// i.e java.lang:type=OperatingSystem
			Configuration configuration = currentItem.getConfigurations().stream()
					.filter(configurationku -> "hbase-env".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration;
		}

		public Properties getPropertiesData() {

			Configuration configuration = currentItem.getConfigurations().stream()
					.filter(configurationku -> "hbase-env".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		//type : hbase-site
		public Configuration getConfigurationHbaseSite() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-site".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbaseSite() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-site".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		//type : hbase-policy
		
		public Configuration getConfigurationHbasePolicy() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-policy".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbasePolicy() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-policy".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		//type: hbase-log4j
		
		public Configuration getConfigurationHbaseLog() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-log4j".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbaseLog() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-log4j".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		
		//type : hbase-atlas
		public Configuration getConfigurationHbaseAtlas() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-atlas-application-properties".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbaseAtlas() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "hbase-atlas-application-properties".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		
		//type : hbase-security
		public Configuration getConfigurationHbaseSecurity() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-security".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbaseSecurity() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-security".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		//type : hbase-audit
		public Configuration getConfigurationHbaseAudit() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-audit".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbaseAudit() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-audit".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
		//type : hbase-policymgr
		public Configuration getConfigurationHbasePolicymgr() {

			// i.e java.lang:type=OperatingSystem

			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-policymgr-ssl".equals(configurationku.getType())).findAny().orElse(null);
			return configuration;
		}
		
		public Properties getPropertiesHbasePolicymgr() {
			Item _de_bean = hbase.getItems().stream().filter(item -> "C10H16".equals(item.getClusterName())).findAny()
					.orElse(null);

			Configuration configuration = _de_bean.getConfigurations().stream()
					.filter(configurationku -> "ranger-hbase-policymgr-ssl".equals(configurationku.getType())).findAny().orElse(null);
			
			return configuration.getProperties();
		}
}
