package com.labs247.telyu.model.Hbase;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonParsingUtil {
	
	public static Hbase json2DataNode(String json) {

		JsonParser parser = new JsonParser();
		
		Hbase hbasePojo =  null;

		JsonElement jsonTree = parser.parse(json);
		

		if (jsonTree.isJsonObject()) {

			JsonObject jsonObject = jsonTree.getAsJsonObject();

			Gson gson = new Gson();
			
			hbasePojo = gson.fromJson(jsonObject, Hbase.class);
			
		}

		return hbasePojo ;
	}
}
