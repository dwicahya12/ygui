package com.labs247.telyu.model.Hbase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {

    @SerializedName("atlas.hook.hbase.keepAliveTime")
    @Expose
    private String atlasHookHbaseKeepAliveTime;
    @SerializedName("atlas.hook.hbase.maxThreads")
    @Expose
    private String atlasHookHbaseMaxThreads;
    @SerializedName("atlas.hook.hbase.minThreads")
    @Expose
    private String atlasHookHbaseMinThreads;
    @SerializedName("atlas.hook.hbase.numRetries")
    @Expose
    private String atlasHookHbaseNumRetries;
    @SerializedName("atlas.hook.hbase.queueSize")
    @Expose
    private String atlasHookHbaseQueueSize;
    @SerializedName("atlas.hook.hbase.synchronous")
    @Expose
    private String atlasHookHbaseSynchronous;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("hbase.atlas.hook")
    @Expose
    private String hbaseAtlasHook;
    @SerializedName("hbase_java_io_tmpdir")
    @Expose
    private String hbaseJavaIoTmpdir;
    @SerializedName("hbase_log_dir")
    @Expose
    private String hbaseLogDir;
    @SerializedName("hbase_master_heapsize")
    @Expose
    private String hbaseMasterHeapsize;
    @SerializedName("hbase_pid_dir")
    @Expose
    private String hbasePidDir;
    @SerializedName("hbase_regionserver_heapsize")
    @Expose
    private String hbaseRegionserverHeapsize;
    @SerializedName("hbase_regionserver_shutdown_timeout")
    @Expose
    private String hbaseRegionserverShutdownTimeout;
    @SerializedName("hbase_regionserver_xmn_max")
    @Expose
    private String hbaseRegionserverXmnMax;
    @SerializedName("hbase_regionserver_xmn_ratio")
    @Expose
    private String hbaseRegionserverXmnRatio;
    @SerializedName("hbase_user")
    @Expose
    private String hbaseUser;
    @SerializedName("hbase_user_nofile_limit")
    @Expose
    private String hbaseUserNofileLimit;
    @SerializedName("hbase_user_nproc_limit")
    @Expose
    private String hbaseUserNprocLimit;
    @SerializedName("phoenix_sql_enabled")
    @Expose
    private String phoenixSqlEnabled;
    @SerializedName("hbase_log_maxbackupindex")
    @Expose
    private String hbaseLogMaxbackupindex;
    @SerializedName("hbase_log_maxfilesize")
    @Expose
    private String hbaseLogMaxfilesize;
    @SerializedName("hbase_security_log_maxbackupindex")
    @Expose
    private String hbaseSecurityLogMaxbackupindex;
    @SerializedName("hbase_security_log_maxfilesize")
    @Expose
    private String hbaseSecurityLogMaxfilesize;
    @SerializedName("security.admin.protocol.acl")
    @Expose
    private String securityAdminProtocolAcl;
    @SerializedName("security.client.protocol.acl")
    @Expose
    private String securityClientProtocolAcl;
    @SerializedName("security.masterregion.protocol.acl")
    @Expose
    private String securityMasterregionProtocolAcl;
    @SerializedName("dfs.domain.socket.path")
    @Expose
    private String dfsDomainSocketPath;
    @SerializedName("hbase.bulkload.staging.dir")
    @Expose
    private String hbaseBulkloadStagingDir;
    @SerializedName("hbase.client.keyvalue.maxsize")
    @Expose
    private String hbaseClientKeyvalueMaxsize;
    @SerializedName("hbase.client.retries.number")
    @Expose
    private String hbaseClientRetriesNumber;
    @SerializedName("hbase.client.scanner.caching")
    @Expose
    private String hbaseClientScannerCaching;
    @SerializedName("hbase.cluster.distributed")
    @Expose
    private String hbaseClusterDistributed;
    @SerializedName("hbase.coprocessor.master.classes")
    @Expose
    private String hbaseCoprocessorMasterClasses;
    @SerializedName("hbase.coprocessor.region.classes")
    @Expose
    private String hbaseCoprocessorRegionClasses;
    @SerializedName("hbase.defaults.for.version.skip")
    @Expose
    private String hbaseDefaultsForVersionSkip;
    @SerializedName("hbase.hregion.majorcompaction")
    @Expose
    private String hbaseHregionMajorcompaction;
    @SerializedName("hbase.hregion.majorcompaction.jitter")
    @Expose
    private String hbaseHregionMajorcompactionJitter;
    @SerializedName("hbase.hregion.max.filesize")
    @Expose
    private String hbaseHregionMaxFilesize;
    @SerializedName("hbase.hregion.memstore.block.multiplier")
    @Expose
    private String hbaseHregionMemstoreBlockMultiplier;
    @SerializedName("hbase.hregion.memstore.flush.size")
    @Expose
    private String hbaseHregionMemstoreFlushSize;
    @SerializedName("hbase.hregion.memstore.mslab.enabled")
    @Expose
    private String hbaseHregionMemstoreMslabEnabled;
    @SerializedName("hbase.hstore.blockingStoreFiles")
    @Expose
    private String hbaseHstoreBlockingStoreFiles;
    @SerializedName("hbase.hstore.compaction.max")
    @Expose
    private String hbaseHstoreCompactionMax;
    @SerializedName("hbase.hstore.compactionThreshold")
    @Expose
    private String hbaseHstoreCompactionThreshold;
    @SerializedName("hbase.local.dir")
    @Expose
    private String hbaseLocalDir;
    @SerializedName("hbase.master.info.bindAddress")
    @Expose
    private String hbaseMasterInfoBindAddress;
    @SerializedName("hbase.master.info.port")
    @Expose
    private String hbaseMasterInfoPort;
    @SerializedName("hbase.master.namespace.init.timeout")
    @Expose
    private String hbaseMasterNamespaceInitTimeout;
    @SerializedName("hbase.master.port")
    @Expose
    private String hbaseMasterPort;
    @SerializedName("hbase.master.ui.readonly")
    @Expose
    private String hbaseMasterUiReadonly;
    @SerializedName("hbase.master.wait.on.regionservers.timeout")
    @Expose
    private String hbaseMasterWaitOnRegionserversTimeout;
    @SerializedName("hbase.region.server.rpc.scheduler.factory.class")
    @Expose
    private String hbaseRegionServerRpcSchedulerFactoryClass;
    @SerializedName("hbase.regionserver.executor.openregion.threads")
    @Expose
    private String hbaseRegionserverExecutorOpenregionThreads;
    @SerializedName("hbase.regionserver.global.memstore.size")
    @Expose
    private String hbaseRegionserverGlobalMemstoreSize;
    @SerializedName("hbase.regionserver.handler.count")
    @Expose
    private String hbaseRegionserverHandlerCount;
    @SerializedName("hbase.regionserver.info.port")
    @Expose
    private String hbaseRegionserverInfoPort;
    @SerializedName("hbase.regionserver.port")
    @Expose
    private String hbaseRegionserverPort;
    @SerializedName("hbase.regionserver.wal.codec")
    @Expose
    private String hbaseRegionserverWalCodec;
    @SerializedName("hbase.rootdir")
    @Expose
    private String hbaseRootdir;
    @SerializedName("hbase.rpc.controllerfactory.class")
    @Expose
    private String hbaseRpcControllerfactoryClass;
    @SerializedName("hbase.rpc.protection")
    @Expose
    private String hbaseRpcProtection;
    @SerializedName("hbase.rpc.timeout")
    @Expose
    private String hbaseRpcTimeout;
    @SerializedName("hbase.security.authentication")
    @Expose
    private String hbaseSecurityAuthentication;
    @SerializedName("hbase.security.authorization")
    @Expose
    private String hbaseSecurityAuthorization;
    @SerializedName("hbase.superuser")
    @Expose
    private String hbaseSuperuser;
    @SerializedName("hbase.tmp.dir")
    @Expose
    private String hbaseTmpDir;
    @SerializedName("hbase.zookeeper.property.clientPort")
    @Expose
    private String hbaseZookeeperPropertyClientPort;
    @SerializedName("hbase.zookeeper.quorum")
    @Expose
    private String hbaseZookeeperQuorum;
    @SerializedName("hbase.zookeeper.useMulti")
    @Expose
    private String hbaseZookeeperUseMulti;
    @SerializedName("hfile.block.cache.size")
    @Expose
    private String hfileBlockCacheSize;
    @SerializedName("phoenix.functions.allowUserDefinedFunctions")
    @Expose
    private String phoenixFunctionsAllowUserDefinedFunctions;
    @SerializedName("phoenix.query.timeoutMs")
    @Expose
    private String phoenixQueryTimeoutMs;
    @SerializedName("zookeeper.recovery.retry")
    @Expose
    private String zookeeperRecoveryRetry;
    @SerializedName("zookeeper.session.timeout")
    @Expose
    private String zookeeperSessionTimeout;
    @SerializedName("zookeeper.znode.parent")
    @Expose
    private String zookeeperZnodeParent;

    public String getAtlasHookHbaseKeepAliveTime() {
        return atlasHookHbaseKeepAliveTime;
    }

    public void setAtlasHookHbaseKeepAliveTime(String atlasHookHbaseKeepAliveTime) {
        this.atlasHookHbaseKeepAliveTime = atlasHookHbaseKeepAliveTime;
    }

    public String getAtlasHookHbaseMaxThreads() {
        return atlasHookHbaseMaxThreads;
    }

    public void setAtlasHookHbaseMaxThreads(String atlasHookHbaseMaxThreads) {
        this.atlasHookHbaseMaxThreads = atlasHookHbaseMaxThreads;
    }

    public String getAtlasHookHbaseMinThreads() {
        return atlasHookHbaseMinThreads;
    }

    public void setAtlasHookHbaseMinThreads(String atlasHookHbaseMinThreads) {
        this.atlasHookHbaseMinThreads = atlasHookHbaseMinThreads;
    }

    public String getAtlasHookHbaseNumRetries() {
        return atlasHookHbaseNumRetries;
    }

    public void setAtlasHookHbaseNumRetries(String atlasHookHbaseNumRetries) {
        this.atlasHookHbaseNumRetries = atlasHookHbaseNumRetries;
    }

    public String getAtlasHookHbaseQueueSize() {
        return atlasHookHbaseQueueSize;
    }

    public void setAtlasHookHbaseQueueSize(String atlasHookHbaseQueueSize) {
        this.atlasHookHbaseQueueSize = atlasHookHbaseQueueSize;
    }

    public String getAtlasHookHbaseSynchronous() {
        return atlasHookHbaseSynchronous;
    }

    public void setAtlasHookHbaseSynchronous(String atlasHookHbaseSynchronous) {
        this.atlasHookHbaseSynchronous = atlasHookHbaseSynchronous;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHbaseAtlasHook() {
        return hbaseAtlasHook;
    }

    public void setHbaseAtlasHook(String hbaseAtlasHook) {
        this.hbaseAtlasHook = hbaseAtlasHook;
    }

    public String getHbaseJavaIoTmpdir() {
        return hbaseJavaIoTmpdir;
    }

    public void setHbaseJavaIoTmpdir(String hbaseJavaIoTmpdir) {
        this.hbaseJavaIoTmpdir = hbaseJavaIoTmpdir;
    }

    public String getHbaseLogDir() {
        return hbaseLogDir;
    }

    public void setHbaseLogDir(String hbaseLogDir) {
        this.hbaseLogDir = hbaseLogDir;
    }

    public String getHbaseMasterHeapsize() {
        return hbaseMasterHeapsize;
    }

    public void setHbaseMasterHeapsize(String hbaseMasterHeapsize) {
        this.hbaseMasterHeapsize = hbaseMasterHeapsize;
    }

    public String getHbasePidDir() {
        return hbasePidDir;
    }

    public void setHbasePidDir(String hbasePidDir) {
        this.hbasePidDir = hbasePidDir;
    }

    public String getHbaseRegionserverHeapsize() {
        return hbaseRegionserverHeapsize;
    }

    public void setHbaseRegionserverHeapsize(String hbaseRegionserverHeapsize) {
        this.hbaseRegionserverHeapsize = hbaseRegionserverHeapsize;
    }

    public String getHbaseRegionserverShutdownTimeout() {
        return hbaseRegionserverShutdownTimeout;
    }

    public void setHbaseRegionserverShutdownTimeout(String hbaseRegionserverShutdownTimeout) {
        this.hbaseRegionserverShutdownTimeout = hbaseRegionserverShutdownTimeout;
    }

    public String getHbaseRegionserverXmnMax() {
        return hbaseRegionserverXmnMax;
    }

    public void setHbaseRegionserverXmnMax(String hbaseRegionserverXmnMax) {
        this.hbaseRegionserverXmnMax = hbaseRegionserverXmnMax;
    }

    public String getHbaseRegionserverXmnRatio() {
        return hbaseRegionserverXmnRatio;
    }

    public void setHbaseRegionserverXmnRatio(String hbaseRegionserverXmnRatio) {
        this.hbaseRegionserverXmnRatio = hbaseRegionserverXmnRatio;
    }

    public String getHbaseUser() {
        return hbaseUser;
    }

    public void setHbaseUser(String hbaseUser) {
        this.hbaseUser = hbaseUser;
    }

    public String getHbaseUserNofileLimit() {
        return hbaseUserNofileLimit;
    }

    public void setHbaseUserNofileLimit(String hbaseUserNofileLimit) {
        this.hbaseUserNofileLimit = hbaseUserNofileLimit;
    }

    public String getHbaseUserNprocLimit() {
        return hbaseUserNprocLimit;
    }

    public void setHbaseUserNprocLimit(String hbaseUserNprocLimit) {
        this.hbaseUserNprocLimit = hbaseUserNprocLimit;
    }

    public String getPhoenixSqlEnabled() {
        return phoenixSqlEnabled;
    }

    public void setPhoenixSqlEnabled(String phoenixSqlEnabled) {
        this.phoenixSqlEnabled = phoenixSqlEnabled;
    }

    public String getHbaseLogMaxbackupindex() {
        return hbaseLogMaxbackupindex;
    }

    public void setHbaseLogMaxbackupindex(String hbaseLogMaxbackupindex) {
        this.hbaseLogMaxbackupindex = hbaseLogMaxbackupindex;
    }

    public String getHbaseLogMaxfilesize() {
        return hbaseLogMaxfilesize;
    }

    public void setHbaseLogMaxfilesize(String hbaseLogMaxfilesize) {
        this.hbaseLogMaxfilesize = hbaseLogMaxfilesize;
    }

    public String getHbaseSecurityLogMaxbackupindex() {
        return hbaseSecurityLogMaxbackupindex;
    }

    public void setHbaseSecurityLogMaxbackupindex(String hbaseSecurityLogMaxbackupindex) {
        this.hbaseSecurityLogMaxbackupindex = hbaseSecurityLogMaxbackupindex;
    }

    public String getHbaseSecurityLogMaxfilesize() {
        return hbaseSecurityLogMaxfilesize;
    }

    public void setHbaseSecurityLogMaxfilesize(String hbaseSecurityLogMaxfilesize) {
        this.hbaseSecurityLogMaxfilesize = hbaseSecurityLogMaxfilesize;
    }

    public String getSecurityAdminProtocolAcl() {
        return securityAdminProtocolAcl;
    }

    public void setSecurityAdminProtocolAcl(String securityAdminProtocolAcl) {
        this.securityAdminProtocolAcl = securityAdminProtocolAcl;
    }

    public String getSecurityClientProtocolAcl() {
        return securityClientProtocolAcl;
    }

    public void setSecurityClientProtocolAcl(String securityClientProtocolAcl) {
        this.securityClientProtocolAcl = securityClientProtocolAcl;
    }

    public String getSecurityMasterregionProtocolAcl() {
        return securityMasterregionProtocolAcl;
    }

    public void setSecurityMasterregionProtocolAcl(String securityMasterregionProtocolAcl) {
        this.securityMasterregionProtocolAcl = securityMasterregionProtocolAcl;
    }

    public String getDfsDomainSocketPath() {
        return dfsDomainSocketPath;
    }

    public void setDfsDomainSocketPath(String dfsDomainSocketPath) {
        this.dfsDomainSocketPath = dfsDomainSocketPath;
    }

    public String getHbaseBulkloadStagingDir() {
        return hbaseBulkloadStagingDir;
    }

    public void setHbaseBulkloadStagingDir(String hbaseBulkloadStagingDir) {
        this.hbaseBulkloadStagingDir = hbaseBulkloadStagingDir;
    }

    public String getHbaseClientKeyvalueMaxsize() {
        return hbaseClientKeyvalueMaxsize;
    }

    public void setHbaseClientKeyvalueMaxsize(String hbaseClientKeyvalueMaxsize) {
        this.hbaseClientKeyvalueMaxsize = hbaseClientKeyvalueMaxsize;
    }

    public String getHbaseClientRetriesNumber() {
        return hbaseClientRetriesNumber;
    }

    public void setHbaseClientRetriesNumber(String hbaseClientRetriesNumber) {
        this.hbaseClientRetriesNumber = hbaseClientRetriesNumber;
    }

    public String getHbaseClientScannerCaching() {
        return hbaseClientScannerCaching;
    }

    public void setHbaseClientScannerCaching(String hbaseClientScannerCaching) {
        this.hbaseClientScannerCaching = hbaseClientScannerCaching;
    }

    public String getHbaseClusterDistributed() {
        return hbaseClusterDistributed;
    }

    public void setHbaseClusterDistributed(String hbaseClusterDistributed) {
        this.hbaseClusterDistributed = hbaseClusterDistributed;
    }

    public String getHbaseCoprocessorMasterClasses() {
        return hbaseCoprocessorMasterClasses;
    }

    public void setHbaseCoprocessorMasterClasses(String hbaseCoprocessorMasterClasses) {
        this.hbaseCoprocessorMasterClasses = hbaseCoprocessorMasterClasses;
    }

    public String getHbaseCoprocessorRegionClasses() {
        return hbaseCoprocessorRegionClasses;
    }

    public void setHbaseCoprocessorRegionClasses(String hbaseCoprocessorRegionClasses) {
        this.hbaseCoprocessorRegionClasses = hbaseCoprocessorRegionClasses;
    }

    public String getHbaseDefaultsForVersionSkip() {
        return hbaseDefaultsForVersionSkip;
    }

    public void setHbaseDefaultsForVersionSkip(String hbaseDefaultsForVersionSkip) {
        this.hbaseDefaultsForVersionSkip = hbaseDefaultsForVersionSkip;
    }

    public String getHbaseHregionMajorcompaction() {
        return hbaseHregionMajorcompaction;
    }

    public void setHbaseHregionMajorcompaction(String hbaseHregionMajorcompaction) {
        this.hbaseHregionMajorcompaction = hbaseHregionMajorcompaction;
    }

    public String getHbaseHregionMajorcompactionJitter() {
        return hbaseHregionMajorcompactionJitter;
    }

    public void setHbaseHregionMajorcompactionJitter(String hbaseHregionMajorcompactionJitter) {
        this.hbaseHregionMajorcompactionJitter = hbaseHregionMajorcompactionJitter;
    }

    public String getHbaseHregionMaxFilesize() {
        return hbaseHregionMaxFilesize;
    }

    public void setHbaseHregionMaxFilesize(String hbaseHregionMaxFilesize) {
        this.hbaseHregionMaxFilesize = hbaseHregionMaxFilesize;
    }

    public String getHbaseHregionMemstoreBlockMultiplier() {
        return hbaseHregionMemstoreBlockMultiplier;
    }

    public void setHbaseHregionMemstoreBlockMultiplier(String hbaseHregionMemstoreBlockMultiplier) {
        this.hbaseHregionMemstoreBlockMultiplier = hbaseHregionMemstoreBlockMultiplier;
    }

    public String getHbaseHregionMemstoreFlushSize() {
        return hbaseHregionMemstoreFlushSize;
    }

    public void setHbaseHregionMemstoreFlushSize(String hbaseHregionMemstoreFlushSize) {
        this.hbaseHregionMemstoreFlushSize = hbaseHregionMemstoreFlushSize;
    }

    public String getHbaseHregionMemstoreMslabEnabled() {
        return hbaseHregionMemstoreMslabEnabled;
    }

    public void setHbaseHregionMemstoreMslabEnabled(String hbaseHregionMemstoreMslabEnabled) {
        this.hbaseHregionMemstoreMslabEnabled = hbaseHregionMemstoreMslabEnabled;
    }

    public String getHbaseHstoreBlockingStoreFiles() {
        return hbaseHstoreBlockingStoreFiles;
    }

    public void setHbaseHstoreBlockingStoreFiles(String hbaseHstoreBlockingStoreFiles) {
        this.hbaseHstoreBlockingStoreFiles = hbaseHstoreBlockingStoreFiles;
    }

    public String getHbaseHstoreCompactionMax() {
        return hbaseHstoreCompactionMax;
    }

    public void setHbaseHstoreCompactionMax(String hbaseHstoreCompactionMax) {
        this.hbaseHstoreCompactionMax = hbaseHstoreCompactionMax;
    }

    public String getHbaseHstoreCompactionThreshold() {
        return hbaseHstoreCompactionThreshold;
    }

    public void setHbaseHstoreCompactionThreshold(String hbaseHstoreCompactionThreshold) {
        this.hbaseHstoreCompactionThreshold = hbaseHstoreCompactionThreshold;
    }

    public String getHbaseLocalDir() {
        return hbaseLocalDir;
    }

    public void setHbaseLocalDir(String hbaseLocalDir) {
        this.hbaseLocalDir = hbaseLocalDir;
    }

    public String getHbaseMasterInfoBindAddress() {
        return hbaseMasterInfoBindAddress;
    }

    public void setHbaseMasterInfoBindAddress(String hbaseMasterInfoBindAddress) {
        this.hbaseMasterInfoBindAddress = hbaseMasterInfoBindAddress;
    }

    public String getHbaseMasterInfoPort() {
        return hbaseMasterInfoPort;
    }

    public void setHbaseMasterInfoPort(String hbaseMasterInfoPort) {
        this.hbaseMasterInfoPort = hbaseMasterInfoPort;
    }

    public String getHbaseMasterNamespaceInitTimeout() {
        return hbaseMasterNamespaceInitTimeout;
    }

    public void setHbaseMasterNamespaceInitTimeout(String hbaseMasterNamespaceInitTimeout) {
        this.hbaseMasterNamespaceInitTimeout = hbaseMasterNamespaceInitTimeout;
    }

    public String getHbaseMasterPort() {
        return hbaseMasterPort;
    }

    public void setHbaseMasterPort(String hbaseMasterPort) {
        this.hbaseMasterPort = hbaseMasterPort;
    }

    public String getHbaseMasterUiReadonly() {
        return hbaseMasterUiReadonly;
    }

    public void setHbaseMasterUiReadonly(String hbaseMasterUiReadonly) {
        this.hbaseMasterUiReadonly = hbaseMasterUiReadonly;
    }

    public String getHbaseMasterWaitOnRegionserversTimeout() {
        return hbaseMasterWaitOnRegionserversTimeout;
    }

    public void setHbaseMasterWaitOnRegionserversTimeout(String hbaseMasterWaitOnRegionserversTimeout) {
        this.hbaseMasterWaitOnRegionserversTimeout = hbaseMasterWaitOnRegionserversTimeout;
    }

    public String getHbaseRegionServerRpcSchedulerFactoryClass() {
        return hbaseRegionServerRpcSchedulerFactoryClass;
    }

    public void setHbaseRegionServerRpcSchedulerFactoryClass(String hbaseRegionServerRpcSchedulerFactoryClass) {
        this.hbaseRegionServerRpcSchedulerFactoryClass = hbaseRegionServerRpcSchedulerFactoryClass;
    }

    public String getHbaseRegionserverExecutorOpenregionThreads() {
        return hbaseRegionserverExecutorOpenregionThreads;
    }

    public void setHbaseRegionserverExecutorOpenregionThreads(String hbaseRegionserverExecutorOpenregionThreads) {
        this.hbaseRegionserverExecutorOpenregionThreads = hbaseRegionserverExecutorOpenregionThreads;
    }

    public String getHbaseRegionserverGlobalMemstoreSize() {
        return hbaseRegionserverGlobalMemstoreSize;
    }

    public void setHbaseRegionserverGlobalMemstoreSize(String hbaseRegionserverGlobalMemstoreSize) {
        this.hbaseRegionserverGlobalMemstoreSize = hbaseRegionserverGlobalMemstoreSize;
    }

    public String getHbaseRegionserverHandlerCount() {
        return hbaseRegionserverHandlerCount;
    }

    public void setHbaseRegionserverHandlerCount(String hbaseRegionserverHandlerCount) {
        this.hbaseRegionserverHandlerCount = hbaseRegionserverHandlerCount;
    }

    public String getHbaseRegionserverInfoPort() {
        return hbaseRegionserverInfoPort;
    }

    public void setHbaseRegionserverInfoPort(String hbaseRegionserverInfoPort) {
        this.hbaseRegionserverInfoPort = hbaseRegionserverInfoPort;
    }

    public String getHbaseRegionserverPort() {
        return hbaseRegionserverPort;
    }

    public void setHbaseRegionserverPort(String hbaseRegionserverPort) {
        this.hbaseRegionserverPort = hbaseRegionserverPort;
    }

    public String getHbaseRegionserverWalCodec() {
        return hbaseRegionserverWalCodec;
    }

    public void setHbaseRegionserverWalCodec(String hbaseRegionserverWalCodec) {
        this.hbaseRegionserverWalCodec = hbaseRegionserverWalCodec;
    }

    public String getHbaseRootdir() {
        return hbaseRootdir;
    }

    public void setHbaseRootdir(String hbaseRootdir) {
        this.hbaseRootdir = hbaseRootdir;
    }

    public String getHbaseRpcControllerfactoryClass() {
        return hbaseRpcControllerfactoryClass;
    }

    public void setHbaseRpcControllerfactoryClass(String hbaseRpcControllerfactoryClass) {
        this.hbaseRpcControllerfactoryClass = hbaseRpcControllerfactoryClass;
    }

    public String getHbaseRpcProtection() {
        return hbaseRpcProtection;
    }

    public void setHbaseRpcProtection(String hbaseRpcProtection) {
        this.hbaseRpcProtection = hbaseRpcProtection;
    }

    public String getHbaseRpcTimeout() {
        return hbaseRpcTimeout;
    }

    public void setHbaseRpcTimeout(String hbaseRpcTimeout) {
        this.hbaseRpcTimeout = hbaseRpcTimeout;
    }

    public String getHbaseSecurityAuthentication() {
        return hbaseSecurityAuthentication;
    }

    public void setHbaseSecurityAuthentication(String hbaseSecurityAuthentication) {
        this.hbaseSecurityAuthentication = hbaseSecurityAuthentication;
    }

    public String getHbaseSecurityAuthorization() {
        return hbaseSecurityAuthorization;
    }

    public void setHbaseSecurityAuthorization(String hbaseSecurityAuthorization) {
        this.hbaseSecurityAuthorization = hbaseSecurityAuthorization;
    }

    public String getHbaseSuperuser() {
        return hbaseSuperuser;
    }

    public void setHbaseSuperuser(String hbaseSuperuser) {
        this.hbaseSuperuser = hbaseSuperuser;
    }

    public String getHbaseTmpDir() {
        return hbaseTmpDir;
    }

    public void setHbaseTmpDir(String hbaseTmpDir) {
        this.hbaseTmpDir = hbaseTmpDir;
    }

    public String getHbaseZookeeperPropertyClientPort() {
        return hbaseZookeeperPropertyClientPort;
    }

    public void setHbaseZookeeperPropertyClientPort(String hbaseZookeeperPropertyClientPort) {
        this.hbaseZookeeperPropertyClientPort = hbaseZookeeperPropertyClientPort;
    }

    public String getHbaseZookeeperQuorum() {
        return hbaseZookeeperQuorum;
    }

    public void setHbaseZookeeperQuorum(String hbaseZookeeperQuorum) {
        this.hbaseZookeeperQuorum = hbaseZookeeperQuorum;
    }

    public String getHbaseZookeeperUseMulti() {
        return hbaseZookeeperUseMulti;
    }

    public void setHbaseZookeeperUseMulti(String hbaseZookeeperUseMulti) {
        this.hbaseZookeeperUseMulti = hbaseZookeeperUseMulti;
    }

    public String getHfileBlockCacheSize() {
        return hfileBlockCacheSize;
    }

    public void setHfileBlockCacheSize(String hfileBlockCacheSize) {
        this.hfileBlockCacheSize = hfileBlockCacheSize;
    }

    public String getPhoenixFunctionsAllowUserDefinedFunctions() {
        return phoenixFunctionsAllowUserDefinedFunctions;
    }

    public void setPhoenixFunctionsAllowUserDefinedFunctions(String phoenixFunctionsAllowUserDefinedFunctions) {
        this.phoenixFunctionsAllowUserDefinedFunctions = phoenixFunctionsAllowUserDefinedFunctions;
    }

    public String getPhoenixQueryTimeoutMs() {
        return phoenixQueryTimeoutMs;
    }

    public void setPhoenixQueryTimeoutMs(String phoenixQueryTimeoutMs) {
        this.phoenixQueryTimeoutMs = phoenixQueryTimeoutMs;
    }

    public String getZookeeperRecoveryRetry() {
        return zookeeperRecoveryRetry;
    }

    public void setZookeeperRecoveryRetry(String zookeeperRecoveryRetry) {
        this.zookeeperRecoveryRetry = zookeeperRecoveryRetry;
    }

    public String getZookeeperSessionTimeout() {
        return zookeeperSessionTimeout;
    }

    public void setZookeeperSessionTimeout(String zookeeperSessionTimeout) {
        this.zookeeperSessionTimeout = zookeeperSessionTimeout;
    }

    public String getZookeeperZnodeParent() {
        return zookeeperZnodeParent;
    }

    public void setZookeeperZnodeParent(String zookeeperZnodeParent) {
        this.zookeeperZnodeParent = zookeeperZnodeParent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Properties.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("atlasHookHbaseKeepAliveTime");
        sb.append('=');
        sb.append(((this.atlasHookHbaseKeepAliveTime == null)?"<null>":this.atlasHookHbaseKeepAliveTime));
        sb.append(',');
        sb.append("atlasHookHbaseMaxThreads");
        sb.append('=');
        sb.append(((this.atlasHookHbaseMaxThreads == null)?"<null>":this.atlasHookHbaseMaxThreads));
        sb.append(',');
        sb.append("atlasHookHbaseMinThreads");
        sb.append('=');
        sb.append(((this.atlasHookHbaseMinThreads == null)?"<null>":this.atlasHookHbaseMinThreads));
        sb.append(',');
        sb.append("atlasHookHbaseNumRetries");
        sb.append('=');
        sb.append(((this.atlasHookHbaseNumRetries == null)?"<null>":this.atlasHookHbaseNumRetries));
        sb.append(',');
        sb.append("atlasHookHbaseQueueSize");
        sb.append('=');
        sb.append(((this.atlasHookHbaseQueueSize == null)?"<null>":this.atlasHookHbaseQueueSize));
        sb.append(',');
        sb.append("atlasHookHbaseSynchronous");
        sb.append('=');
        sb.append(((this.atlasHookHbaseSynchronous == null)?"<null>":this.atlasHookHbaseSynchronous));
        sb.append(',');
        sb.append("content");
        sb.append('=');
        sb.append(((this.content == null)?"<null>":this.content));
        sb.append(',');
        sb.append("hbaseAtlasHook");
        sb.append('=');
        sb.append(((this.hbaseAtlasHook == null)?"<null>":this.hbaseAtlasHook));
        sb.append(',');
        sb.append("hbaseJavaIoTmpdir");
        sb.append('=');
        sb.append(((this.hbaseJavaIoTmpdir == null)?"<null>":this.hbaseJavaIoTmpdir));
        sb.append(',');
        sb.append("hbaseLogDir");
        sb.append('=');
        sb.append(((this.hbaseLogDir == null)?"<null>":this.hbaseLogDir));
        sb.append(',');
        sb.append("hbaseMasterHeapsize");
        sb.append('=');
        sb.append(((this.hbaseMasterHeapsize == null)?"<null>":this.hbaseMasterHeapsize));
        sb.append(',');
        sb.append("hbasePidDir");
        sb.append('=');
        sb.append(((this.hbasePidDir == null)?"<null>":this.hbasePidDir));
        sb.append(',');
        sb.append("hbaseRegionserverHeapsize");
        sb.append('=');
        sb.append(((this.hbaseRegionserverHeapsize == null)?"<null>":this.hbaseRegionserverHeapsize));
        sb.append(',');
        sb.append("hbaseRegionserverShutdownTimeout");
        sb.append('=');
        sb.append(((this.hbaseRegionserverShutdownTimeout == null)?"<null>":this.hbaseRegionserverShutdownTimeout));
        sb.append(',');
        sb.append("hbaseRegionserverXmnMax");
        sb.append('=');
        sb.append(((this.hbaseRegionserverXmnMax == null)?"<null>":this.hbaseRegionserverXmnMax));
        sb.append(',');
        sb.append("hbaseRegionserverXmnRatio");
        sb.append('=');
        sb.append(((this.hbaseRegionserverXmnRatio == null)?"<null>":this.hbaseRegionserverXmnRatio));
        sb.append(',');
        sb.append("hbaseUser");
        sb.append('=');
        sb.append(((this.hbaseUser == null)?"<null>":this.hbaseUser));
        sb.append(',');
        sb.append("hbaseUserNofileLimit");
        sb.append('=');
        sb.append(((this.hbaseUserNofileLimit == null)?"<null>":this.hbaseUserNofileLimit));
        sb.append(',');
        sb.append("hbaseUserNprocLimit");
        sb.append('=');
        sb.append(((this.hbaseUserNprocLimit == null)?"<null>":this.hbaseUserNprocLimit));
        sb.append(',');
        sb.append("phoenixSqlEnabled");
        sb.append('=');
        sb.append(((this.phoenixSqlEnabled == null)?"<null>":this.phoenixSqlEnabled));
        sb.append(',');
        sb.append("hbaseLogMaxbackupindex");
        sb.append('=');
        sb.append(((this.hbaseLogMaxbackupindex == null)?"<null>":this.hbaseLogMaxbackupindex));
        sb.append(',');
        sb.append("hbaseLogMaxfilesize");
        sb.append('=');
        sb.append(((this.hbaseLogMaxfilesize == null)?"<null>":this.hbaseLogMaxfilesize));
        sb.append(',');
        sb.append("hbaseSecurityLogMaxbackupindex");
        sb.append('=');
        sb.append(((this.hbaseSecurityLogMaxbackupindex == null)?"<null>":this.hbaseSecurityLogMaxbackupindex));
        sb.append(',');
        sb.append("hbaseSecurityLogMaxfilesize");
        sb.append('=');
        sb.append(((this.hbaseSecurityLogMaxfilesize == null)?"<null>":this.hbaseSecurityLogMaxfilesize));
        sb.append(',');
        sb.append("securityAdminProtocolAcl");
        sb.append('=');
        sb.append(((this.securityAdminProtocolAcl == null)?"<null>":this.securityAdminProtocolAcl));
        sb.append(',');
        sb.append("securityClientProtocolAcl");
        sb.append('=');
        sb.append(((this.securityClientProtocolAcl == null)?"<null>":this.securityClientProtocolAcl));
        sb.append(',');
        sb.append("securityMasterregionProtocolAcl");
        sb.append('=');
        sb.append(((this.securityMasterregionProtocolAcl == null)?"<null>":this.securityMasterregionProtocolAcl));
        sb.append(',');
        sb.append("dfsDomainSocketPath");
        sb.append('=');
        sb.append(((this.dfsDomainSocketPath == null)?"<null>":this.dfsDomainSocketPath));
        sb.append(',');
        sb.append("hbaseBulkloadStagingDir");
        sb.append('=');
        sb.append(((this.hbaseBulkloadStagingDir == null)?"<null>":this.hbaseBulkloadStagingDir));
        sb.append(',');
        sb.append("hbaseClientKeyvalueMaxsize");
        sb.append('=');
        sb.append(((this.hbaseClientKeyvalueMaxsize == null)?"<null>":this.hbaseClientKeyvalueMaxsize));
        sb.append(',');
        sb.append("hbaseClientRetriesNumber");
        sb.append('=');
        sb.append(((this.hbaseClientRetriesNumber == null)?"<null>":this.hbaseClientRetriesNumber));
        sb.append(',');
        sb.append("hbaseClientScannerCaching");
        sb.append('=');
        sb.append(((this.hbaseClientScannerCaching == null)?"<null>":this.hbaseClientScannerCaching));
        sb.append(',');
        sb.append("hbaseClusterDistributed");
        sb.append('=');
        sb.append(((this.hbaseClusterDistributed == null)?"<null>":this.hbaseClusterDistributed));
        sb.append(',');
        sb.append("hbaseCoprocessorMasterClasses");
        sb.append('=');
        sb.append(((this.hbaseCoprocessorMasterClasses == null)?"<null>":this.hbaseCoprocessorMasterClasses));
        sb.append(',');
        sb.append("hbaseCoprocessorRegionClasses");
        sb.append('=');
        sb.append(((this.hbaseCoprocessorRegionClasses == null)?"<null>":this.hbaseCoprocessorRegionClasses));
        sb.append(',');
        sb.append("hbaseDefaultsForVersionSkip");
        sb.append('=');
        sb.append(((this.hbaseDefaultsForVersionSkip == null)?"<null>":this.hbaseDefaultsForVersionSkip));
        sb.append(',');
        sb.append("hbaseHregionMajorcompaction");
        sb.append('=');
        sb.append(((this.hbaseHregionMajorcompaction == null)?"<null>":this.hbaseHregionMajorcompaction));
        sb.append(',');
        sb.append("hbaseHregionMajorcompactionJitter");
        sb.append('=');
        sb.append(((this.hbaseHregionMajorcompactionJitter == null)?"<null>":this.hbaseHregionMajorcompactionJitter));
        sb.append(',');
        sb.append("hbaseHregionMaxFilesize");
        sb.append('=');
        sb.append(((this.hbaseHregionMaxFilesize == null)?"<null>":this.hbaseHregionMaxFilesize));
        sb.append(',');
        sb.append("hbaseHregionMemstoreBlockMultiplier");
        sb.append('=');
        sb.append(((this.hbaseHregionMemstoreBlockMultiplier == null)?"<null>":this.hbaseHregionMemstoreBlockMultiplier));
        sb.append(',');
        sb.append("hbaseHregionMemstoreFlushSize");
        sb.append('=');
        sb.append(((this.hbaseHregionMemstoreFlushSize == null)?"<null>":this.hbaseHregionMemstoreFlushSize));
        sb.append(',');
        sb.append("hbaseHregionMemstoreMslabEnabled");
        sb.append('=');
        sb.append(((this.hbaseHregionMemstoreMslabEnabled == null)?"<null>":this.hbaseHregionMemstoreMslabEnabled));
        sb.append(',');
        sb.append("hbaseHstoreBlockingStoreFiles");
        sb.append('=');
        sb.append(((this.hbaseHstoreBlockingStoreFiles == null)?"<null>":this.hbaseHstoreBlockingStoreFiles));
        sb.append(',');
        sb.append("hbaseHstoreCompactionMax");
        sb.append('=');
        sb.append(((this.hbaseHstoreCompactionMax == null)?"<null>":this.hbaseHstoreCompactionMax));
        sb.append(',');
        sb.append("hbaseHstoreCompactionThreshold");
        sb.append('=');
        sb.append(((this.hbaseHstoreCompactionThreshold == null)?"<null>":this.hbaseHstoreCompactionThreshold));
        sb.append(',');
        sb.append("hbaseLocalDir");
        sb.append('=');
        sb.append(((this.hbaseLocalDir == null)?"<null>":this.hbaseLocalDir));
        sb.append(',');
        sb.append("hbaseMasterInfoBindAddress");
        sb.append('=');
        sb.append(((this.hbaseMasterInfoBindAddress == null)?"<null>":this.hbaseMasterInfoBindAddress));
        sb.append(',');
        sb.append("hbaseMasterInfoPort");
        sb.append('=');
        sb.append(((this.hbaseMasterInfoPort == null)?"<null>":this.hbaseMasterInfoPort));
        sb.append(',');
        sb.append("hbaseMasterNamespaceInitTimeout");
        sb.append('=');
        sb.append(((this.hbaseMasterNamespaceInitTimeout == null)?"<null>":this.hbaseMasterNamespaceInitTimeout));
        sb.append(',');
        sb.append("hbaseMasterPort");
        sb.append('=');
        sb.append(((this.hbaseMasterPort == null)?"<null>":this.hbaseMasterPort));
        sb.append(',');
        sb.append("hbaseMasterUiReadonly");
        sb.append('=');
        sb.append(((this.hbaseMasterUiReadonly == null)?"<null>":this.hbaseMasterUiReadonly));
        sb.append(',');
        sb.append("hbaseMasterWaitOnRegionserversTimeout");
        sb.append('=');
        sb.append(((this.hbaseMasterWaitOnRegionserversTimeout == null)?"<null>":this.hbaseMasterWaitOnRegionserversTimeout));
        sb.append(',');
        sb.append("hbaseRegionServerRpcSchedulerFactoryClass");
        sb.append('=');
        sb.append(((this.hbaseRegionServerRpcSchedulerFactoryClass == null)?"<null>":this.hbaseRegionServerRpcSchedulerFactoryClass));
        sb.append(',');
        sb.append("hbaseRegionserverExecutorOpenregionThreads");
        sb.append('=');
        sb.append(((this.hbaseRegionserverExecutorOpenregionThreads == null)?"<null>":this.hbaseRegionserverExecutorOpenregionThreads));
        sb.append(',');
        sb.append("hbaseRegionserverGlobalMemstoreSize");
        sb.append('=');
        sb.append(((this.hbaseRegionserverGlobalMemstoreSize == null)?"<null>":this.hbaseRegionserverGlobalMemstoreSize));
        sb.append(',');
        sb.append("hbaseRegionserverHandlerCount");
        sb.append('=');
        sb.append(((this.hbaseRegionserverHandlerCount == null)?"<null>":this.hbaseRegionserverHandlerCount));
        sb.append(',');
        sb.append("hbaseRegionserverInfoPort");
        sb.append('=');
        sb.append(((this.hbaseRegionserverInfoPort == null)?"<null>":this.hbaseRegionserverInfoPort));
        sb.append(',');
        sb.append("hbaseRegionserverPort");
        sb.append('=');
        sb.append(((this.hbaseRegionserverPort == null)?"<null>":this.hbaseRegionserverPort));
        sb.append(',');
        sb.append("hbaseRegionserverWalCodec");
        sb.append('=');
        sb.append(((this.hbaseRegionserverWalCodec == null)?"<null>":this.hbaseRegionserverWalCodec));
        sb.append(',');
        sb.append("hbaseRootdir");
        sb.append('=');
        sb.append(((this.hbaseRootdir == null)?"<null>":this.hbaseRootdir));
        sb.append(',');
        sb.append("hbaseRpcControllerfactoryClass");
        sb.append('=');
        sb.append(((this.hbaseRpcControllerfactoryClass == null)?"<null>":this.hbaseRpcControllerfactoryClass));
        sb.append(',');
        sb.append("hbaseRpcProtection");
        sb.append('=');
        sb.append(((this.hbaseRpcProtection == null)?"<null>":this.hbaseRpcProtection));
        sb.append(',');
        sb.append("hbaseRpcTimeout");
        sb.append('=');
        sb.append(((this.hbaseRpcTimeout == null)?"<null>":this.hbaseRpcTimeout));
        sb.append(',');
        sb.append("hbaseSecurityAuthentication");
        sb.append('=');
        sb.append(((this.hbaseSecurityAuthentication == null)?"<null>":this.hbaseSecurityAuthentication));
        sb.append(',');
        sb.append("hbaseSecurityAuthorization");
        sb.append('=');
        sb.append(((this.hbaseSecurityAuthorization == null)?"<null>":this.hbaseSecurityAuthorization));
        sb.append(',');
        sb.append("hbaseSuperuser");
        sb.append('=');
        sb.append(((this.hbaseSuperuser == null)?"<null>":this.hbaseSuperuser));
        sb.append(',');
        sb.append("hbaseTmpDir");
        sb.append('=');
        sb.append(((this.hbaseTmpDir == null)?"<null>":this.hbaseTmpDir));
        sb.append(',');
        sb.append("hbaseZookeeperPropertyClientPort");
        sb.append('=');
        sb.append(((this.hbaseZookeeperPropertyClientPort == null)?"<null>":this.hbaseZookeeperPropertyClientPort));
        sb.append(',');
        sb.append("hbaseZookeeperQuorum");
        sb.append('=');
        sb.append(((this.hbaseZookeeperQuorum == null)?"<null>":this.hbaseZookeeperQuorum));
        sb.append(',');
        sb.append("hbaseZookeeperUseMulti");
        sb.append('=');
        sb.append(((this.hbaseZookeeperUseMulti == null)?"<null>":this.hbaseZookeeperUseMulti));
        sb.append(',');
        sb.append("hfileBlockCacheSize");
        sb.append('=');
        sb.append(((this.hfileBlockCacheSize == null)?"<null>":this.hfileBlockCacheSize));
        sb.append(',');
        sb.append("phoenixFunctionsAllowUserDefinedFunctions");
        sb.append('=');
        sb.append(((this.phoenixFunctionsAllowUserDefinedFunctions == null)?"<null>":this.phoenixFunctionsAllowUserDefinedFunctions));
        sb.append(',');
        sb.append("phoenixQueryTimeoutMs");
        sb.append('=');
        sb.append(((this.phoenixQueryTimeoutMs == null)?"<null>":this.phoenixQueryTimeoutMs));
        sb.append(',');
        sb.append("zookeeperRecoveryRetry");
        sb.append('=');
        sb.append(((this.zookeeperRecoveryRetry == null)?"<null>":this.zookeeperRecoveryRetry));
        sb.append(',');
        sb.append("zookeeperSessionTimeout");
        sb.append('=');
        sb.append(((this.zookeeperSessionTimeout == null)?"<null>":this.zookeeperSessionTimeout));
        sb.append(',');
        sb.append("zookeeperZnodeParent");
        sb.append('=');
        sb.append(((this.zookeeperZnodeParent == null)?"<null>":this.zookeeperZnodeParent));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.hbaseZookeeperQuorum == null)? 0 :this.hbaseZookeeperQuorum.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMaxFilesize == null)? 0 :this.hbaseHregionMaxFilesize.hashCode()));
        result = ((result* 31)+((this.hbaseRegionServerRpcSchedulerFactoryClass == null)? 0 :this.hbaseRegionServerRpcSchedulerFactoryClass.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMemstoreMslabEnabled == null)? 0 :this.hbaseHregionMemstoreMslabEnabled.hashCode()));
        result = ((result* 31)+((this.hbaseHstoreBlockingStoreFiles == null)? 0 :this.hbaseHstoreBlockingStoreFiles.hashCode()));
        result = ((result* 31)+((this.securityAdminProtocolAcl == null)? 0 :this.securityAdminProtocolAcl.hashCode()));
        result = ((result* 31)+((this.hbaseHstoreCompactionThreshold == null)? 0 :this.hbaseHstoreCompactionThreshold.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverHandlerCount == null)? 0 :this.hbaseRegionserverHandlerCount.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseSynchronous == null)? 0 :this.atlasHookHbaseSynchronous.hashCode()));
        result = ((result* 31)+((this.hbaseUser == null)? 0 :this.hbaseUser.hashCode()));
        result = ((result* 31)+((this.phoenixQueryTimeoutMs == null)? 0 :this.phoenixQueryTimeoutMs.hashCode()));
        result = ((result* 31)+((this.hbaseLogMaxfilesize == null)? 0 :this.hbaseLogMaxfilesize.hashCode()));
        result = ((result* 31)+((this.hbaseSecurityLogMaxbackupindex == null)? 0 :this.hbaseSecurityLogMaxbackupindex.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMajorcompaction == null)? 0 :this.hbaseHregionMajorcompaction.hashCode()));
        result = ((result* 31)+((this.hbaseMasterUiReadonly == null)? 0 :this.hbaseMasterUiReadonly.hashCode()));
        result = ((result* 31)+((this.hbaseCoprocessorRegionClasses == null)? 0 :this.hbaseCoprocessorRegionClasses.hashCode()));
        result = ((result* 31)+((this.hbaseClientKeyvalueMaxsize == null)? 0 :this.hbaseClientKeyvalueMaxsize.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverHeapsize == null)? 0 :this.hbaseRegionserverHeapsize.hashCode()));
        result = ((result* 31)+((this.hbaseSecurityLogMaxfilesize == null)? 0 :this.hbaseSecurityLogMaxfilesize.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverXmnMax == null)? 0 :this.hbaseRegionserverXmnMax.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverWalCodec == null)? 0 :this.hbaseRegionserverWalCodec.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverXmnRatio == null)? 0 :this.hbaseRegionserverXmnRatio.hashCode()));
        result = ((result* 31)+((this.dfsDomainSocketPath == null)? 0 :this.dfsDomainSocketPath.hashCode()));
        result = ((result* 31)+((this.hbaseLogDir == null)? 0 :this.hbaseLogDir.hashCode()));
        result = ((result* 31)+((this.hbaseZookeeperPropertyClientPort == null)? 0 :this.hbaseZookeeperPropertyClientPort.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMemstoreFlushSize == null)? 0 :this.hbaseHregionMemstoreFlushSize.hashCode()));
        result = ((result* 31)+((this.zookeeperRecoveryRetry == null)? 0 :this.zookeeperRecoveryRetry.hashCode()));
        result = ((result* 31)+((this.hfileBlockCacheSize == null)? 0 :this.hfileBlockCacheSize.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseKeepAliveTime == null)? 0 :this.atlasHookHbaseKeepAliveTime.hashCode()));
        result = ((result* 31)+((this.hbaseUserNprocLimit == null)? 0 :this.hbaseUserNprocLimit.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseNumRetries == null)? 0 :this.atlasHookHbaseNumRetries.hashCode()));
        result = ((result* 31)+((this.hbaseLogMaxbackupindex == null)? 0 :this.hbaseLogMaxbackupindex.hashCode()));
        result = ((result* 31)+((this.hbaseRpcProtection == null)? 0 :this.hbaseRpcProtection.hashCode()));
        result = ((result* 31)+((this.hbaseZookeeperUseMulti == null)? 0 :this.hbaseZookeeperUseMulti.hashCode()));
        result = ((result* 31)+((this.hbaseSecurityAuthentication == null)? 0 :this.hbaseSecurityAuthentication.hashCode()));
        result = ((result* 31)+((this.hbaseSecurityAuthorization == null)? 0 :this.hbaseSecurityAuthorization.hashCode()));
        result = ((result* 31)+((this.hbaseHstoreCompactionMax == null)? 0 :this.hbaseHstoreCompactionMax.hashCode()));
        result = ((result* 31)+((this.content == null)? 0 :this.content.hashCode()));
        result = ((result* 31)+((this.hbaseMasterWaitOnRegionserversTimeout == null)? 0 :this.hbaseMasterWaitOnRegionserversTimeout.hashCode()));
        result = ((result* 31)+((this.zookeeperSessionTimeout == null)? 0 :this.zookeeperSessionTimeout.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverShutdownTimeout == null)? 0 :this.hbaseRegionserverShutdownTimeout.hashCode()));
        result = ((result* 31)+((this.hbaseMasterNamespaceInitTimeout == null)? 0 :this.hbaseMasterNamespaceInitTimeout.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseMaxThreads == null)? 0 :this.atlasHookHbaseMaxThreads.hashCode()));
        result = ((result* 31)+((this.phoenixFunctionsAllowUserDefinedFunctions == null)? 0 :this.phoenixFunctionsAllowUserDefinedFunctions.hashCode()));
        result = ((result* 31)+((this.hbaseClientRetriesNumber == null)? 0 :this.hbaseClientRetriesNumber.hashCode()));
        result = ((result* 31)+((this.hbaseMasterInfoBindAddress == null)? 0 :this.hbaseMasterInfoBindAddress.hashCode()));
        result = ((result* 31)+((this.hbaseJavaIoTmpdir == null)? 0 :this.hbaseJavaIoTmpdir.hashCode()));
        result = ((result* 31)+((this.hbaseMasterHeapsize == null)? 0 :this.hbaseMasterHeapsize.hashCode()));
        result = ((result* 31)+((this.securityClientProtocolAcl == null)? 0 :this.securityClientProtocolAcl.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseQueueSize == null)? 0 :this.atlasHookHbaseQueueSize.hashCode()));
        result = ((result* 31)+((this.hbaseAtlasHook == null)? 0 :this.hbaseAtlasHook.hashCode()));
        result = ((result* 31)+((this.hbaseRpcControllerfactoryClass == null)? 0 :this.hbaseRpcControllerfactoryClass.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverExecutorOpenregionThreads == null)? 0 :this.hbaseRegionserverExecutorOpenregionThreads.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverInfoPort == null)? 0 :this.hbaseRegionserverInfoPort.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverPort == null)? 0 :this.hbaseRegionserverPort.hashCode()));
        result = ((result* 31)+((this.hbaseUserNofileLimit == null)? 0 :this.hbaseUserNofileLimit.hashCode()));
        result = ((result* 31)+((this.hbaseDefaultsForVersionSkip == null)? 0 :this.hbaseDefaultsForVersionSkip.hashCode()));
        result = ((result* 31)+((this.hbaseMasterInfoPort == null)? 0 :this.hbaseMasterInfoPort.hashCode()));
        result = ((result* 31)+((this.hbaseMasterPort == null)? 0 :this.hbaseMasterPort.hashCode()));
        result = ((result* 31)+((this.phoenixSqlEnabled == null)? 0 :this.phoenixSqlEnabled.hashCode()));
        result = ((result* 31)+((this.hbaseClientScannerCaching == null)? 0 :this.hbaseClientScannerCaching.hashCode()));
        result = ((result* 31)+((this.hbaseRegionserverGlobalMemstoreSize == null)? 0 :this.hbaseRegionserverGlobalMemstoreSize.hashCode()));
        result = ((result* 31)+((this.securityMasterregionProtocolAcl == null)? 0 :this.securityMasterregionProtocolAcl.hashCode()));
        result = ((result* 31)+((this.hbaseCoprocessorMasterClasses == null)? 0 :this.hbaseCoprocessorMasterClasses.hashCode()));
        result = ((result* 31)+((this.hbaseRootdir == null)? 0 :this.hbaseRootdir.hashCode()));
        result = ((result* 31)+((this.zookeeperZnodeParent == null)? 0 :this.zookeeperZnodeParent.hashCode()));
        result = ((result* 31)+((this.hbaseBulkloadStagingDir == null)? 0 :this.hbaseBulkloadStagingDir.hashCode()));
        result = ((result* 31)+((this.hbaseClusterDistributed == null)? 0 :this.hbaseClusterDistributed.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMemstoreBlockMultiplier == null)? 0 :this.hbaseHregionMemstoreBlockMultiplier.hashCode()));
        result = ((result* 31)+((this.hbasePidDir == null)? 0 :this.hbasePidDir.hashCode()));
        result = ((result* 31)+((this.hbaseHregionMajorcompactionJitter == null)? 0 :this.hbaseHregionMajorcompactionJitter.hashCode()));
        result = ((result* 31)+((this.hbaseTmpDir == null)? 0 :this.hbaseTmpDir.hashCode()));
        result = ((result* 31)+((this.hbaseLocalDir == null)? 0 :this.hbaseLocalDir.hashCode()));
        result = ((result* 31)+((this.atlasHookHbaseMinThreads == null)? 0 :this.atlasHookHbaseMinThreads.hashCode()));
        result = ((result* 31)+((this.hbaseRpcTimeout == null)? 0 :this.hbaseRpcTimeout.hashCode()));
        result = ((result* 31)+((this.hbaseSuperuser == null)? 0 :this.hbaseSuperuser.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Properties) == false) {
            return false;
        }
        Properties rhs = ((Properties) other);
        return (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((this.hbaseZookeeperQuorum == rhs.hbaseZookeeperQuorum)||((this.hbaseZookeeperQuorum!= null)&&this.hbaseZookeeperQuorum.equals(rhs.hbaseZookeeperQuorum)))&&((this.hbaseHregionMaxFilesize == rhs.hbaseHregionMaxFilesize)||((this.hbaseHregionMaxFilesize!= null)&&this.hbaseHregionMaxFilesize.equals(rhs.hbaseHregionMaxFilesize))))&&((this.hbaseRegionServerRpcSchedulerFactoryClass == rhs.hbaseRegionServerRpcSchedulerFactoryClass)||((this.hbaseRegionServerRpcSchedulerFactoryClass!= null)&&this.hbaseRegionServerRpcSchedulerFactoryClass.equals(rhs.hbaseRegionServerRpcSchedulerFactoryClass))))&&((this.hbaseHregionMemstoreMslabEnabled == rhs.hbaseHregionMemstoreMslabEnabled)||((this.hbaseHregionMemstoreMslabEnabled!= null)&&this.hbaseHregionMemstoreMslabEnabled.equals(rhs.hbaseHregionMemstoreMslabEnabled))))&&((this.hbaseHstoreBlockingStoreFiles == rhs.hbaseHstoreBlockingStoreFiles)||((this.hbaseHstoreBlockingStoreFiles!= null)&&this.hbaseHstoreBlockingStoreFiles.equals(rhs.hbaseHstoreBlockingStoreFiles))))&&((this.securityAdminProtocolAcl == rhs.securityAdminProtocolAcl)||((this.securityAdminProtocolAcl!= null)&&this.securityAdminProtocolAcl.equals(rhs.securityAdminProtocolAcl))))&&((this.hbaseHstoreCompactionThreshold == rhs.hbaseHstoreCompactionThreshold)||((this.hbaseHstoreCompactionThreshold!= null)&&this.hbaseHstoreCompactionThreshold.equals(rhs.hbaseHstoreCompactionThreshold))))&&((this.hbaseRegionserverHandlerCount == rhs.hbaseRegionserverHandlerCount)||((this.hbaseRegionserverHandlerCount!= null)&&this.hbaseRegionserverHandlerCount.equals(rhs.hbaseRegionserverHandlerCount))))&&((this.atlasHookHbaseSynchronous == rhs.atlasHookHbaseSynchronous)||((this.atlasHookHbaseSynchronous!= null)&&this.atlasHookHbaseSynchronous.equals(rhs.atlasHookHbaseSynchronous))))&&((this.hbaseUser == rhs.hbaseUser)||((this.hbaseUser!= null)&&this.hbaseUser.equals(rhs.hbaseUser))))&&((this.phoenixQueryTimeoutMs == rhs.phoenixQueryTimeoutMs)||((this.phoenixQueryTimeoutMs!= null)&&this.phoenixQueryTimeoutMs.equals(rhs.phoenixQueryTimeoutMs))))&&((this.hbaseLogMaxfilesize == rhs.hbaseLogMaxfilesize)||((this.hbaseLogMaxfilesize!= null)&&this.hbaseLogMaxfilesize.equals(rhs.hbaseLogMaxfilesize))))&&((this.hbaseSecurityLogMaxbackupindex == rhs.hbaseSecurityLogMaxbackupindex)||((this.hbaseSecurityLogMaxbackupindex!= null)&&this.hbaseSecurityLogMaxbackupindex.equals(rhs.hbaseSecurityLogMaxbackupindex))))&&((this.hbaseHregionMajorcompaction == rhs.hbaseHregionMajorcompaction)||((this.hbaseHregionMajorcompaction!= null)&&this.hbaseHregionMajorcompaction.equals(rhs.hbaseHregionMajorcompaction))))&&((this.hbaseMasterUiReadonly == rhs.hbaseMasterUiReadonly)||((this.hbaseMasterUiReadonly!= null)&&this.hbaseMasterUiReadonly.equals(rhs.hbaseMasterUiReadonly))))&&((this.hbaseCoprocessorRegionClasses == rhs.hbaseCoprocessorRegionClasses)||((this.hbaseCoprocessorRegionClasses!= null)&&this.hbaseCoprocessorRegionClasses.equals(rhs.hbaseCoprocessorRegionClasses))))&&((this.hbaseClientKeyvalueMaxsize == rhs.hbaseClientKeyvalueMaxsize)||((this.hbaseClientKeyvalueMaxsize!= null)&&this.hbaseClientKeyvalueMaxsize.equals(rhs.hbaseClientKeyvalueMaxsize))))&&((this.hbaseRegionserverHeapsize == rhs.hbaseRegionserverHeapsize)||((this.hbaseRegionserverHeapsize!= null)&&this.hbaseRegionserverHeapsize.equals(rhs.hbaseRegionserverHeapsize))))&&((this.hbaseSecurityLogMaxfilesize == rhs.hbaseSecurityLogMaxfilesize)||((this.hbaseSecurityLogMaxfilesize!= null)&&this.hbaseSecurityLogMaxfilesize.equals(rhs.hbaseSecurityLogMaxfilesize))))&&((this.hbaseRegionserverXmnMax == rhs.hbaseRegionserverXmnMax)||((this.hbaseRegionserverXmnMax!= null)&&this.hbaseRegionserverXmnMax.equals(rhs.hbaseRegionserverXmnMax))))&&((this.hbaseRegionserverWalCodec == rhs.hbaseRegionserverWalCodec)||((this.hbaseRegionserverWalCodec!= null)&&this.hbaseRegionserverWalCodec.equals(rhs.hbaseRegionserverWalCodec))))&&((this.hbaseRegionserverXmnRatio == rhs.hbaseRegionserverXmnRatio)||((this.hbaseRegionserverXmnRatio!= null)&&this.hbaseRegionserverXmnRatio.equals(rhs.hbaseRegionserverXmnRatio))))&&((this.dfsDomainSocketPath == rhs.dfsDomainSocketPath)||((this.dfsDomainSocketPath!= null)&&this.dfsDomainSocketPath.equals(rhs.dfsDomainSocketPath))))&&((this.hbaseLogDir == rhs.hbaseLogDir)||((this.hbaseLogDir!= null)&&this.hbaseLogDir.equals(rhs.hbaseLogDir))))&&((this.hbaseZookeeperPropertyClientPort == rhs.hbaseZookeeperPropertyClientPort)||((this.hbaseZookeeperPropertyClientPort!= null)&&this.hbaseZookeeperPropertyClientPort.equals(rhs.hbaseZookeeperPropertyClientPort))))&&((this.hbaseHregionMemstoreFlushSize == rhs.hbaseHregionMemstoreFlushSize)||((this.hbaseHregionMemstoreFlushSize!= null)&&this.hbaseHregionMemstoreFlushSize.equals(rhs.hbaseHregionMemstoreFlushSize))))&&((this.zookeeperRecoveryRetry == rhs.zookeeperRecoveryRetry)||((this.zookeeperRecoveryRetry!= null)&&this.zookeeperRecoveryRetry.equals(rhs.zookeeperRecoveryRetry))))&&((this.hfileBlockCacheSize == rhs.hfileBlockCacheSize)||((this.hfileBlockCacheSize!= null)&&this.hfileBlockCacheSize.equals(rhs.hfileBlockCacheSize))))&&((this.atlasHookHbaseKeepAliveTime == rhs.atlasHookHbaseKeepAliveTime)||((this.atlasHookHbaseKeepAliveTime!= null)&&this.atlasHookHbaseKeepAliveTime.equals(rhs.atlasHookHbaseKeepAliveTime))))&&((this.hbaseUserNprocLimit == rhs.hbaseUserNprocLimit)||((this.hbaseUserNprocLimit!= null)&&this.hbaseUserNprocLimit.equals(rhs.hbaseUserNprocLimit))))&&((this.atlasHookHbaseNumRetries == rhs.atlasHookHbaseNumRetries)||((this.atlasHookHbaseNumRetries!= null)&&this.atlasHookHbaseNumRetries.equals(rhs.atlasHookHbaseNumRetries))))&&((this.hbaseLogMaxbackupindex == rhs.hbaseLogMaxbackupindex)||((this.hbaseLogMaxbackupindex!= null)&&this.hbaseLogMaxbackupindex.equals(rhs.hbaseLogMaxbackupindex))))&&((this.hbaseRpcProtection == rhs.hbaseRpcProtection)||((this.hbaseRpcProtection!= null)&&this.hbaseRpcProtection.equals(rhs.hbaseRpcProtection))))&&((this.hbaseZookeeperUseMulti == rhs.hbaseZookeeperUseMulti)||((this.hbaseZookeeperUseMulti!= null)&&this.hbaseZookeeperUseMulti.equals(rhs.hbaseZookeeperUseMulti))))&&((this.hbaseSecurityAuthentication == rhs.hbaseSecurityAuthentication)||((this.hbaseSecurityAuthentication!= null)&&this.hbaseSecurityAuthentication.equals(rhs.hbaseSecurityAuthentication))))&&((this.hbaseSecurityAuthorization == rhs.hbaseSecurityAuthorization)||((this.hbaseSecurityAuthorization!= null)&&this.hbaseSecurityAuthorization.equals(rhs.hbaseSecurityAuthorization))))&&((this.hbaseHstoreCompactionMax == rhs.hbaseHstoreCompactionMax)||((this.hbaseHstoreCompactionMax!= null)&&this.hbaseHstoreCompactionMax.equals(rhs.hbaseHstoreCompactionMax))))&&((this.content == rhs.content)||((this.content!= null)&&this.content.equals(rhs.content))))&&((this.hbaseMasterWaitOnRegionserversTimeout == rhs.hbaseMasterWaitOnRegionserversTimeout)||((this.hbaseMasterWaitOnRegionserversTimeout!= null)&&this.hbaseMasterWaitOnRegionserversTimeout.equals(rhs.hbaseMasterWaitOnRegionserversTimeout))))&&((this.zookeeperSessionTimeout == rhs.zookeeperSessionTimeout)||((this.zookeeperSessionTimeout!= null)&&this.zookeeperSessionTimeout.equals(rhs.zookeeperSessionTimeout))))&&((this.hbaseRegionserverShutdownTimeout == rhs.hbaseRegionserverShutdownTimeout)||((this.hbaseRegionserverShutdownTimeout!= null)&&this.hbaseRegionserverShutdownTimeout.equals(rhs.hbaseRegionserverShutdownTimeout))))&&((this.hbaseMasterNamespaceInitTimeout == rhs.hbaseMasterNamespaceInitTimeout)||((this.hbaseMasterNamespaceInitTimeout!= null)&&this.hbaseMasterNamespaceInitTimeout.equals(rhs.hbaseMasterNamespaceInitTimeout))))&&((this.atlasHookHbaseMaxThreads == rhs.atlasHookHbaseMaxThreads)||((this.atlasHookHbaseMaxThreads!= null)&&this.atlasHookHbaseMaxThreads.equals(rhs.atlasHookHbaseMaxThreads))))&&((this.phoenixFunctionsAllowUserDefinedFunctions == rhs.phoenixFunctionsAllowUserDefinedFunctions)||((this.phoenixFunctionsAllowUserDefinedFunctions!= null)&&this.phoenixFunctionsAllowUserDefinedFunctions.equals(rhs.phoenixFunctionsAllowUserDefinedFunctions))))&&((this.hbaseClientRetriesNumber == rhs.hbaseClientRetriesNumber)||((this.hbaseClientRetriesNumber!= null)&&this.hbaseClientRetriesNumber.equals(rhs.hbaseClientRetriesNumber))))&&((this.hbaseMasterInfoBindAddress == rhs.hbaseMasterInfoBindAddress)||((this.hbaseMasterInfoBindAddress!= null)&&this.hbaseMasterInfoBindAddress.equals(rhs.hbaseMasterInfoBindAddress))))&&((this.hbaseJavaIoTmpdir == rhs.hbaseJavaIoTmpdir)||((this.hbaseJavaIoTmpdir!= null)&&this.hbaseJavaIoTmpdir.equals(rhs.hbaseJavaIoTmpdir))))&&((this.hbaseMasterHeapsize == rhs.hbaseMasterHeapsize)||((this.hbaseMasterHeapsize!= null)&&this.hbaseMasterHeapsize.equals(rhs.hbaseMasterHeapsize))))&&((this.securityClientProtocolAcl == rhs.securityClientProtocolAcl)||((this.securityClientProtocolAcl!= null)&&this.securityClientProtocolAcl.equals(rhs.securityClientProtocolAcl))))&&((this.atlasHookHbaseQueueSize == rhs.atlasHookHbaseQueueSize)||((this.atlasHookHbaseQueueSize!= null)&&this.atlasHookHbaseQueueSize.equals(rhs.atlasHookHbaseQueueSize))))&&((this.hbaseAtlasHook == rhs.hbaseAtlasHook)||((this.hbaseAtlasHook!= null)&&this.hbaseAtlasHook.equals(rhs.hbaseAtlasHook))))&&((this.hbaseRpcControllerfactoryClass == rhs.hbaseRpcControllerfactoryClass)||((this.hbaseRpcControllerfactoryClass!= null)&&this.hbaseRpcControllerfactoryClass.equals(rhs.hbaseRpcControllerfactoryClass))))&&((this.hbaseRegionserverExecutorOpenregionThreads == rhs.hbaseRegionserverExecutorOpenregionThreads)||((this.hbaseRegionserverExecutorOpenregionThreads!= null)&&this.hbaseRegionserverExecutorOpenregionThreads.equals(rhs.hbaseRegionserverExecutorOpenregionThreads))))&&((this.hbaseRegionserverInfoPort == rhs.hbaseRegionserverInfoPort)||((this.hbaseRegionserverInfoPort!= null)&&this.hbaseRegionserverInfoPort.equals(rhs.hbaseRegionserverInfoPort))))&&((this.hbaseRegionserverPort == rhs.hbaseRegionserverPort)||((this.hbaseRegionserverPort!= null)&&this.hbaseRegionserverPort.equals(rhs.hbaseRegionserverPort))))&&((this.hbaseUserNofileLimit == rhs.hbaseUserNofileLimit)||((this.hbaseUserNofileLimit!= null)&&this.hbaseUserNofileLimit.equals(rhs.hbaseUserNofileLimit))))&&((this.hbaseDefaultsForVersionSkip == rhs.hbaseDefaultsForVersionSkip)||((this.hbaseDefaultsForVersionSkip!= null)&&this.hbaseDefaultsForVersionSkip.equals(rhs.hbaseDefaultsForVersionSkip))))&&((this.hbaseMasterInfoPort == rhs.hbaseMasterInfoPort)||((this.hbaseMasterInfoPort!= null)&&this.hbaseMasterInfoPort.equals(rhs.hbaseMasterInfoPort))))&&((this.hbaseMasterPort == rhs.hbaseMasterPort)||((this.hbaseMasterPort!= null)&&this.hbaseMasterPort.equals(rhs.hbaseMasterPort))))&&((this.phoenixSqlEnabled == rhs.phoenixSqlEnabled)||((this.phoenixSqlEnabled!= null)&&this.phoenixSqlEnabled.equals(rhs.phoenixSqlEnabled))))&&((this.hbaseClientScannerCaching == rhs.hbaseClientScannerCaching)||((this.hbaseClientScannerCaching!= null)&&this.hbaseClientScannerCaching.equals(rhs.hbaseClientScannerCaching))))&&((this.hbaseRegionserverGlobalMemstoreSize == rhs.hbaseRegionserverGlobalMemstoreSize)||((this.hbaseRegionserverGlobalMemstoreSize!= null)&&this.hbaseRegionserverGlobalMemstoreSize.equals(rhs.hbaseRegionserverGlobalMemstoreSize))))&&((this.securityMasterregionProtocolAcl == rhs.securityMasterregionProtocolAcl)||((this.securityMasterregionProtocolAcl!= null)&&this.securityMasterregionProtocolAcl.equals(rhs.securityMasterregionProtocolAcl))))&&((this.hbaseCoprocessorMasterClasses == rhs.hbaseCoprocessorMasterClasses)||((this.hbaseCoprocessorMasterClasses!= null)&&this.hbaseCoprocessorMasterClasses.equals(rhs.hbaseCoprocessorMasterClasses))))&&((this.hbaseRootdir == rhs.hbaseRootdir)||((this.hbaseRootdir!= null)&&this.hbaseRootdir.equals(rhs.hbaseRootdir))))&&((this.zookeeperZnodeParent == rhs.zookeeperZnodeParent)||((this.zookeeperZnodeParent!= null)&&this.zookeeperZnodeParent.equals(rhs.zookeeperZnodeParent))))&&((this.hbaseBulkloadStagingDir == rhs.hbaseBulkloadStagingDir)||((this.hbaseBulkloadStagingDir!= null)&&this.hbaseBulkloadStagingDir.equals(rhs.hbaseBulkloadStagingDir))))&&((this.hbaseClusterDistributed == rhs.hbaseClusterDistributed)||((this.hbaseClusterDistributed!= null)&&this.hbaseClusterDistributed.equals(rhs.hbaseClusterDistributed))))&&((this.hbaseHregionMemstoreBlockMultiplier == rhs.hbaseHregionMemstoreBlockMultiplier)||((this.hbaseHregionMemstoreBlockMultiplier!= null)&&this.hbaseHregionMemstoreBlockMultiplier.equals(rhs.hbaseHregionMemstoreBlockMultiplier))))&&((this.hbasePidDir == rhs.hbasePidDir)||((this.hbasePidDir!= null)&&this.hbasePidDir.equals(rhs.hbasePidDir))))&&((this.hbaseHregionMajorcompactionJitter == rhs.hbaseHregionMajorcompactionJitter)||((this.hbaseHregionMajorcompactionJitter!= null)&&this.hbaseHregionMajorcompactionJitter.equals(rhs.hbaseHregionMajorcompactionJitter))))&&((this.hbaseTmpDir == rhs.hbaseTmpDir)||((this.hbaseTmpDir!= null)&&this.hbaseTmpDir.equals(rhs.hbaseTmpDir))))&&((this.hbaseLocalDir == rhs.hbaseLocalDir)||((this.hbaseLocalDir!= null)&&this.hbaseLocalDir.equals(rhs.hbaseLocalDir))))&&((this.atlasHookHbaseMinThreads == rhs.atlasHookHbaseMinThreads)||((this.atlasHookHbaseMinThreads!= null)&&this.atlasHookHbaseMinThreads.equals(rhs.atlasHookHbaseMinThreads))))&&((this.hbaseRpcTimeout == rhs.hbaseRpcTimeout)||((this.hbaseRpcTimeout!= null)&&this.hbaseRpcTimeout.equals(rhs.hbaseRpcTimeout))))&&((this.hbaseSuperuser == rhs.hbaseSuperuser)||((this.hbaseSuperuser!= null)&&this.hbaseSuperuser.equals(rhs.hbaseSuperuser))));
    }

}
