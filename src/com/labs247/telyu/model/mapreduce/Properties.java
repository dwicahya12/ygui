package com.labs247.telyu.model.mapreduce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Properties {

    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("jobhistory_heapsize")
    @Expose
    private String jobhistoryHeapsize;
    @SerializedName("mapred_log_dir_prefix")
    @Expose
    private String mapredLogDirPrefix;
    @SerializedName("mapred_pid_dir_prefix")
    @Expose
    private String mapredPidDirPrefix;
    @SerializedName("mapred_user")
    @Expose
    private String mapredUser;
    @SerializedName("mapred_user_nofile_limit")
    @Expose
    private String mapredUserNofileLimit;
    @SerializedName("mapred_user_nproc_limit")
    @Expose
    private String mapredUserNprocLimit;
    @SerializedName("mapred.local.dir")
    @Expose
    private String mapredLocalDir;
    @SerializedName("mapreduce.admin.map.child.java.opts")
    @Expose
    private String mapreduceAdminMapChildJavaOpts;
    @SerializedName("mapreduce.admin.reduce.child.java.opts")
    @Expose
    private String mapreduceAdminReduceChildJavaOpts;
    @SerializedName("mapreduce.admin.user.env")
    @Expose
    private String mapreduceAdminUserEnv;
    @SerializedName("mapreduce.am.max-attempts")
    @Expose
    private String mapreduceAmMaxAttempts;
    @SerializedName("mapreduce.application.classpath")
    @Expose
    private String mapreduceApplicationClasspath;
    @SerializedName("mapreduce.application.framework.path")
    @Expose
    private String mapreduceApplicationFrameworkPath;
    @SerializedName("mapreduce.cluster.acls.enabled")
    @Expose
    private String mapreduceClusterAclsEnabled;
    @SerializedName("mapreduce.cluster.administrators")
    @Expose
    private String mapreduceClusterAdministrators;
    @SerializedName("mapreduce.framework.name")
    @Expose
    private String mapreduceFrameworkName;
    @SerializedName("mapreduce.job.acl-modify-job")
    @Expose
    private String mapreduceJobAclModifyJob;
    @SerializedName("mapreduce.job.acl-view-job")
    @Expose
    private String mapreduceJobAclViewJob;
    @SerializedName("mapreduce.job.counters.max")
    @Expose
    private String mapreduceJobCountersMax;
    @SerializedName("mapreduce.job.emit-timeline-data")
    @Expose
    private String mapreduceJobEmitTimelineData;
    @SerializedName("mapreduce.job.queuename")
    @Expose
    private String mapreduceJobQueuename;
    @SerializedName("mapreduce.job.reduce.slowstart.completedmaps")
    @Expose
    private String mapreduceJobReduceSlowstartCompletedmaps;
    @SerializedName("mapreduce.jobhistory.address")
    @Expose
    private String mapreduceJobhistoryAddress;
    @SerializedName("mapreduce.jobhistory.admin.acl")
    @Expose
    private String mapreduceJobhistoryAdminAcl;
    @SerializedName("mapreduce.jobhistory.bind-host")
    @Expose
    private String mapreduceJobhistoryBindHost;
    @SerializedName("mapreduce.jobhistory.done-dir")
    @Expose
    private String mapreduceJobhistoryDoneDir;
    @SerializedName("mapreduce.jobhistory.http.policy")
    @Expose
    private String mapreduceJobhistoryHttpPolicy;
    @SerializedName("mapreduce.jobhistory.intermediate-done-dir")
    @Expose
    private String mapreduceJobhistoryIntermediateDoneDir;
    @SerializedName("mapreduce.jobhistory.recovery.enable")
    @Expose
    private String mapreduceJobhistoryRecoveryEnable;
    @SerializedName("mapreduce.jobhistory.recovery.store.class")
    @Expose
    private String mapreduceJobhistoryRecoveryStoreClass;
    @SerializedName("mapreduce.jobhistory.recovery.store.leveldb.path")
    @Expose
    private String mapreduceJobhistoryRecoveryStoreLeveldbPath;
    @SerializedName("mapreduce.jobhistory.webapp.address")
    @Expose
    private String mapreduceJobhistoryWebappAddress;
    @SerializedName("mapreduce.map.java.opts")
    @Expose
    private String mapreduceMapJavaOpts;
    @SerializedName("mapreduce.map.log.level")
    @Expose
    private String mapreduceMapLogLevel;
    @SerializedName("mapreduce.map.memory.mb")
    @Expose
    private String mapreduceMapMemoryMb;
    @SerializedName("mapreduce.map.output.compress")
    @Expose
    private String mapreduceMapOutputCompress;
    @SerializedName("mapreduce.map.sort.spill.percent")
    @Expose
    private String mapreduceMapSortSpillPercent;
    @SerializedName("mapreduce.map.speculative")
    @Expose
    private String mapreduceMapSpeculative;
    @SerializedName("mapreduce.output.fileoutputformat.compress")
    @Expose
    private String mapreduceOutputFileoutputformatCompress;
    @SerializedName("mapreduce.output.fileoutputformat.compress.type")
    @Expose
    private String mapreduceOutputFileoutputformatCompressType;
    @SerializedName("mapreduce.reduce.input.buffer.percent")
    @Expose
    private String mapreduceReduceInputBufferPercent;
    @SerializedName("mapreduce.reduce.java.opts")
    @Expose
    private String mapreduceReduceJavaOpts;
    @SerializedName("mapreduce.reduce.log.level")
    @Expose
    private String mapreduceReduceLogLevel;
    @SerializedName("mapreduce.reduce.memory.mb")
    @Expose
    private String mapreduceReduceMemoryMb;
    @SerializedName("mapreduce.reduce.shuffle.fetch.retry.enabled")
    @Expose
    private String mapreduceReduceShuffleFetchRetryEnabled;
    @SerializedName("mapreduce.reduce.shuffle.fetch.retry.interval-ms")
    @Expose
    private String mapreduceReduceShuffleFetchRetryIntervalMs;
    @SerializedName("mapreduce.reduce.shuffle.fetch.retry.timeout-ms")
    @Expose
    private String mapreduceReduceShuffleFetchRetryTimeoutMs;
    @SerializedName("mapreduce.reduce.shuffle.input.buffer.percent")
    @Expose
    private String mapreduceReduceShuffleInputBufferPercent;
    @SerializedName("mapreduce.reduce.shuffle.merge.percent")
    @Expose
    private String mapreduceReduceShuffleMergePercent;
    @SerializedName("mapreduce.reduce.shuffle.parallelcopies")
    @Expose
    private String mapreduceReduceShuffleParallelcopies;
    @SerializedName("mapreduce.reduce.speculative")
    @Expose
    private String mapreduceReduceSpeculative;
    @SerializedName("mapreduce.shuffle.port")
    @Expose
    private String mapreduceShufflePort;
    @SerializedName("mapreduce.task.io.sort.factor")
    @Expose
    private String mapreduceTaskIoSortFactor;
    @SerializedName("mapreduce.task.io.sort.mb")
    @Expose
    private String mapreduceTaskIoSortMb;
    @SerializedName("mapreduce.task.timeout")
    @Expose
    private String mapreduceTaskTimeout;
    @SerializedName("yarn.app.mapreduce.am.admin-command-opts")
    @Expose
    private String yarnAppMapreduceAmAdminCommandOpts;
    @SerializedName("yarn.app.mapreduce.am.command-opts")
    @Expose
    private String yarnAppMapreduceAmCommandOpts;
    @SerializedName("yarn.app.mapreduce.am.log.level")
    @Expose
    private String yarnAppMapreduceAmLogLevel;
    @SerializedName("yarn.app.mapreduce.am.resource.mb")
    @Expose
    private String yarnAppMapreduceAmResourceMb;
    @SerializedName("yarn.app.mapreduce.am.staging-dir")
    @Expose
    private String yarnAppMapreduceAmStagingDir;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getJobhistoryHeapsize() {
        return jobhistoryHeapsize;
    }

    public void setJobhistoryHeapsize(String jobhistoryHeapsize) {
        this.jobhistoryHeapsize = jobhistoryHeapsize;
    }

    public String getMapredLogDirPrefix() {
        return mapredLogDirPrefix;
    }

    public void setMapredLogDirPrefix(String mapredLogDirPrefix) {
        this.mapredLogDirPrefix = mapredLogDirPrefix;
    }

    public String getMapredPidDirPrefix() {
        return mapredPidDirPrefix;
    }

    public void setMapredPidDirPrefix(String mapredPidDirPrefix) {
        this.mapredPidDirPrefix = mapredPidDirPrefix;
    }

    public String getMapredUser() {
        return mapredUser;
    }

    public void setMapredUser(String mapredUser) {
        this.mapredUser = mapredUser;
    }

    public String getMapredUserNofileLimit() {
        return mapredUserNofileLimit;
    }

    public void setMapredUserNofileLimit(String mapredUserNofileLimit) {
        this.mapredUserNofileLimit = mapredUserNofileLimit;
    }

    public String getMapredUserNprocLimit() {
        return mapredUserNprocLimit;
    }

    public void setMapredUserNprocLimit(String mapredUserNprocLimit) {
        this.mapredUserNprocLimit = mapredUserNprocLimit;
    }

    public String getMapredLocalDir() {
        return mapredLocalDir;
    }

    public void setMapredLocalDir(String mapredLocalDir) {
        this.mapredLocalDir = mapredLocalDir;
    }

    public String getMapreduceAdminMapChildJavaOpts() {
        return mapreduceAdminMapChildJavaOpts;
    }

    public void setMapreduceAdminMapChildJavaOpts(String mapreduceAdminMapChildJavaOpts) {
        this.mapreduceAdminMapChildJavaOpts = mapreduceAdminMapChildJavaOpts;
    }

    public String getMapreduceAdminReduceChildJavaOpts() {
        return mapreduceAdminReduceChildJavaOpts;
    }

    public void setMapreduceAdminReduceChildJavaOpts(String mapreduceAdminReduceChildJavaOpts) {
        this.mapreduceAdminReduceChildJavaOpts = mapreduceAdminReduceChildJavaOpts;
    }

    public String getMapreduceAdminUserEnv() {
        return mapreduceAdminUserEnv;
    }

    public void setMapreduceAdminUserEnv(String mapreduceAdminUserEnv) {
        this.mapreduceAdminUserEnv = mapreduceAdminUserEnv;
    }

    public String getMapreduceAmMaxAttempts() {
        return mapreduceAmMaxAttempts;
    }

    public void setMapreduceAmMaxAttempts(String mapreduceAmMaxAttempts) {
        this.mapreduceAmMaxAttempts = mapreduceAmMaxAttempts;
    }

    public String getMapreduceApplicationClasspath() {
        return mapreduceApplicationClasspath;
    }

    public void setMapreduceApplicationClasspath(String mapreduceApplicationClasspath) {
        this.mapreduceApplicationClasspath = mapreduceApplicationClasspath;
    }

    public String getMapreduceApplicationFrameworkPath() {
        return mapreduceApplicationFrameworkPath;
    }

    public void setMapreduceApplicationFrameworkPath(String mapreduceApplicationFrameworkPath) {
        this.mapreduceApplicationFrameworkPath = mapreduceApplicationFrameworkPath;
    }

    public String getMapreduceClusterAclsEnabled() {
        return mapreduceClusterAclsEnabled;
    }

    public void setMapreduceClusterAclsEnabled(String mapreduceClusterAclsEnabled) {
        this.mapreduceClusterAclsEnabled = mapreduceClusterAclsEnabled;
    }

    public String getMapreduceClusterAdministrators() {
        return mapreduceClusterAdministrators;
    }

    public void setMapreduceClusterAdministrators(String mapreduceClusterAdministrators) {
        this.mapreduceClusterAdministrators = mapreduceClusterAdministrators;
    }

    public String getMapreduceFrameworkName() {
        return mapreduceFrameworkName;
    }

    public void setMapreduceFrameworkName(String mapreduceFrameworkName) {
        this.mapreduceFrameworkName = mapreduceFrameworkName;
    }

    public String getMapreduceJobAclModifyJob() {
        return mapreduceJobAclModifyJob;
    }

    public void setMapreduceJobAclModifyJob(String mapreduceJobAclModifyJob) {
        this.mapreduceJobAclModifyJob = mapreduceJobAclModifyJob;
    }

    public String getMapreduceJobAclViewJob() {
        return mapreduceJobAclViewJob;
    }

    public void setMapreduceJobAclViewJob(String mapreduceJobAclViewJob) {
        this.mapreduceJobAclViewJob = mapreduceJobAclViewJob;
    }

    public String getMapreduceJobCountersMax() {
        return mapreduceJobCountersMax;
    }

    public void setMapreduceJobCountersMax(String mapreduceJobCountersMax) {
        this.mapreduceJobCountersMax = mapreduceJobCountersMax;
    }

    public String getMapreduceJobEmitTimelineData() {
        return mapreduceJobEmitTimelineData;
    }

    public void setMapreduceJobEmitTimelineData(String mapreduceJobEmitTimelineData) {
        this.mapreduceJobEmitTimelineData = mapreduceJobEmitTimelineData;
    }

    public String getMapreduceJobQueuename() {
        return mapreduceJobQueuename;
    }

    public void setMapreduceJobQueuename(String mapreduceJobQueuename) {
        this.mapreduceJobQueuename = mapreduceJobQueuename;
    }

    public String getMapreduceJobReduceSlowstartCompletedmaps() {
        return mapreduceJobReduceSlowstartCompletedmaps;
    }

    public void setMapreduceJobReduceSlowstartCompletedmaps(String mapreduceJobReduceSlowstartCompletedmaps) {
        this.mapreduceJobReduceSlowstartCompletedmaps = mapreduceJobReduceSlowstartCompletedmaps;
    }

    public String getMapreduceJobhistoryAddress() {
        return mapreduceJobhistoryAddress;
    }

    public void setMapreduceJobhistoryAddress(String mapreduceJobhistoryAddress) {
        this.mapreduceJobhistoryAddress = mapreduceJobhistoryAddress;
    }

    public String getMapreduceJobhistoryAdminAcl() {
        return mapreduceJobhistoryAdminAcl;
    }

    public void setMapreduceJobhistoryAdminAcl(String mapreduceJobhistoryAdminAcl) {
        this.mapreduceJobhistoryAdminAcl = mapreduceJobhistoryAdminAcl;
    }

    public String getMapreduceJobhistoryBindHost() {
        return mapreduceJobhistoryBindHost;
    }

    public void setMapreduceJobhistoryBindHost(String mapreduceJobhistoryBindHost) {
        this.mapreduceJobhistoryBindHost = mapreduceJobhistoryBindHost;
    }

    public String getMapreduceJobhistoryDoneDir() {
        return mapreduceJobhistoryDoneDir;
    }

    public void setMapreduceJobhistoryDoneDir(String mapreduceJobhistoryDoneDir) {
        this.mapreduceJobhistoryDoneDir = mapreduceJobhistoryDoneDir;
    }

    public String getMapreduceJobhistoryHttpPolicy() {
        return mapreduceJobhistoryHttpPolicy;
    }

    public void setMapreduceJobhistoryHttpPolicy(String mapreduceJobhistoryHttpPolicy) {
        this.mapreduceJobhistoryHttpPolicy = mapreduceJobhistoryHttpPolicy;
    }

    public String getMapreduceJobhistoryIntermediateDoneDir() {
        return mapreduceJobhistoryIntermediateDoneDir;
    }

    public void setMapreduceJobhistoryIntermediateDoneDir(String mapreduceJobhistoryIntermediateDoneDir) {
        this.mapreduceJobhistoryIntermediateDoneDir = mapreduceJobhistoryIntermediateDoneDir;
    }

    public String getMapreduceJobhistoryRecoveryEnable() {
        return mapreduceJobhistoryRecoveryEnable;
    }

    public void setMapreduceJobhistoryRecoveryEnable(String mapreduceJobhistoryRecoveryEnable) {
        this.mapreduceJobhistoryRecoveryEnable = mapreduceJobhistoryRecoveryEnable;
    }

    public String getMapreduceJobhistoryRecoveryStoreClass() {
        return mapreduceJobhistoryRecoveryStoreClass;
    }

    public void setMapreduceJobhistoryRecoveryStoreClass(String mapreduceJobhistoryRecoveryStoreClass) {
        this.mapreduceJobhistoryRecoveryStoreClass = mapreduceJobhistoryRecoveryStoreClass;
    }

    public String getMapreduceJobhistoryRecoveryStoreLeveldbPath() {
        return mapreduceJobhistoryRecoveryStoreLeveldbPath;
    }

    public void setMapreduceJobhistoryRecoveryStoreLeveldbPath(String mapreduceJobhistoryRecoveryStoreLeveldbPath) {
        this.mapreduceJobhistoryRecoveryStoreLeveldbPath = mapreduceJobhistoryRecoveryStoreLeveldbPath;
    }

    public String getMapreduceJobhistoryWebappAddress() {
        return mapreduceJobhistoryWebappAddress;
    }

    public void setMapreduceJobhistoryWebappAddress(String mapreduceJobhistoryWebappAddress) {
        this.mapreduceJobhistoryWebappAddress = mapreduceJobhistoryWebappAddress;
    }

    public String getMapreduceMapJavaOpts() {
        return mapreduceMapJavaOpts;
    }

    public void setMapreduceMapJavaOpts(String mapreduceMapJavaOpts) {
        this.mapreduceMapJavaOpts = mapreduceMapJavaOpts;
    }

    public String getMapreduceMapLogLevel() {
        return mapreduceMapLogLevel;
    }

    public void setMapreduceMapLogLevel(String mapreduceMapLogLevel) {
        this.mapreduceMapLogLevel = mapreduceMapLogLevel;
    }

    public String getMapreduceMapMemoryMb() {
        return mapreduceMapMemoryMb;
    }

    public void setMapreduceMapMemoryMb(String mapreduceMapMemoryMb) {
        this.mapreduceMapMemoryMb = mapreduceMapMemoryMb;
    }

    public String getMapreduceMapOutputCompress() {
        return mapreduceMapOutputCompress;
    }

    public void setMapreduceMapOutputCompress(String mapreduceMapOutputCompress) {
        this.mapreduceMapOutputCompress = mapreduceMapOutputCompress;
    }

    public String getMapreduceMapSortSpillPercent() {
        return mapreduceMapSortSpillPercent;
    }

    public void setMapreduceMapSortSpillPercent(String mapreduceMapSortSpillPercent) {
        this.mapreduceMapSortSpillPercent = mapreduceMapSortSpillPercent;
    }

    public String getMapreduceMapSpeculative() {
        return mapreduceMapSpeculative;
    }

    public void setMapreduceMapSpeculative(String mapreduceMapSpeculative) {
        this.mapreduceMapSpeculative = mapreduceMapSpeculative;
    }

    public String getMapreduceOutputFileoutputformatCompress() {
        return mapreduceOutputFileoutputformatCompress;
    }

    public void setMapreduceOutputFileoutputformatCompress(String mapreduceOutputFileoutputformatCompress) {
        this.mapreduceOutputFileoutputformatCompress = mapreduceOutputFileoutputformatCompress;
    }

    public String getMapreduceOutputFileoutputformatCompressType() {
        return mapreduceOutputFileoutputformatCompressType;
    }

    public void setMapreduceOutputFileoutputformatCompressType(String mapreduceOutputFileoutputformatCompressType) {
        this.mapreduceOutputFileoutputformatCompressType = mapreduceOutputFileoutputformatCompressType;
    }

    public String getMapreduceReduceInputBufferPercent() {
        return mapreduceReduceInputBufferPercent;
    }

    public void setMapreduceReduceInputBufferPercent(String mapreduceReduceInputBufferPercent) {
        this.mapreduceReduceInputBufferPercent = mapreduceReduceInputBufferPercent;
    }

    public String getMapreduceReduceJavaOpts() {
        return mapreduceReduceJavaOpts;
    }

    public void setMapreduceReduceJavaOpts(String mapreduceReduceJavaOpts) {
        this.mapreduceReduceJavaOpts = mapreduceReduceJavaOpts;
    }

    public String getMapreduceReduceLogLevel() {
        return mapreduceReduceLogLevel;
    }

    public void setMapreduceReduceLogLevel(String mapreduceReduceLogLevel) {
        this.mapreduceReduceLogLevel = mapreduceReduceLogLevel;
    }

    public String getMapreduceReduceMemoryMb() {
        return mapreduceReduceMemoryMb;
    }

    public void setMapreduceReduceMemoryMb(String mapreduceReduceMemoryMb) {
        this.mapreduceReduceMemoryMb = mapreduceReduceMemoryMb;
    }

    public String getMapreduceReduceShuffleFetchRetryEnabled() {
        return mapreduceReduceShuffleFetchRetryEnabled;
    }

    public void setMapreduceReduceShuffleFetchRetryEnabled(String mapreduceReduceShuffleFetchRetryEnabled) {
        this.mapreduceReduceShuffleFetchRetryEnabled = mapreduceReduceShuffleFetchRetryEnabled;
    }

    public String getMapreduceReduceShuffleFetchRetryIntervalMs() {
        return mapreduceReduceShuffleFetchRetryIntervalMs;
    }

    public void setMapreduceReduceShuffleFetchRetryIntervalMs(String mapreduceReduceShuffleFetchRetryIntervalMs) {
        this.mapreduceReduceShuffleFetchRetryIntervalMs = mapreduceReduceShuffleFetchRetryIntervalMs;
    }

    public String getMapreduceReduceShuffleFetchRetryTimeoutMs() {
        return mapreduceReduceShuffleFetchRetryTimeoutMs;
    }

    public void setMapreduceReduceShuffleFetchRetryTimeoutMs(String mapreduceReduceShuffleFetchRetryTimeoutMs) {
        this.mapreduceReduceShuffleFetchRetryTimeoutMs = mapreduceReduceShuffleFetchRetryTimeoutMs;
    }

    public String getMapreduceReduceShuffleInputBufferPercent() {
        return mapreduceReduceShuffleInputBufferPercent;
    }

    public void setMapreduceReduceShuffleInputBufferPercent(String mapreduceReduceShuffleInputBufferPercent) {
        this.mapreduceReduceShuffleInputBufferPercent = mapreduceReduceShuffleInputBufferPercent;
    }

    public String getMapreduceReduceShuffleMergePercent() {
        return mapreduceReduceShuffleMergePercent;
    }

    public void setMapreduceReduceShuffleMergePercent(String mapreduceReduceShuffleMergePercent) {
        this.mapreduceReduceShuffleMergePercent = mapreduceReduceShuffleMergePercent;
    }

    public String getMapreduceReduceShuffleParallelcopies() {
        return mapreduceReduceShuffleParallelcopies;
    }

    public void setMapreduceReduceShuffleParallelcopies(String mapreduceReduceShuffleParallelcopies) {
        this.mapreduceReduceShuffleParallelcopies = mapreduceReduceShuffleParallelcopies;
    }

    public String getMapreduceReduceSpeculative() {
        return mapreduceReduceSpeculative;
    }

    public void setMapreduceReduceSpeculative(String mapreduceReduceSpeculative) {
        this.mapreduceReduceSpeculative = mapreduceReduceSpeculative;
    }

    public String getMapreduceShufflePort() {
        return mapreduceShufflePort;
    }

    public void setMapreduceShufflePort(String mapreduceShufflePort) {
        this.mapreduceShufflePort = mapreduceShufflePort;
    }

    public String getMapreduceTaskIoSortFactor() {
        return mapreduceTaskIoSortFactor;
    }

    public void setMapreduceTaskIoSortFactor(String mapreduceTaskIoSortFactor) {
        this.mapreduceTaskIoSortFactor = mapreduceTaskIoSortFactor;
    }

    public String getMapreduceTaskIoSortMb() {
        return mapreduceTaskIoSortMb;
    }

    public void setMapreduceTaskIoSortMb(String mapreduceTaskIoSortMb) {
        this.mapreduceTaskIoSortMb = mapreduceTaskIoSortMb;
    }

    public String getMapreduceTaskTimeout() {
        return mapreduceTaskTimeout;
    }

    public void setMapreduceTaskTimeout(String mapreduceTaskTimeout) {
        this.mapreduceTaskTimeout = mapreduceTaskTimeout;
    }

    public String getYarnAppMapreduceAmAdminCommandOpts() {
        return yarnAppMapreduceAmAdminCommandOpts;
    }

    public void setYarnAppMapreduceAmAdminCommandOpts(String yarnAppMapreduceAmAdminCommandOpts) {
        this.yarnAppMapreduceAmAdminCommandOpts = yarnAppMapreduceAmAdminCommandOpts;
    }

    public String getYarnAppMapreduceAmCommandOpts() {
        return yarnAppMapreduceAmCommandOpts;
    }

    public void setYarnAppMapreduceAmCommandOpts(String yarnAppMapreduceAmCommandOpts) {
        this.yarnAppMapreduceAmCommandOpts = yarnAppMapreduceAmCommandOpts;
    }

    public String getYarnAppMapreduceAmLogLevel() {
        return yarnAppMapreduceAmLogLevel;
    }

    public void setYarnAppMapreduceAmLogLevel(String yarnAppMapreduceAmLogLevel) {
        this.yarnAppMapreduceAmLogLevel = yarnAppMapreduceAmLogLevel;
    }

    public String getYarnAppMapreduceAmResourceMb() {
        return yarnAppMapreduceAmResourceMb;
    }

    public void setYarnAppMapreduceAmResourceMb(String yarnAppMapreduceAmResourceMb) {
        this.yarnAppMapreduceAmResourceMb = yarnAppMapreduceAmResourceMb;
    }

    public String getYarnAppMapreduceAmStagingDir() {
        return yarnAppMapreduceAmStagingDir;
    }

    public void setYarnAppMapreduceAmStagingDir(String yarnAppMapreduceAmStagingDir) {
        this.yarnAppMapreduceAmStagingDir = yarnAppMapreduceAmStagingDir;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Properties.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("content");
        sb.append('=');
        sb.append(((this.content == null)?"<null>":this.content));
        sb.append(',');
        sb.append("jobhistoryHeapsize");
        sb.append('=');
        sb.append(((this.jobhistoryHeapsize == null)?"<null>":this.jobhistoryHeapsize));
        sb.append(',');
        sb.append("mapredLogDirPrefix");
        sb.append('=');
        sb.append(((this.mapredLogDirPrefix == null)?"<null>":this.mapredLogDirPrefix));
        sb.append(',');
        sb.append("mapredPidDirPrefix");
        sb.append('=');
        sb.append(((this.mapredPidDirPrefix == null)?"<null>":this.mapredPidDirPrefix));
        sb.append(',');
        sb.append("mapredUser");
        sb.append('=');
        sb.append(((this.mapredUser == null)?"<null>":this.mapredUser));
        sb.append(',');
        sb.append("mapredUserNofileLimit");
        sb.append('=');
        sb.append(((this.mapredUserNofileLimit == null)?"<null>":this.mapredUserNofileLimit));
        sb.append(',');
        sb.append("mapredUserNprocLimit");
        sb.append('=');
        sb.append(((this.mapredUserNprocLimit == null)?"<null>":this.mapredUserNprocLimit));
        sb.append(',');
        sb.append("mapredLocalDir");
        sb.append('=');
        sb.append(((this.mapredLocalDir == null)?"<null>":this.mapredLocalDir));
        sb.append(',');
        sb.append("mapreduceAdminMapChildJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceAdminMapChildJavaOpts == null)?"<null>":this.mapreduceAdminMapChildJavaOpts));
        sb.append(',');
        sb.append("mapreduceAdminReduceChildJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceAdminReduceChildJavaOpts == null)?"<null>":this.mapreduceAdminReduceChildJavaOpts));
        sb.append(',');
        sb.append("mapreduceAdminUserEnv");
        sb.append('=');
        sb.append(((this.mapreduceAdminUserEnv == null)?"<null>":this.mapreduceAdminUserEnv));
        sb.append(',');
        sb.append("mapreduceAmMaxAttempts");
        sb.append('=');
        sb.append(((this.mapreduceAmMaxAttempts == null)?"<null>":this.mapreduceAmMaxAttempts));
        sb.append(',');
        sb.append("mapreduceApplicationClasspath");
        sb.append('=');
        sb.append(((this.mapreduceApplicationClasspath == null)?"<null>":this.mapreduceApplicationClasspath));
        sb.append(',');
        sb.append("mapreduceApplicationFrameworkPath");
        sb.append('=');
        sb.append(((this.mapreduceApplicationFrameworkPath == null)?"<null>":this.mapreduceApplicationFrameworkPath));
        sb.append(',');
        sb.append("mapreduceClusterAclsEnabled");
        sb.append('=');
        sb.append(((this.mapreduceClusterAclsEnabled == null)?"<null>":this.mapreduceClusterAclsEnabled));
        sb.append(',');
        sb.append("mapreduceClusterAdministrators");
        sb.append('=');
        sb.append(((this.mapreduceClusterAdministrators == null)?"<null>":this.mapreduceClusterAdministrators));
        sb.append(',');
        sb.append("mapreduceFrameworkName");
        sb.append('=');
        sb.append(((this.mapreduceFrameworkName == null)?"<null>":this.mapreduceFrameworkName));
        sb.append(',');
        sb.append("mapreduceJobAclModifyJob");
        sb.append('=');
        sb.append(((this.mapreduceJobAclModifyJob == null)?"<null>":this.mapreduceJobAclModifyJob));
        sb.append(',');
        sb.append("mapreduceJobAclViewJob");
        sb.append('=');
        sb.append(((this.mapreduceJobAclViewJob == null)?"<null>":this.mapreduceJobAclViewJob));
        sb.append(',');
        sb.append("mapreduceJobCountersMax");
        sb.append('=');
        sb.append(((this.mapreduceJobCountersMax == null)?"<null>":this.mapreduceJobCountersMax));
        sb.append(',');
        sb.append("mapreduceJobEmitTimelineData");
        sb.append('=');
        sb.append(((this.mapreduceJobEmitTimelineData == null)?"<null>":this.mapreduceJobEmitTimelineData));
        sb.append(',');
        sb.append("mapreduceJobQueuename");
        sb.append('=');
        sb.append(((this.mapreduceJobQueuename == null)?"<null>":this.mapreduceJobQueuename));
        sb.append(',');
        sb.append("mapreduceJobReduceSlowstartCompletedmaps");
        sb.append('=');
        sb.append(((this.mapreduceJobReduceSlowstartCompletedmaps == null)?"<null>":this.mapreduceJobReduceSlowstartCompletedmaps));
        sb.append(',');
        sb.append("mapreduceJobhistoryAddress");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryAddress == null)?"<null>":this.mapreduceJobhistoryAddress));
        sb.append(',');
        sb.append("mapreduceJobhistoryAdminAcl");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryAdminAcl == null)?"<null>":this.mapreduceJobhistoryAdminAcl));
        sb.append(',');
        sb.append("mapreduceJobhistoryBindHost");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryBindHost == null)?"<null>":this.mapreduceJobhistoryBindHost));
        sb.append(',');
        sb.append("mapreduceJobhistoryDoneDir");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryDoneDir == null)?"<null>":this.mapreduceJobhistoryDoneDir));
        sb.append(',');
        sb.append("mapreduceJobhistoryHttpPolicy");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryHttpPolicy == null)?"<null>":this.mapreduceJobhistoryHttpPolicy));
        sb.append(',');
        sb.append("mapreduceJobhistoryIntermediateDoneDir");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryIntermediateDoneDir == null)?"<null>":this.mapreduceJobhistoryIntermediateDoneDir));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryEnable");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryEnable == null)?"<null>":this.mapreduceJobhistoryRecoveryEnable));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryStoreClass");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryStoreClass == null)?"<null>":this.mapreduceJobhistoryRecoveryStoreClass));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryStoreLeveldbPath");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == null)?"<null>":this.mapreduceJobhistoryRecoveryStoreLeveldbPath));
        sb.append(',');
        sb.append("mapreduceJobhistoryWebappAddress");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryWebappAddress == null)?"<null>":this.mapreduceJobhistoryWebappAddress));
        sb.append(',');
        sb.append("mapreduceMapJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceMapJavaOpts == null)?"<null>":this.mapreduceMapJavaOpts));
        sb.append(',');
        sb.append("mapreduceMapLogLevel");
        sb.append('=');
        sb.append(((this.mapreduceMapLogLevel == null)?"<null>":this.mapreduceMapLogLevel));
        sb.append(',');
        sb.append("mapreduceMapMemoryMb");
        sb.append('=');
        sb.append(((this.mapreduceMapMemoryMb == null)?"<null>":this.mapreduceMapMemoryMb));
        sb.append(',');
        sb.append("mapreduceMapOutputCompress");
        sb.append('=');
        sb.append(((this.mapreduceMapOutputCompress == null)?"<null>":this.mapreduceMapOutputCompress));
        sb.append(',');
        sb.append("mapreduceMapSortSpillPercent");
        sb.append('=');
        sb.append(((this.mapreduceMapSortSpillPercent == null)?"<null>":this.mapreduceMapSortSpillPercent));
        sb.append(',');
        sb.append("mapreduceMapSpeculative");
        sb.append('=');
        sb.append(((this.mapreduceMapSpeculative == null)?"<null>":this.mapreduceMapSpeculative));
        sb.append(',');
        sb.append("mapreduceOutputFileoutputformatCompress");
        sb.append('=');
        sb.append(((this.mapreduceOutputFileoutputformatCompress == null)?"<null>":this.mapreduceOutputFileoutputformatCompress));
        sb.append(',');
        sb.append("mapreduceOutputFileoutputformatCompressType");
        sb.append('=');
        sb.append(((this.mapreduceOutputFileoutputformatCompressType == null)?"<null>":this.mapreduceOutputFileoutputformatCompressType));
        sb.append(',');
        sb.append("mapreduceReduceInputBufferPercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceInputBufferPercent == null)?"<null>":this.mapreduceReduceInputBufferPercent));
        sb.append(',');
        sb.append("mapreduceReduceJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceReduceJavaOpts == null)?"<null>":this.mapreduceReduceJavaOpts));
        sb.append(',');
        sb.append("mapreduceReduceLogLevel");
        sb.append('=');
        sb.append(((this.mapreduceReduceLogLevel == null)?"<null>":this.mapreduceReduceLogLevel));
        sb.append(',');
        sb.append("mapreduceReduceMemoryMb");
        sb.append('=');
        sb.append(((this.mapreduceReduceMemoryMb == null)?"<null>":this.mapreduceReduceMemoryMb));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryEnabled");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryEnabled == null)?"<null>":this.mapreduceReduceShuffleFetchRetryEnabled));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryIntervalMs");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryIntervalMs == null)?"<null>":this.mapreduceReduceShuffleFetchRetryIntervalMs));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryTimeoutMs");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryTimeoutMs == null)?"<null>":this.mapreduceReduceShuffleFetchRetryTimeoutMs));
        sb.append(',');
        sb.append("mapreduceReduceShuffleInputBufferPercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleInputBufferPercent == null)?"<null>":this.mapreduceReduceShuffleInputBufferPercent));
        sb.append(',');
        sb.append("mapreduceReduceShuffleMergePercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleMergePercent == null)?"<null>":this.mapreduceReduceShuffleMergePercent));
        sb.append(',');
        sb.append("mapreduceReduceShuffleParallelcopies");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleParallelcopies == null)?"<null>":this.mapreduceReduceShuffleParallelcopies));
        sb.append(',');
        sb.append("mapreduceReduceSpeculative");
        sb.append('=');
        sb.append(((this.mapreduceReduceSpeculative == null)?"<null>":this.mapreduceReduceSpeculative));
        sb.append(',');
        sb.append("mapreduceShufflePort");
        sb.append('=');
        sb.append(((this.mapreduceShufflePort == null)?"<null>":this.mapreduceShufflePort));
        sb.append(',');
        sb.append("mapreduceTaskIoSortFactor");
        sb.append('=');
        sb.append(((this.mapreduceTaskIoSortFactor == null)?"<null>":this.mapreduceTaskIoSortFactor));
        sb.append(',');
        sb.append("mapreduceTaskIoSortMb");
        sb.append('=');
        sb.append(((this.mapreduceTaskIoSortMb == null)?"<null>":this.mapreduceTaskIoSortMb));
        sb.append(',');
        sb.append("mapreduceTaskTimeout");
        sb.append('=');
        sb.append(((this.mapreduceTaskTimeout == null)?"<null>":this.mapreduceTaskTimeout));
        sb.append(',');
        sb.append("yarnAppMapreduceAmAdminCommandOpts");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmAdminCommandOpts == null)?"<null>":this.yarnAppMapreduceAmAdminCommandOpts));
        sb.append(',');
        sb.append("yarnAppMapreduceAmCommandOpts");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmCommandOpts == null)?"<null>":this.yarnAppMapreduceAmCommandOpts));
        sb.append(',');
        sb.append("yarnAppMapreduceAmLogLevel");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmLogLevel == null)?"<null>":this.yarnAppMapreduceAmLogLevel));
        sb.append(',');
        sb.append("yarnAppMapreduceAmResourceMb");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmResourceMb == null)?"<null>":this.yarnAppMapreduceAmResourceMb));
        sb.append(',');
        sb.append("yarnAppMapreduceAmStagingDir");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmStagingDir == null)?"<null>":this.yarnAppMapreduceAmStagingDir));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.mapredUserNofileLimit == null)? 0 :this.mapredUserNofileLimit.hashCode()));
        result = ((result* 31)+((this.mapreduceOutputFileoutputformatCompressType == null)? 0 :this.mapreduceOutputFileoutputformatCompressType.hashCode()));
        result = ((result* 31)+((this.mapredLocalDir == null)? 0 :this.mapredLocalDir.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskTimeout == null)? 0 :this.mapreduceTaskTimeout.hashCode()));
        result = ((result* 31)+((this.mapreduceJobEmitTimelineData == null)? 0 :this.mapreduceJobEmitTimelineData.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryIntervalMs == null)? 0 :this.mapreduceReduceShuffleFetchRetryIntervalMs.hashCode()));
        result = ((result* 31)+((this.mapreduceOutputFileoutputformatCompress == null)? 0 :this.mapreduceOutputFileoutputformatCompress.hashCode()));
        result = ((result* 31)+((this.mapreduceMapMemoryMb == null)? 0 :this.mapreduceMapMemoryMb.hashCode()));
        result = ((result* 31)+((this.mapreduceAmMaxAttempts == null)? 0 :this.mapreduceAmMaxAttempts.hashCode()));
        result = ((result* 31)+((this.mapreduceClusterAdministrators == null)? 0 :this.mapreduceClusterAdministrators.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminUserEnv == null)? 0 :this.mapreduceAdminUserEnv.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskIoSortMb == null)? 0 :this.mapreduceTaskIoSortMb.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceJavaOpts == null)? 0 :this.mapreduceReduceJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleMergePercent == null)? 0 :this.mapreduceReduceShuffleMergePercent.hashCode()));
        result = ((result* 31)+((this.mapreduceApplicationClasspath == null)? 0 :this.mapreduceApplicationClasspath.hashCode()));
        result = ((result* 31)+((this.mapreduceJobAclModifyJob == null)? 0 :this.mapreduceJobAclModifyJob.hashCode()));
        result = ((result* 31)+((this.mapreduceMapSortSpillPercent == null)? 0 :this.mapreduceMapSortSpillPercent.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryEnable == null)? 0 :this.mapreduceJobhistoryRecoveryEnable.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmStagingDir == null)? 0 :this.yarnAppMapreduceAmStagingDir.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryAddress == null)? 0 :this.mapreduceJobhistoryAddress.hashCode()));
        result = ((result* 31)+((this.mapreduceMapOutputCompress == null)? 0 :this.mapreduceMapOutputCompress.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmCommandOpts == null)? 0 :this.yarnAppMapreduceAmCommandOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceJobReduceSlowstartCompletedmaps == null)? 0 :this.mapreduceJobReduceSlowstartCompletedmaps.hashCode()));
        result = ((result* 31)+((this.mapredPidDirPrefix == null)? 0 :this.mapredPidDirPrefix.hashCode()));
        result = ((result* 31)+((this.mapreduceJobAclViewJob == null)? 0 :this.mapreduceJobAclViewJob.hashCode()));
        result = ((result* 31)+((this.mapreduceMapJavaOpts == null)? 0 :this.mapreduceMapJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceSpeculative == null)? 0 :this.mapreduceReduceSpeculative.hashCode()));
        result = ((result* 31)+((this.mapreduceClusterAclsEnabled == null)? 0 :this.mapreduceClusterAclsEnabled.hashCode()));
        result = ((result* 31)+((this.mapreduceJobCountersMax == null)? 0 :this.mapreduceJobCountersMax.hashCode()));
        result = ((result* 31)+((this.mapreduceApplicationFrameworkPath == null)? 0 :this.mapreduceApplicationFrameworkPath.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminReduceChildJavaOpts == null)? 0 :this.mapreduceAdminReduceChildJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceMapSpeculative == null)? 0 :this.mapreduceMapSpeculative.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryIntermediateDoneDir == null)? 0 :this.mapreduceJobhistoryIntermediateDoneDir.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryTimeoutMs == null)? 0 :this.mapreduceReduceShuffleFetchRetryTimeoutMs.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmLogLevel == null)? 0 :this.yarnAppMapreduceAmLogLevel.hashCode()));
        result = ((result* 31)+((this.content == null)? 0 :this.content.hashCode()));
        result = ((result* 31)+((this.mapredUserNprocLimit == null)? 0 :this.mapredUserNprocLimit.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskIoSortFactor == null)? 0 :this.mapreduceTaskIoSortFactor.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryEnabled == null)? 0 :this.mapreduceReduceShuffleFetchRetryEnabled.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryAdminAcl == null)? 0 :this.mapreduceJobhistoryAdminAcl.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryHttpPolicy == null)? 0 :this.mapreduceJobhistoryHttpPolicy.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminMapChildJavaOpts == null)? 0 :this.mapreduceAdminMapChildJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceMapLogLevel == null)? 0 :this.mapreduceMapLogLevel.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceMemoryMb == null)? 0 :this.mapreduceReduceMemoryMb.hashCode()));
        result = ((result* 31)+((this.mapredLogDirPrefix == null)? 0 :this.mapredLogDirPrefix.hashCode()));
        result = ((result* 31)+((this.mapredUser == null)? 0 :this.mapredUser.hashCode()));
        result = ((result* 31)+((this.mapreduceJobQueuename == null)? 0 :this.mapreduceJobQueuename.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryDoneDir == null)? 0 :this.mapreduceJobhistoryDoneDir.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryBindHost == null)? 0 :this.mapreduceJobhistoryBindHost.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryWebappAddress == null)? 0 :this.mapreduceJobhistoryWebappAddress.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmResourceMb == null)? 0 :this.yarnAppMapreduceAmResourceMb.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceInputBufferPercent == null)? 0 :this.mapreduceReduceInputBufferPercent.hashCode()));
        result = ((result* 31)+((this.jobhistoryHeapsize == null)? 0 :this.jobhistoryHeapsize.hashCode()));
        result = ((result* 31)+((this.mapreduceFrameworkName == null)? 0 :this.mapreduceFrameworkName.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleParallelcopies == null)? 0 :this.mapreduceReduceShuffleParallelcopies.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmAdminCommandOpts == null)? 0 :this.yarnAppMapreduceAmAdminCommandOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleInputBufferPercent == null)? 0 :this.mapreduceReduceShuffleInputBufferPercent.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryStoreClass == null)? 0 :this.mapreduceJobhistoryRecoveryStoreClass.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == null)? 0 :this.mapreduceJobhistoryRecoveryStoreLeveldbPath.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceLogLevel == null)? 0 :this.mapreduceReduceLogLevel.hashCode()));
        result = ((result* 31)+((this.mapreduceShufflePort == null)? 0 :this.mapreduceShufflePort.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Properties) == false) {
            return false;
        }
        Properties rhs = ((Properties) other);
        return ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((this.mapredUserNofileLimit == rhs.mapredUserNofileLimit)||((this.mapredUserNofileLimit!= null)&&this.mapredUserNofileLimit.equals(rhs.mapredUserNofileLimit)))&&((this.mapreduceOutputFileoutputformatCompressType == rhs.mapreduceOutputFileoutputformatCompressType)||((this.mapreduceOutputFileoutputformatCompressType!= null)&&this.mapreduceOutputFileoutputformatCompressType.equals(rhs.mapreduceOutputFileoutputformatCompressType))))&&((this.mapredLocalDir == rhs.mapredLocalDir)||((this.mapredLocalDir!= null)&&this.mapredLocalDir.equals(rhs.mapredLocalDir))))&&((this.mapreduceTaskTimeout == rhs.mapreduceTaskTimeout)||((this.mapreduceTaskTimeout!= null)&&this.mapreduceTaskTimeout.equals(rhs.mapreduceTaskTimeout))))&&((this.mapreduceJobEmitTimelineData == rhs.mapreduceJobEmitTimelineData)||((this.mapreduceJobEmitTimelineData!= null)&&this.mapreduceJobEmitTimelineData.equals(rhs.mapreduceJobEmitTimelineData))))&&((this.mapreduceReduceShuffleFetchRetryIntervalMs == rhs.mapreduceReduceShuffleFetchRetryIntervalMs)||((this.mapreduceReduceShuffleFetchRetryIntervalMs!= null)&&this.mapreduceReduceShuffleFetchRetryIntervalMs.equals(rhs.mapreduceReduceShuffleFetchRetryIntervalMs))))&&((this.mapreduceOutputFileoutputformatCompress == rhs.mapreduceOutputFileoutputformatCompress)||((this.mapreduceOutputFileoutputformatCompress!= null)&&this.mapreduceOutputFileoutputformatCompress.equals(rhs.mapreduceOutputFileoutputformatCompress))))&&((this.mapreduceMapMemoryMb == rhs.mapreduceMapMemoryMb)||((this.mapreduceMapMemoryMb!= null)&&this.mapreduceMapMemoryMb.equals(rhs.mapreduceMapMemoryMb))))&&((this.mapreduceAmMaxAttempts == rhs.mapreduceAmMaxAttempts)||((this.mapreduceAmMaxAttempts!= null)&&this.mapreduceAmMaxAttempts.equals(rhs.mapreduceAmMaxAttempts))))&&((this.mapreduceClusterAdministrators == rhs.mapreduceClusterAdministrators)||((this.mapreduceClusterAdministrators!= null)&&this.mapreduceClusterAdministrators.equals(rhs.mapreduceClusterAdministrators))))&&((this.mapreduceAdminUserEnv == rhs.mapreduceAdminUserEnv)||((this.mapreduceAdminUserEnv!= null)&&this.mapreduceAdminUserEnv.equals(rhs.mapreduceAdminUserEnv))))&&((this.mapreduceTaskIoSortMb == rhs.mapreduceTaskIoSortMb)||((this.mapreduceTaskIoSortMb!= null)&&this.mapreduceTaskIoSortMb.equals(rhs.mapreduceTaskIoSortMb))))&&((this.mapreduceReduceJavaOpts == rhs.mapreduceReduceJavaOpts)||((this.mapreduceReduceJavaOpts!= null)&&this.mapreduceReduceJavaOpts.equals(rhs.mapreduceReduceJavaOpts))))&&((this.mapreduceReduceShuffleMergePercent == rhs.mapreduceReduceShuffleMergePercent)||((this.mapreduceReduceShuffleMergePercent!= null)&&this.mapreduceReduceShuffleMergePercent.equals(rhs.mapreduceReduceShuffleMergePercent))))&&((this.mapreduceApplicationClasspath == rhs.mapreduceApplicationClasspath)||((this.mapreduceApplicationClasspath!= null)&&this.mapreduceApplicationClasspath.equals(rhs.mapreduceApplicationClasspath))))&&((this.mapreduceJobAclModifyJob == rhs.mapreduceJobAclModifyJob)||((this.mapreduceJobAclModifyJob!= null)&&this.mapreduceJobAclModifyJob.equals(rhs.mapreduceJobAclModifyJob))))&&((this.mapreduceMapSortSpillPercent == rhs.mapreduceMapSortSpillPercent)||((this.mapreduceMapSortSpillPercent!= null)&&this.mapreduceMapSortSpillPercent.equals(rhs.mapreduceMapSortSpillPercent))))&&((this.mapreduceJobhistoryRecoveryEnable == rhs.mapreduceJobhistoryRecoveryEnable)||((this.mapreduceJobhistoryRecoveryEnable!= null)&&this.mapreduceJobhistoryRecoveryEnable.equals(rhs.mapreduceJobhistoryRecoveryEnable))))&&((this.yarnAppMapreduceAmStagingDir == rhs.yarnAppMapreduceAmStagingDir)||((this.yarnAppMapreduceAmStagingDir!= null)&&this.yarnAppMapreduceAmStagingDir.equals(rhs.yarnAppMapreduceAmStagingDir))))&&((this.mapreduceJobhistoryAddress == rhs.mapreduceJobhistoryAddress)||((this.mapreduceJobhistoryAddress!= null)&&this.mapreduceJobhistoryAddress.equals(rhs.mapreduceJobhistoryAddress))))&&((this.mapreduceMapOutputCompress == rhs.mapreduceMapOutputCompress)||((this.mapreduceMapOutputCompress!= null)&&this.mapreduceMapOutputCompress.equals(rhs.mapreduceMapOutputCompress))))&&((this.yarnAppMapreduceAmCommandOpts == rhs.yarnAppMapreduceAmCommandOpts)||((this.yarnAppMapreduceAmCommandOpts!= null)&&this.yarnAppMapreduceAmCommandOpts.equals(rhs.yarnAppMapreduceAmCommandOpts))))&&((this.mapreduceJobReduceSlowstartCompletedmaps == rhs.mapreduceJobReduceSlowstartCompletedmaps)||((this.mapreduceJobReduceSlowstartCompletedmaps!= null)&&this.mapreduceJobReduceSlowstartCompletedmaps.equals(rhs.mapreduceJobReduceSlowstartCompletedmaps))))&&((this.mapredPidDirPrefix == rhs.mapredPidDirPrefix)||((this.mapredPidDirPrefix!= null)&&this.mapredPidDirPrefix.equals(rhs.mapredPidDirPrefix))))&&((this.mapreduceJobAclViewJob == rhs.mapreduceJobAclViewJob)||((this.mapreduceJobAclViewJob!= null)&&this.mapreduceJobAclViewJob.equals(rhs.mapreduceJobAclViewJob))))&&((this.mapreduceMapJavaOpts == rhs.mapreduceMapJavaOpts)||((this.mapreduceMapJavaOpts!= null)&&this.mapreduceMapJavaOpts.equals(rhs.mapreduceMapJavaOpts))))&&((this.mapreduceReduceSpeculative == rhs.mapreduceReduceSpeculative)||((this.mapreduceReduceSpeculative!= null)&&this.mapreduceReduceSpeculative.equals(rhs.mapreduceReduceSpeculative))))&&((this.mapreduceClusterAclsEnabled == rhs.mapreduceClusterAclsEnabled)||((this.mapreduceClusterAclsEnabled!= null)&&this.mapreduceClusterAclsEnabled.equals(rhs.mapreduceClusterAclsEnabled))))&&((this.mapreduceJobCountersMax == rhs.mapreduceJobCountersMax)||((this.mapreduceJobCountersMax!= null)&&this.mapreduceJobCountersMax.equals(rhs.mapreduceJobCountersMax))))&&((this.mapreduceApplicationFrameworkPath == rhs.mapreduceApplicationFrameworkPath)||((this.mapreduceApplicationFrameworkPath!= null)&&this.mapreduceApplicationFrameworkPath.equals(rhs.mapreduceApplicationFrameworkPath))))&&((this.mapreduceAdminReduceChildJavaOpts == rhs.mapreduceAdminReduceChildJavaOpts)||((this.mapreduceAdminReduceChildJavaOpts!= null)&&this.mapreduceAdminReduceChildJavaOpts.equals(rhs.mapreduceAdminReduceChildJavaOpts))))&&((this.mapreduceMapSpeculative == rhs.mapreduceMapSpeculative)||((this.mapreduceMapSpeculative!= null)&&this.mapreduceMapSpeculative.equals(rhs.mapreduceMapSpeculative))))&&((this.mapreduceJobhistoryIntermediateDoneDir == rhs.mapreduceJobhistoryIntermediateDoneDir)||((this.mapreduceJobhistoryIntermediateDoneDir!= null)&&this.mapreduceJobhistoryIntermediateDoneDir.equals(rhs.mapreduceJobhistoryIntermediateDoneDir))))&&((this.mapreduceReduceShuffleFetchRetryTimeoutMs == rhs.mapreduceReduceShuffleFetchRetryTimeoutMs)||((this.mapreduceReduceShuffleFetchRetryTimeoutMs!= null)&&this.mapreduceReduceShuffleFetchRetryTimeoutMs.equals(rhs.mapreduceReduceShuffleFetchRetryTimeoutMs))))&&((this.yarnAppMapreduceAmLogLevel == rhs.yarnAppMapreduceAmLogLevel)||((this.yarnAppMapreduceAmLogLevel!= null)&&this.yarnAppMapreduceAmLogLevel.equals(rhs.yarnAppMapreduceAmLogLevel))))&&((this.content == rhs.content)||((this.content!= null)&&this.content.equals(rhs.content))))&&((this.mapredUserNprocLimit == rhs.mapredUserNprocLimit)||((this.mapredUserNprocLimit!= null)&&this.mapredUserNprocLimit.equals(rhs.mapredUserNprocLimit))))&&((this.mapreduceTaskIoSortFactor == rhs.mapreduceTaskIoSortFactor)||((this.mapreduceTaskIoSortFactor!= null)&&this.mapreduceTaskIoSortFactor.equals(rhs.mapreduceTaskIoSortFactor))))&&((this.mapreduceReduceShuffleFetchRetryEnabled == rhs.mapreduceReduceShuffleFetchRetryEnabled)||((this.mapreduceReduceShuffleFetchRetryEnabled!= null)&&this.mapreduceReduceShuffleFetchRetryEnabled.equals(rhs.mapreduceReduceShuffleFetchRetryEnabled))))&&((this.mapreduceJobhistoryAdminAcl == rhs.mapreduceJobhistoryAdminAcl)||((this.mapreduceJobhistoryAdminAcl!= null)&&this.mapreduceJobhistoryAdminAcl.equals(rhs.mapreduceJobhistoryAdminAcl))))&&((this.mapreduceJobhistoryHttpPolicy == rhs.mapreduceJobhistoryHttpPolicy)||((this.mapreduceJobhistoryHttpPolicy!= null)&&this.mapreduceJobhistoryHttpPolicy.equals(rhs.mapreduceJobhistoryHttpPolicy))))&&((this.mapreduceAdminMapChildJavaOpts == rhs.mapreduceAdminMapChildJavaOpts)||((this.mapreduceAdminMapChildJavaOpts!= null)&&this.mapreduceAdminMapChildJavaOpts.equals(rhs.mapreduceAdminMapChildJavaOpts))))&&((this.mapreduceMapLogLevel == rhs.mapreduceMapLogLevel)||((this.mapreduceMapLogLevel!= null)&&this.mapreduceMapLogLevel.equals(rhs.mapreduceMapLogLevel))))&&((this.mapreduceReduceMemoryMb == rhs.mapreduceReduceMemoryMb)||((this.mapreduceReduceMemoryMb!= null)&&this.mapreduceReduceMemoryMb.equals(rhs.mapreduceReduceMemoryMb))))&&((this.mapredLogDirPrefix == rhs.mapredLogDirPrefix)||((this.mapredLogDirPrefix!= null)&&this.mapredLogDirPrefix.equals(rhs.mapredLogDirPrefix))))&&((this.mapredUser == rhs.mapredUser)||((this.mapredUser!= null)&&this.mapredUser.equals(rhs.mapredUser))))&&((this.mapreduceJobQueuename == rhs.mapreduceJobQueuename)||((this.mapreduceJobQueuename!= null)&&this.mapreduceJobQueuename.equals(rhs.mapreduceJobQueuename))))&&((this.mapreduceJobhistoryDoneDir == rhs.mapreduceJobhistoryDoneDir)||((this.mapreduceJobhistoryDoneDir!= null)&&this.mapreduceJobhistoryDoneDir.equals(rhs.mapreduceJobhistoryDoneDir))))&&((this.mapreduceJobhistoryBindHost == rhs.mapreduceJobhistoryBindHost)||((this.mapreduceJobhistoryBindHost!= null)&&this.mapreduceJobhistoryBindHost.equals(rhs.mapreduceJobhistoryBindHost))))&&((this.mapreduceJobhistoryWebappAddress == rhs.mapreduceJobhistoryWebappAddress)||((this.mapreduceJobhistoryWebappAddress!= null)&&this.mapreduceJobhistoryWebappAddress.equals(rhs.mapreduceJobhistoryWebappAddress))))&&((this.yarnAppMapreduceAmResourceMb == rhs.yarnAppMapreduceAmResourceMb)||((this.yarnAppMapreduceAmResourceMb!= null)&&this.yarnAppMapreduceAmResourceMb.equals(rhs.yarnAppMapreduceAmResourceMb))))&&((this.mapreduceReduceInputBufferPercent == rhs.mapreduceReduceInputBufferPercent)||((this.mapreduceReduceInputBufferPercent!= null)&&this.mapreduceReduceInputBufferPercent.equals(rhs.mapreduceReduceInputBufferPercent))))&&((this.jobhistoryHeapsize == rhs.jobhistoryHeapsize)||((this.jobhistoryHeapsize!= null)&&this.jobhistoryHeapsize.equals(rhs.jobhistoryHeapsize))))&&((this.mapreduceFrameworkName == rhs.mapreduceFrameworkName)||((this.mapreduceFrameworkName!= null)&&this.mapreduceFrameworkName.equals(rhs.mapreduceFrameworkName))))&&((this.mapreduceReduceShuffleParallelcopies == rhs.mapreduceReduceShuffleParallelcopies)||((this.mapreduceReduceShuffleParallelcopies!= null)&&this.mapreduceReduceShuffleParallelcopies.equals(rhs.mapreduceReduceShuffleParallelcopies))))&&((this.yarnAppMapreduceAmAdminCommandOpts == rhs.yarnAppMapreduceAmAdminCommandOpts)||((this.yarnAppMapreduceAmAdminCommandOpts!= null)&&this.yarnAppMapreduceAmAdminCommandOpts.equals(rhs.yarnAppMapreduceAmAdminCommandOpts))))&&((this.mapreduceReduceShuffleInputBufferPercent == rhs.mapreduceReduceShuffleInputBufferPercent)||((this.mapreduceReduceShuffleInputBufferPercent!= null)&&this.mapreduceReduceShuffleInputBufferPercent.equals(rhs.mapreduceReduceShuffleInputBufferPercent))))&&((this.mapreduceJobhistoryRecoveryStoreClass == rhs.mapreduceJobhistoryRecoveryStoreClass)||((this.mapreduceJobhistoryRecoveryStoreClass!= null)&&this.mapreduceJobhistoryRecoveryStoreClass.equals(rhs.mapreduceJobhistoryRecoveryStoreClass))))&&((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == rhs.mapreduceJobhistoryRecoveryStoreLeveldbPath)||((this.mapreduceJobhistoryRecoveryStoreLeveldbPath!= null)&&this.mapreduceJobhistoryRecoveryStoreLeveldbPath.equals(rhs.mapreduceJobhistoryRecoveryStoreLeveldbPath))))&&((this.mapreduceReduceLogLevel == rhs.mapreduceReduceLogLevel)||((this.mapreduceReduceLogLevel!= null)&&this.mapreduceReduceLogLevel.equals(rhs.mapreduceReduceLogLevel))))&&((this.mapreduceShufflePort == rhs.mapreduceShufflePort)||((this.mapreduceShufflePort!= null)&&this.mapreduceShufflePort.equals(rhs.mapreduceShufflePort))));
    }

}
