package com.labs247.telyu.model.mapreduce2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "content",
    "jobhistory_heapsize",
    "mapred_log_dir_prefix",
    "mapred_pid_dir_prefix",
    "mapred_user",
    "mapred_user_nofile_limit",
    "mapred_user_nproc_limit",
    "mapred.local.dir",
    "mapreduce.admin.map.child.java.opts",
    "mapreduce.admin.reduce.child.java.opts",
    "mapreduce.admin.user.env",
    "mapreduce.am.max-attempts",
    "mapreduce.application.classpath",
    "mapreduce.application.framework.path",
    "mapreduce.cluster.acls.enabled",
    "mapreduce.cluster.administrators",
    "mapreduce.framework.name",
    "mapreduce.job.acl-modify-job",
    "mapreduce.job.acl-view-job",
    "mapreduce.job.counters.max",
    "mapreduce.job.emit-timeline-data",
    "mapreduce.job.queuename",
    "mapreduce.job.reduce.slowstart.completedmaps",
    "mapreduce.jobhistory.address",
    "mapreduce.jobhistory.admin.acl",
    "mapreduce.jobhistory.bind-host",
    "mapreduce.jobhistory.done-dir",
    "mapreduce.jobhistory.http.policy",
    "mapreduce.jobhistory.intermediate-done-dir",
    "mapreduce.jobhistory.recovery.enable",
    "mapreduce.jobhistory.recovery.store.class",
    "mapreduce.jobhistory.recovery.store.leveldb.path",
    "mapreduce.jobhistory.webapp.address",
    "mapreduce.map.java.opts",
    "mapreduce.map.log.level",
    "mapreduce.map.memory.mb",
    "mapreduce.map.output.compress",
    "mapreduce.map.sort.spill.percent",
    "mapreduce.map.speculative",
    "mapreduce.output.fileoutputformat.compress",
    "mapreduce.output.fileoutputformat.compress.type",
    "mapreduce.reduce.input.buffer.percent",
    "mapreduce.reduce.java.opts",
    "mapreduce.reduce.log.level",
    "mapreduce.reduce.memory.mb",
    "mapreduce.reduce.shuffle.fetch.retry.enabled",
    "mapreduce.reduce.shuffle.fetch.retry.interval-ms",
    "mapreduce.reduce.shuffle.fetch.retry.timeout-ms",
    "mapreduce.reduce.shuffle.input.buffer.percent",
    "mapreduce.reduce.shuffle.merge.percent",
    "mapreduce.reduce.shuffle.parallelcopies",
    "mapreduce.reduce.speculative",
    "mapreduce.shuffle.port",
    "mapreduce.task.io.sort.factor",
    "mapreduce.task.io.sort.mb",
    "mapreduce.task.timeout",
    "yarn.app.mapreduce.am.admin-command-opts",
    "yarn.app.mapreduce.am.command-opts",
    "yarn.app.mapreduce.am.log.level",
    "yarn.app.mapreduce.am.resource.mb",
    "yarn.app.mapreduce.am.staging-dir"
})
public class Properties {

    @JsonProperty("content")
    private String content;
    @JsonProperty("jobhistory_heapsize")
    private String jobhistoryHeapsize;
    @JsonProperty("mapred_log_dir_prefix")
    private String mapredLogDirPrefix;
    @JsonProperty("mapred_pid_dir_prefix")
    private String mapredPidDirPrefix;
    @JsonProperty("mapred_user")
    private String mapredUser;
    @JsonProperty("mapred_user_nofile_limit")
    private String mapredUserNofileLimit;
    @JsonProperty("mapred_user_nproc_limit")
    private String mapredUserNprocLimit;
    @JsonProperty("mapred.local.dir")
    private String mapredLocalDir;
    @JsonProperty("mapreduce.admin.map.child.java.opts")
    private String mapreduceAdminMapChildJavaOpts;
    @JsonProperty("mapreduce.admin.reduce.child.java.opts")
    private String mapreduceAdminReduceChildJavaOpts;
    @JsonProperty("mapreduce.admin.user.env")
    private String mapreduceAdminUserEnv;
    @JsonProperty("mapreduce.am.max-attempts")
    private String mapreduceAmMaxAttempts;
    @JsonProperty("mapreduce.application.classpath")
    private String mapreduceApplicationClasspath;
    @JsonProperty("mapreduce.application.framework.path")
    private String mapreduceApplicationFrameworkPath;
    @JsonProperty("mapreduce.cluster.acls.enabled")
    private String mapreduceClusterAclsEnabled;
    @JsonProperty("mapreduce.cluster.administrators")
    private String mapreduceClusterAdministrators;
    @JsonProperty("mapreduce.framework.name")
    private String mapreduceFrameworkName;
    @JsonProperty("mapreduce.job.acl-modify-job")
    private String mapreduceJobAclModifyJob;
    @JsonProperty("mapreduce.job.acl-view-job")
    private String mapreduceJobAclViewJob;
    @JsonProperty("mapreduce.job.counters.max")
    private String mapreduceJobCountersMax;
    @JsonProperty("mapreduce.job.emit-timeline-data")
    private String mapreduceJobEmitTimelineData;
    @JsonProperty("mapreduce.job.queuename")
    private String mapreduceJobQueuename;
    @JsonProperty("mapreduce.job.reduce.slowstart.completedmaps")
    private String mapreduceJobReduceSlowstartCompletedmaps;
    @JsonProperty("mapreduce.jobhistory.address")
    private String mapreduceJobhistoryAddress;
    @JsonProperty("mapreduce.jobhistory.admin.acl")
    private String mapreduceJobhistoryAdminAcl;
    @JsonProperty("mapreduce.jobhistory.bind-host")
    private String mapreduceJobhistoryBindHost;
    @JsonProperty("mapreduce.jobhistory.done-dir")
    private String mapreduceJobhistoryDoneDir;
    @JsonProperty("mapreduce.jobhistory.http.policy")
    private String mapreduceJobhistoryHttpPolicy;
    @JsonProperty("mapreduce.jobhistory.intermediate-done-dir")
    private String mapreduceJobhistoryIntermediateDoneDir;
    @JsonProperty("mapreduce.jobhistory.recovery.enable")
    private String mapreduceJobhistoryRecoveryEnable;
    @JsonProperty("mapreduce.jobhistory.recovery.store.class")
    private String mapreduceJobhistoryRecoveryStoreClass;
    @JsonProperty("mapreduce.jobhistory.recovery.store.leveldb.path")
    private String mapreduceJobhistoryRecoveryStoreLeveldbPath;
    @JsonProperty("mapreduce.jobhistory.webapp.address")
    private String mapreduceJobhistoryWebappAddress;
    @JsonProperty("mapreduce.map.java.opts")
    private String mapreduceMapJavaOpts;
    @JsonProperty("mapreduce.map.log.level")
    private String mapreduceMapLogLevel;
    @JsonProperty("mapreduce.map.memory.mb")
    private String mapreduceMapMemoryMb;
    @JsonProperty("mapreduce.map.output.compress")
    private String mapreduceMapOutputCompress;
    @JsonProperty("mapreduce.map.sort.spill.percent")
    private String mapreduceMapSortSpillPercent;
    @JsonProperty("mapreduce.map.speculative")
    private String mapreduceMapSpeculative;
    @JsonProperty("mapreduce.output.fileoutputformat.compress")
    private String mapreduceOutputFileoutputformatCompress;
    @JsonProperty("mapreduce.output.fileoutputformat.compress.type")
    private String mapreduceOutputFileoutputformatCompressType;
    @JsonProperty("mapreduce.reduce.input.buffer.percent")
    private String mapreduceReduceInputBufferPercent;
    @JsonProperty("mapreduce.reduce.java.opts")
    private String mapreduceReduceJavaOpts;
    @JsonProperty("mapreduce.reduce.log.level")
    private String mapreduceReduceLogLevel;
    @JsonProperty("mapreduce.reduce.memory.mb")
    private String mapreduceReduceMemoryMb;
    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.enabled")
    private String mapreduceReduceShuffleFetchRetryEnabled;
    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.interval-ms")
    private String mapreduceReduceShuffleFetchRetryIntervalMs;
    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.timeout-ms")
    private String mapreduceReduceShuffleFetchRetryTimeoutMs;
    @JsonProperty("mapreduce.reduce.shuffle.input.buffer.percent")
    private String mapreduceReduceShuffleInputBufferPercent;
    @JsonProperty("mapreduce.reduce.shuffle.merge.percent")
    private String mapreduceReduceShuffleMergePercent;
    @JsonProperty("mapreduce.reduce.shuffle.parallelcopies")
    private String mapreduceReduceShuffleParallelcopies;
    @JsonProperty("mapreduce.reduce.speculative")
    private String mapreduceReduceSpeculative;
    @JsonProperty("mapreduce.shuffle.port")
    private String mapreduceShufflePort;
    @JsonProperty("mapreduce.task.io.sort.factor")
    private String mapreduceTaskIoSortFactor;
    @JsonProperty("mapreduce.task.io.sort.mb")
    private String mapreduceTaskIoSortMb;
    @JsonProperty("mapreduce.task.timeout")
    private String mapreduceTaskTimeout;
    @JsonProperty("yarn.app.mapreduce.am.admin-command-opts")
    private String yarnAppMapreduceAmAdminCommandOpts;
    @JsonProperty("yarn.app.mapreduce.am.command-opts")
    private String yarnAppMapreduceAmCommandOpts;
    @JsonProperty("yarn.app.mapreduce.am.log.level")
    private String yarnAppMapreduceAmLogLevel;
    @JsonProperty("yarn.app.mapreduce.am.resource.mb")
    private String yarnAppMapreduceAmResourceMb;
    @JsonProperty("yarn.app.mapreduce.am.staging-dir")
    private String yarnAppMapreduceAmStagingDir;

    @JsonProperty("content")
    public String getContent() {
        return content;
    }

    @JsonProperty("content")
    public void setContent(String content) {
        this.content = content;
    }

    @JsonProperty("jobhistory_heapsize")
    public String getJobhistoryHeapsize() {
        return jobhistoryHeapsize;
    }

    @JsonProperty("jobhistory_heapsize")
    public void setJobhistoryHeapsize(String jobhistoryHeapsize) {
        this.jobhistoryHeapsize = jobhistoryHeapsize;
    }

    @JsonProperty("mapred_log_dir_prefix")
    public String getMapredLogDirPrefix() {
        return mapredLogDirPrefix;
    }

    @JsonProperty("mapred_log_dir_prefix")
    public void setMapredLogDirPrefix(String mapredLogDirPrefix) {
        this.mapredLogDirPrefix = mapredLogDirPrefix;
    }

    @JsonProperty("mapred_pid_dir_prefix")
    public String getMapredPidDirPrefix() {
        return mapredPidDirPrefix;
    }

    @JsonProperty("mapred_pid_dir_prefix")
    public void setMapredPidDirPrefix(String mapredPidDirPrefix) {
        this.mapredPidDirPrefix = mapredPidDirPrefix;
    }

    @JsonProperty("mapred_user")
    public String getMapredUser() {
        return mapredUser;
    }

    @JsonProperty("mapred_user")
    public void setMapredUser(String mapredUser) {
        this.mapredUser = mapredUser;
    }

    @JsonProperty("mapred_user_nofile_limit")
    public String getMapredUserNofileLimit() {
        return mapredUserNofileLimit;
    }

    @JsonProperty("mapred_user_nofile_limit")
    public void setMapredUserNofileLimit(String mapredUserNofileLimit) {
        this.mapredUserNofileLimit = mapredUserNofileLimit;
    }

    @JsonProperty("mapred_user_nproc_limit")
    public String getMapredUserNprocLimit() {
        return mapredUserNprocLimit;
    }

    @JsonProperty("mapred_user_nproc_limit")
    public void setMapredUserNprocLimit(String mapredUserNprocLimit) {
        this.mapredUserNprocLimit = mapredUserNprocLimit;
    }

    @JsonProperty("mapred.local.dir")
    public String getMapredLocalDir() {
        return mapredLocalDir;
    }

    @JsonProperty("mapred.local.dir")
    public void setMapredLocalDir(String mapredLocalDir) {
        this.mapredLocalDir = mapredLocalDir;
    }

    @JsonProperty("mapreduce.admin.map.child.java.opts")
    public String getMapreduceAdminMapChildJavaOpts() {
        return mapreduceAdminMapChildJavaOpts;
    }

    @JsonProperty("mapreduce.admin.map.child.java.opts")
    public void setMapreduceAdminMapChildJavaOpts(String mapreduceAdminMapChildJavaOpts) {
        this.mapreduceAdminMapChildJavaOpts = mapreduceAdminMapChildJavaOpts;
    }

    @JsonProperty("mapreduce.admin.reduce.child.java.opts")
    public String getMapreduceAdminReduceChildJavaOpts() {
        return mapreduceAdminReduceChildJavaOpts;
    }

    @JsonProperty("mapreduce.admin.reduce.child.java.opts")
    public void setMapreduceAdminReduceChildJavaOpts(String mapreduceAdminReduceChildJavaOpts) {
        this.mapreduceAdminReduceChildJavaOpts = mapreduceAdminReduceChildJavaOpts;
    }

    @JsonProperty("mapreduce.admin.user.env")
    public String getMapreduceAdminUserEnv() {
        return mapreduceAdminUserEnv;
    }

    @JsonProperty("mapreduce.admin.user.env")
    public void setMapreduceAdminUserEnv(String mapreduceAdminUserEnv) {
        this.mapreduceAdminUserEnv = mapreduceAdminUserEnv;
    }

    @JsonProperty("mapreduce.am.max-attempts")
    public String getMapreduceAmMaxAttempts() {
        return mapreduceAmMaxAttempts;
    }

    @JsonProperty("mapreduce.am.max-attempts")
    public void setMapreduceAmMaxAttempts(String mapreduceAmMaxAttempts) {
        this.mapreduceAmMaxAttempts = mapreduceAmMaxAttempts;
    }

    @JsonProperty("mapreduce.application.classpath")
    public String getMapreduceApplicationClasspath() {
        return mapreduceApplicationClasspath;
    }

    @JsonProperty("mapreduce.application.classpath")
    public void setMapreduceApplicationClasspath(String mapreduceApplicationClasspath) {
        this.mapreduceApplicationClasspath = mapreduceApplicationClasspath;
    }

    @JsonProperty("mapreduce.application.framework.path")
    public String getMapreduceApplicationFrameworkPath() {
        return mapreduceApplicationFrameworkPath;
    }

    @JsonProperty("mapreduce.application.framework.path")
    public void setMapreduceApplicationFrameworkPath(String mapreduceApplicationFrameworkPath) {
        this.mapreduceApplicationFrameworkPath = mapreduceApplicationFrameworkPath;
    }

    @JsonProperty("mapreduce.cluster.acls.enabled")
    public String getMapreduceClusterAclsEnabled() {
        return mapreduceClusterAclsEnabled;
    }

    @JsonProperty("mapreduce.cluster.acls.enabled")
    public void setMapreduceClusterAclsEnabled(String mapreduceClusterAclsEnabled) {
        this.mapreduceClusterAclsEnabled = mapreduceClusterAclsEnabled;
    }

    @JsonProperty("mapreduce.cluster.administrators")
    public String getMapreduceClusterAdministrators() {
        return mapreduceClusterAdministrators;
    }

    @JsonProperty("mapreduce.cluster.administrators")
    public void setMapreduceClusterAdministrators(String mapreduceClusterAdministrators) {
        this.mapreduceClusterAdministrators = mapreduceClusterAdministrators;
    }

    @JsonProperty("mapreduce.framework.name")
    public String getMapreduceFrameworkName() {
        return mapreduceFrameworkName;
    }

    @JsonProperty("mapreduce.framework.name")
    public void setMapreduceFrameworkName(String mapreduceFrameworkName) {
        this.mapreduceFrameworkName = mapreduceFrameworkName;
    }

    @JsonProperty("mapreduce.job.acl-modify-job")
    public String getMapreduceJobAclModifyJob() {
        return mapreduceJobAclModifyJob;
    }

    @JsonProperty("mapreduce.job.acl-modify-job")
    public void setMapreduceJobAclModifyJob(String mapreduceJobAclModifyJob) {
        this.mapreduceJobAclModifyJob = mapreduceJobAclModifyJob;
    }

    @JsonProperty("mapreduce.job.acl-view-job")
    public String getMapreduceJobAclViewJob() {
        return mapreduceJobAclViewJob;
    }

    @JsonProperty("mapreduce.job.acl-view-job")
    public void setMapreduceJobAclViewJob(String mapreduceJobAclViewJob) {
        this.mapreduceJobAclViewJob = mapreduceJobAclViewJob;
    }

    @JsonProperty("mapreduce.job.counters.max")
    public String getMapreduceJobCountersMax() {
        return mapreduceJobCountersMax;
    }

    @JsonProperty("mapreduce.job.counters.max")
    public void setMapreduceJobCountersMax(String mapreduceJobCountersMax) {
        this.mapreduceJobCountersMax = mapreduceJobCountersMax;
    }

    @JsonProperty("mapreduce.job.emit-timeline-data")
    public String getMapreduceJobEmitTimelineData() {
        return mapreduceJobEmitTimelineData;
    }

    @JsonProperty("mapreduce.job.emit-timeline-data")
    public void setMapreduceJobEmitTimelineData(String mapreduceJobEmitTimelineData) {
        this.mapreduceJobEmitTimelineData = mapreduceJobEmitTimelineData;
    }

    @JsonProperty("mapreduce.job.queuename")
    public String getMapreduceJobQueuename() {
        return mapreduceJobQueuename;
    }

    @JsonProperty("mapreduce.job.queuename")
    public void setMapreduceJobQueuename(String mapreduceJobQueuename) {
        this.mapreduceJobQueuename = mapreduceJobQueuename;
    }

    @JsonProperty("mapreduce.job.reduce.slowstart.completedmaps")
    public String getMapreduceJobReduceSlowstartCompletedmaps() {
        return mapreduceJobReduceSlowstartCompletedmaps;
    }

    @JsonProperty("mapreduce.job.reduce.slowstart.completedmaps")
    public void setMapreduceJobReduceSlowstartCompletedmaps(String mapreduceJobReduceSlowstartCompletedmaps) {
        this.mapreduceJobReduceSlowstartCompletedmaps = mapreduceJobReduceSlowstartCompletedmaps;
    }

    @JsonProperty("mapreduce.jobhistory.address")
    public String getMapreduceJobhistoryAddress() {
        return mapreduceJobhistoryAddress;
    }

    @JsonProperty("mapreduce.jobhistory.address")
    public void setMapreduceJobhistoryAddress(String mapreduceJobhistoryAddress) {
        this.mapreduceJobhistoryAddress = mapreduceJobhistoryAddress;
    }

    @JsonProperty("mapreduce.jobhistory.admin.acl")
    public String getMapreduceJobhistoryAdminAcl() {
        return mapreduceJobhistoryAdminAcl;
    }

    @JsonProperty("mapreduce.jobhistory.admin.acl")
    public void setMapreduceJobhistoryAdminAcl(String mapreduceJobhistoryAdminAcl) {
        this.mapreduceJobhistoryAdminAcl = mapreduceJobhistoryAdminAcl;
    }

    @JsonProperty("mapreduce.jobhistory.bind-host")
    public String getMapreduceJobhistoryBindHost() {
        return mapreduceJobhistoryBindHost;
    }

    @JsonProperty("mapreduce.jobhistory.bind-host")
    public void setMapreduceJobhistoryBindHost(String mapreduceJobhistoryBindHost) {
        this.mapreduceJobhistoryBindHost = mapreduceJobhistoryBindHost;
    }

    @JsonProperty("mapreduce.jobhistory.done-dir")
    public String getMapreduceJobhistoryDoneDir() {
        return mapreduceJobhistoryDoneDir;
    }

    @JsonProperty("mapreduce.jobhistory.done-dir")
    public void setMapreduceJobhistoryDoneDir(String mapreduceJobhistoryDoneDir) {
        this.mapreduceJobhistoryDoneDir = mapreduceJobhistoryDoneDir;
    }

    @JsonProperty("mapreduce.jobhistory.http.policy")
    public String getMapreduceJobhistoryHttpPolicy() {
        return mapreduceJobhistoryHttpPolicy;
    }

    @JsonProperty("mapreduce.jobhistory.http.policy")
    public void setMapreduceJobhistoryHttpPolicy(String mapreduceJobhistoryHttpPolicy) {
        this.mapreduceJobhistoryHttpPolicy = mapreduceJobhistoryHttpPolicy;
    }

    @JsonProperty("mapreduce.jobhistory.intermediate-done-dir")
    public String getMapreduceJobhistoryIntermediateDoneDir() {
        return mapreduceJobhistoryIntermediateDoneDir;
    }

    @JsonProperty("mapreduce.jobhistory.intermediate-done-dir")
    public void setMapreduceJobhistoryIntermediateDoneDir(String mapreduceJobhistoryIntermediateDoneDir) {
        this.mapreduceJobhistoryIntermediateDoneDir = mapreduceJobhistoryIntermediateDoneDir;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.enable")
    public String getMapreduceJobhistoryRecoveryEnable() {
        return mapreduceJobhistoryRecoveryEnable;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.enable")
    public void setMapreduceJobhistoryRecoveryEnable(String mapreduceJobhistoryRecoveryEnable) {
        this.mapreduceJobhistoryRecoveryEnable = mapreduceJobhistoryRecoveryEnable;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.store.class")
    public String getMapreduceJobhistoryRecoveryStoreClass() {
        return mapreduceJobhistoryRecoveryStoreClass;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.store.class")
    public void setMapreduceJobhistoryRecoveryStoreClass(String mapreduceJobhistoryRecoveryStoreClass) {
        this.mapreduceJobhistoryRecoveryStoreClass = mapreduceJobhistoryRecoveryStoreClass;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.store.leveldb.path")
    public String getMapreduceJobhistoryRecoveryStoreLeveldbPath() {
        return mapreduceJobhistoryRecoveryStoreLeveldbPath;
    }

    @JsonProperty("mapreduce.jobhistory.recovery.store.leveldb.path")
    public void setMapreduceJobhistoryRecoveryStoreLeveldbPath(String mapreduceJobhistoryRecoveryStoreLeveldbPath) {
        this.mapreduceJobhistoryRecoveryStoreLeveldbPath = mapreduceJobhistoryRecoveryStoreLeveldbPath;
    }

    @JsonProperty("mapreduce.jobhistory.webapp.address")
    public String getMapreduceJobhistoryWebappAddress() {
        return mapreduceJobhistoryWebappAddress;
    }

    @JsonProperty("mapreduce.jobhistory.webapp.address")
    public void setMapreduceJobhistoryWebappAddress(String mapreduceJobhistoryWebappAddress) {
        this.mapreduceJobhistoryWebappAddress = mapreduceJobhistoryWebappAddress;
    }

    @JsonProperty("mapreduce.map.java.opts")
    public String getMapreduceMapJavaOpts() {
        return mapreduceMapJavaOpts;
    }

    @JsonProperty("mapreduce.map.java.opts")
    public void setMapreduceMapJavaOpts(String mapreduceMapJavaOpts) {
        this.mapreduceMapJavaOpts = mapreduceMapJavaOpts;
    }

    @JsonProperty("mapreduce.map.log.level")
    public String getMapreduceMapLogLevel() {
        return mapreduceMapLogLevel;
    }

    @JsonProperty("mapreduce.map.log.level")
    public void setMapreduceMapLogLevel(String mapreduceMapLogLevel) {
        this.mapreduceMapLogLevel = mapreduceMapLogLevel;
    }

    @JsonProperty("mapreduce.map.memory.mb")
    public String getMapreduceMapMemoryMb() {
        return mapreduceMapMemoryMb;
    }

    @JsonProperty("mapreduce.map.memory.mb")
    public void setMapreduceMapMemoryMb(String mapreduceMapMemoryMb) {
        this.mapreduceMapMemoryMb = mapreduceMapMemoryMb;
    }

    @JsonProperty("mapreduce.map.output.compress")
    public String getMapreduceMapOutputCompress() {
        return mapreduceMapOutputCompress;
    }

    @JsonProperty("mapreduce.map.output.compress")
    public void setMapreduceMapOutputCompress(String mapreduceMapOutputCompress) {
        this.mapreduceMapOutputCompress = mapreduceMapOutputCompress;
    }

    @JsonProperty("mapreduce.map.sort.spill.percent")
    public String getMapreduceMapSortSpillPercent() {
        return mapreduceMapSortSpillPercent;
    }

    @JsonProperty("mapreduce.map.sort.spill.percent")
    public void setMapreduceMapSortSpillPercent(String mapreduceMapSortSpillPercent) {
        this.mapreduceMapSortSpillPercent = mapreduceMapSortSpillPercent;
    }

    @JsonProperty("mapreduce.map.speculative")
    public String getMapreduceMapSpeculative() {
        return mapreduceMapSpeculative;
    }

    @JsonProperty("mapreduce.map.speculative")
    public void setMapreduceMapSpeculative(String mapreduceMapSpeculative) {
        this.mapreduceMapSpeculative = mapreduceMapSpeculative;
    }

    @JsonProperty("mapreduce.output.fileoutputformat.compress")
    public String getMapreduceOutputFileoutputformatCompress() {
        return mapreduceOutputFileoutputformatCompress;
    }

    @JsonProperty("mapreduce.output.fileoutputformat.compress")
    public void setMapreduceOutputFileoutputformatCompress(String mapreduceOutputFileoutputformatCompress) {
        this.mapreduceOutputFileoutputformatCompress = mapreduceOutputFileoutputformatCompress;
    }

    @JsonProperty("mapreduce.output.fileoutputformat.compress.type")
    public String getMapreduceOutputFileoutputformatCompressType() {
        return mapreduceOutputFileoutputformatCompressType;
    }

    @JsonProperty("mapreduce.output.fileoutputformat.compress.type")
    public void setMapreduceOutputFileoutputformatCompressType(String mapreduceOutputFileoutputformatCompressType) {
        this.mapreduceOutputFileoutputformatCompressType = mapreduceOutputFileoutputformatCompressType;
    }

    @JsonProperty("mapreduce.reduce.input.buffer.percent")
    public String getMapreduceReduceInputBufferPercent() {
        return mapreduceReduceInputBufferPercent;
    }

    @JsonProperty("mapreduce.reduce.input.buffer.percent")
    public void setMapreduceReduceInputBufferPercent(String mapreduceReduceInputBufferPercent) {
        this.mapreduceReduceInputBufferPercent = mapreduceReduceInputBufferPercent;
    }

    @JsonProperty("mapreduce.reduce.java.opts")
    public String getMapreduceReduceJavaOpts() {
        return mapreduceReduceJavaOpts;
    }

    @JsonProperty("mapreduce.reduce.java.opts")
    public void setMapreduceReduceJavaOpts(String mapreduceReduceJavaOpts) {
        this.mapreduceReduceJavaOpts = mapreduceReduceJavaOpts;
    }

    @JsonProperty("mapreduce.reduce.log.level")
    public String getMapreduceReduceLogLevel() {
        return mapreduceReduceLogLevel;
    }

    @JsonProperty("mapreduce.reduce.log.level")
    public void setMapreduceReduceLogLevel(String mapreduceReduceLogLevel) {
        this.mapreduceReduceLogLevel = mapreduceReduceLogLevel;
    }

    @JsonProperty("mapreduce.reduce.memory.mb")
    public String getMapreduceReduceMemoryMb() {
        return mapreduceReduceMemoryMb;
    }

    @JsonProperty("mapreduce.reduce.memory.mb")
    public void setMapreduceReduceMemoryMb(String mapreduceReduceMemoryMb) {
        this.mapreduceReduceMemoryMb = mapreduceReduceMemoryMb;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.enabled")
    public String getMapreduceReduceShuffleFetchRetryEnabled() {
        return mapreduceReduceShuffleFetchRetryEnabled;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.enabled")
    public void setMapreduceReduceShuffleFetchRetryEnabled(String mapreduceReduceShuffleFetchRetryEnabled) {
        this.mapreduceReduceShuffleFetchRetryEnabled = mapreduceReduceShuffleFetchRetryEnabled;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.interval-ms")
    public String getMapreduceReduceShuffleFetchRetryIntervalMs() {
        return mapreduceReduceShuffleFetchRetryIntervalMs;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.interval-ms")
    public void setMapreduceReduceShuffleFetchRetryIntervalMs(String mapreduceReduceShuffleFetchRetryIntervalMs) {
        this.mapreduceReduceShuffleFetchRetryIntervalMs = mapreduceReduceShuffleFetchRetryIntervalMs;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.timeout-ms")
    public String getMapreduceReduceShuffleFetchRetryTimeoutMs() {
        return mapreduceReduceShuffleFetchRetryTimeoutMs;
    }

    @JsonProperty("mapreduce.reduce.shuffle.fetch.retry.timeout-ms")
    public void setMapreduceReduceShuffleFetchRetryTimeoutMs(String mapreduceReduceShuffleFetchRetryTimeoutMs) {
        this.mapreduceReduceShuffleFetchRetryTimeoutMs = mapreduceReduceShuffleFetchRetryTimeoutMs;
    }

    @JsonProperty("mapreduce.reduce.shuffle.input.buffer.percent")
    public String getMapreduceReduceShuffleInputBufferPercent() {
        return mapreduceReduceShuffleInputBufferPercent;
    }

    @JsonProperty("mapreduce.reduce.shuffle.input.buffer.percent")
    public void setMapreduceReduceShuffleInputBufferPercent(String mapreduceReduceShuffleInputBufferPercent) {
        this.mapreduceReduceShuffleInputBufferPercent = mapreduceReduceShuffleInputBufferPercent;
    }

    @JsonProperty("mapreduce.reduce.shuffle.merge.percent")
    public String getMapreduceReduceShuffleMergePercent() {
        return mapreduceReduceShuffleMergePercent;
    }

    @JsonProperty("mapreduce.reduce.shuffle.merge.percent")
    public void setMapreduceReduceShuffleMergePercent(String mapreduceReduceShuffleMergePercent) {
        this.mapreduceReduceShuffleMergePercent = mapreduceReduceShuffleMergePercent;
    }

    @JsonProperty("mapreduce.reduce.shuffle.parallelcopies")
    public String getMapreduceReduceShuffleParallelcopies() {
        return mapreduceReduceShuffleParallelcopies;
    }

    @JsonProperty("mapreduce.reduce.shuffle.parallelcopies")
    public void setMapreduceReduceShuffleParallelcopies(String mapreduceReduceShuffleParallelcopies) {
        this.mapreduceReduceShuffleParallelcopies = mapreduceReduceShuffleParallelcopies;
    }

    @JsonProperty("mapreduce.reduce.speculative")
    public String getMapreduceReduceSpeculative() {
        return mapreduceReduceSpeculative;
    }

    @JsonProperty("mapreduce.reduce.speculative")
    public void setMapreduceReduceSpeculative(String mapreduceReduceSpeculative) {
        this.mapreduceReduceSpeculative = mapreduceReduceSpeculative;
    }

    @JsonProperty("mapreduce.shuffle.port")
    public String getMapreduceShufflePort() {
        return mapreduceShufflePort;
    }

    @JsonProperty("mapreduce.shuffle.port")
    public void setMapreduceShufflePort(String mapreduceShufflePort) {
        this.mapreduceShufflePort = mapreduceShufflePort;
    }

    @JsonProperty("mapreduce.task.io.sort.factor")
    public String getMapreduceTaskIoSortFactor() {
        return mapreduceTaskIoSortFactor;
    }

    @JsonProperty("mapreduce.task.io.sort.factor")
    public void setMapreduceTaskIoSortFactor(String mapreduceTaskIoSortFactor) {
        this.mapreduceTaskIoSortFactor = mapreduceTaskIoSortFactor;
    }

    @JsonProperty("mapreduce.task.io.sort.mb")
    public String getMapreduceTaskIoSortMb() {
        return mapreduceTaskIoSortMb;
    }

    @JsonProperty("mapreduce.task.io.sort.mb")
    public void setMapreduceTaskIoSortMb(String mapreduceTaskIoSortMb) {
        this.mapreduceTaskIoSortMb = mapreduceTaskIoSortMb;
    }

    @JsonProperty("mapreduce.task.timeout")
    public String getMapreduceTaskTimeout() {
        return mapreduceTaskTimeout;
    }

    @JsonProperty("mapreduce.task.timeout")
    public void setMapreduceTaskTimeout(String mapreduceTaskTimeout) {
        this.mapreduceTaskTimeout = mapreduceTaskTimeout;
    }

    @JsonProperty("yarn.app.mapreduce.am.admin-command-opts")
    public String getYarnAppMapreduceAmAdminCommandOpts() {
        return yarnAppMapreduceAmAdminCommandOpts;
    }

    @JsonProperty("yarn.app.mapreduce.am.admin-command-opts")
    public void setYarnAppMapreduceAmAdminCommandOpts(String yarnAppMapreduceAmAdminCommandOpts) {
        this.yarnAppMapreduceAmAdminCommandOpts = yarnAppMapreduceAmAdminCommandOpts;
    }

    @JsonProperty("yarn.app.mapreduce.am.command-opts")
    public String getYarnAppMapreduceAmCommandOpts() {
        return yarnAppMapreduceAmCommandOpts;
    }

    @JsonProperty("yarn.app.mapreduce.am.command-opts")
    public void setYarnAppMapreduceAmCommandOpts(String yarnAppMapreduceAmCommandOpts) {
        this.yarnAppMapreduceAmCommandOpts = yarnAppMapreduceAmCommandOpts;
    }

    @JsonProperty("yarn.app.mapreduce.am.log.level")
    public String getYarnAppMapreduceAmLogLevel() {
        return yarnAppMapreduceAmLogLevel;
    }

    @JsonProperty("yarn.app.mapreduce.am.log.level")
    public void setYarnAppMapreduceAmLogLevel(String yarnAppMapreduceAmLogLevel) {
        this.yarnAppMapreduceAmLogLevel = yarnAppMapreduceAmLogLevel;
    }

    @JsonProperty("yarn.app.mapreduce.am.resource.mb")
    public String getYarnAppMapreduceAmResourceMb() {
        return yarnAppMapreduceAmResourceMb;
    }

    @JsonProperty("yarn.app.mapreduce.am.resource.mb")
    public void setYarnAppMapreduceAmResourceMb(String yarnAppMapreduceAmResourceMb) {
        this.yarnAppMapreduceAmResourceMb = yarnAppMapreduceAmResourceMb;
    }

    @JsonProperty("yarn.app.mapreduce.am.staging-dir")
    public String getYarnAppMapreduceAmStagingDir() {
        return yarnAppMapreduceAmStagingDir;
    }

    @JsonProperty("yarn.app.mapreduce.am.staging-dir")
    public void setYarnAppMapreduceAmStagingDir(String yarnAppMapreduceAmStagingDir) {
        this.yarnAppMapreduceAmStagingDir = yarnAppMapreduceAmStagingDir;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Properties.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("content");
        sb.append('=');
        sb.append(((this.content == null)?"<null>":this.content));
        sb.append(',');
        sb.append("jobhistoryHeapsize");
        sb.append('=');
        sb.append(((this.jobhistoryHeapsize == null)?"<null>":this.jobhistoryHeapsize));
        sb.append(',');
        sb.append("mapredLogDirPrefix");
        sb.append('=');
        sb.append(((this.mapredLogDirPrefix == null)?"<null>":this.mapredLogDirPrefix));
        sb.append(',');
        sb.append("mapredPidDirPrefix");
        sb.append('=');
        sb.append(((this.mapredPidDirPrefix == null)?"<null>":this.mapredPidDirPrefix));
        sb.append(',');
        sb.append("mapredUser");
        sb.append('=');
        sb.append(((this.mapredUser == null)?"<null>":this.mapredUser));
        sb.append(',');
        sb.append("mapredUserNofileLimit");
        sb.append('=');
        sb.append(((this.mapredUserNofileLimit == null)?"<null>":this.mapredUserNofileLimit));
        sb.append(',');
        sb.append("mapredUserNprocLimit");
        sb.append('=');
        sb.append(((this.mapredUserNprocLimit == null)?"<null>":this.mapredUserNprocLimit));
        sb.append(',');
        sb.append("mapredLocalDir");
        sb.append('=');
        sb.append(((this.mapredLocalDir == null)?"<null>":this.mapredLocalDir));
        sb.append(',');
        sb.append("mapreduceAdminMapChildJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceAdminMapChildJavaOpts == null)?"<null>":this.mapreduceAdminMapChildJavaOpts));
        sb.append(',');
        sb.append("mapreduceAdminReduceChildJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceAdminReduceChildJavaOpts == null)?"<null>":this.mapreduceAdminReduceChildJavaOpts));
        sb.append(',');
        sb.append("mapreduceAdminUserEnv");
        sb.append('=');
        sb.append(((this.mapreduceAdminUserEnv == null)?"<null>":this.mapreduceAdminUserEnv));
        sb.append(',');
        sb.append("mapreduceAmMaxAttempts");
        sb.append('=');
        sb.append(((this.mapreduceAmMaxAttempts == null)?"<null>":this.mapreduceAmMaxAttempts));
        sb.append(',');
        sb.append("mapreduceApplicationClasspath");
        sb.append('=');
        sb.append(((this.mapreduceApplicationClasspath == null)?"<null>":this.mapreduceApplicationClasspath));
        sb.append(',');
        sb.append("mapreduceApplicationFrameworkPath");
        sb.append('=');
        sb.append(((this.mapreduceApplicationFrameworkPath == null)?"<null>":this.mapreduceApplicationFrameworkPath));
        sb.append(',');
        sb.append("mapreduceClusterAclsEnabled");
        sb.append('=');
        sb.append(((this.mapreduceClusterAclsEnabled == null)?"<null>":this.mapreduceClusterAclsEnabled));
        sb.append(',');
        sb.append("mapreduceClusterAdministrators");
        sb.append('=');
        sb.append(((this.mapreduceClusterAdministrators == null)?"<null>":this.mapreduceClusterAdministrators));
        sb.append(',');
        sb.append("mapreduceFrameworkName");
        sb.append('=');
        sb.append(((this.mapreduceFrameworkName == null)?"<null>":this.mapreduceFrameworkName));
        sb.append(',');
        sb.append("mapreduceJobAclModifyJob");
        sb.append('=');
        sb.append(((this.mapreduceJobAclModifyJob == null)?"<null>":this.mapreduceJobAclModifyJob));
        sb.append(',');
        sb.append("mapreduceJobAclViewJob");
        sb.append('=');
        sb.append(((this.mapreduceJobAclViewJob == null)?"<null>":this.mapreduceJobAclViewJob));
        sb.append(',');
        sb.append("mapreduceJobCountersMax");
        sb.append('=');
        sb.append(((this.mapreduceJobCountersMax == null)?"<null>":this.mapreduceJobCountersMax));
        sb.append(',');
        sb.append("mapreduceJobEmitTimelineData");
        sb.append('=');
        sb.append(((this.mapreduceJobEmitTimelineData == null)?"<null>":this.mapreduceJobEmitTimelineData));
        sb.append(',');
        sb.append("mapreduceJobQueuename");
        sb.append('=');
        sb.append(((this.mapreduceJobQueuename == null)?"<null>":this.mapreduceJobQueuename));
        sb.append(',');
        sb.append("mapreduceJobReduceSlowstartCompletedmaps");
        sb.append('=');
        sb.append(((this.mapreduceJobReduceSlowstartCompletedmaps == null)?"<null>":this.mapreduceJobReduceSlowstartCompletedmaps));
        sb.append(',');
        sb.append("mapreduceJobhistoryAddress");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryAddress == null)?"<null>":this.mapreduceJobhistoryAddress));
        sb.append(',');
        sb.append("mapreduceJobhistoryAdminAcl");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryAdminAcl == null)?"<null>":this.mapreduceJobhistoryAdminAcl));
        sb.append(',');
        sb.append("mapreduceJobhistoryBindHost");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryBindHost == null)?"<null>":this.mapreduceJobhistoryBindHost));
        sb.append(',');
        sb.append("mapreduceJobhistoryDoneDir");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryDoneDir == null)?"<null>":this.mapreduceJobhistoryDoneDir));
        sb.append(',');
        sb.append("mapreduceJobhistoryHttpPolicy");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryHttpPolicy == null)?"<null>":this.mapreduceJobhistoryHttpPolicy));
        sb.append(',');
        sb.append("mapreduceJobhistoryIntermediateDoneDir");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryIntermediateDoneDir == null)?"<null>":this.mapreduceJobhistoryIntermediateDoneDir));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryEnable");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryEnable == null)?"<null>":this.mapreduceJobhistoryRecoveryEnable));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryStoreClass");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryStoreClass == null)?"<null>":this.mapreduceJobhistoryRecoveryStoreClass));
        sb.append(',');
        sb.append("mapreduceJobhistoryRecoveryStoreLeveldbPath");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == null)?"<null>":this.mapreduceJobhistoryRecoveryStoreLeveldbPath));
        sb.append(',');
        sb.append("mapreduceJobhistoryWebappAddress");
        sb.append('=');
        sb.append(((this.mapreduceJobhistoryWebappAddress == null)?"<null>":this.mapreduceJobhistoryWebappAddress));
        sb.append(',');
        sb.append("mapreduceMapJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceMapJavaOpts == null)?"<null>":this.mapreduceMapJavaOpts));
        sb.append(',');
        sb.append("mapreduceMapLogLevel");
        sb.append('=');
        sb.append(((this.mapreduceMapLogLevel == null)?"<null>":this.mapreduceMapLogLevel));
        sb.append(',');
        sb.append("mapreduceMapMemoryMb");
        sb.append('=');
        sb.append(((this.mapreduceMapMemoryMb == null)?"<null>":this.mapreduceMapMemoryMb));
        sb.append(',');
        sb.append("mapreduceMapOutputCompress");
        sb.append('=');
        sb.append(((this.mapreduceMapOutputCompress == null)?"<null>":this.mapreduceMapOutputCompress));
        sb.append(',');
        sb.append("mapreduceMapSortSpillPercent");
        sb.append('=');
        sb.append(((this.mapreduceMapSortSpillPercent == null)?"<null>":this.mapreduceMapSortSpillPercent));
        sb.append(',');
        sb.append("mapreduceMapSpeculative");
        sb.append('=');
        sb.append(((this.mapreduceMapSpeculative == null)?"<null>":this.mapreduceMapSpeculative));
        sb.append(',');
        sb.append("mapreduceOutputFileoutputformatCompress");
        sb.append('=');
        sb.append(((this.mapreduceOutputFileoutputformatCompress == null)?"<null>":this.mapreduceOutputFileoutputformatCompress));
        sb.append(',');
        sb.append("mapreduceOutputFileoutputformatCompressType");
        sb.append('=');
        sb.append(((this.mapreduceOutputFileoutputformatCompressType == null)?"<null>":this.mapreduceOutputFileoutputformatCompressType));
        sb.append(',');
        sb.append("mapreduceReduceInputBufferPercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceInputBufferPercent == null)?"<null>":this.mapreduceReduceInputBufferPercent));
        sb.append(',');
        sb.append("mapreduceReduceJavaOpts");
        sb.append('=');
        sb.append(((this.mapreduceReduceJavaOpts == null)?"<null>":this.mapreduceReduceJavaOpts));
        sb.append(',');
        sb.append("mapreduceReduceLogLevel");
        sb.append('=');
        sb.append(((this.mapreduceReduceLogLevel == null)?"<null>":this.mapreduceReduceLogLevel));
        sb.append(',');
        sb.append("mapreduceReduceMemoryMb");
        sb.append('=');
        sb.append(((this.mapreduceReduceMemoryMb == null)?"<null>":this.mapreduceReduceMemoryMb));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryEnabled");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryEnabled == null)?"<null>":this.mapreduceReduceShuffleFetchRetryEnabled));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryIntervalMs");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryIntervalMs == null)?"<null>":this.mapreduceReduceShuffleFetchRetryIntervalMs));
        sb.append(',');
        sb.append("mapreduceReduceShuffleFetchRetryTimeoutMs");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleFetchRetryTimeoutMs == null)?"<null>":this.mapreduceReduceShuffleFetchRetryTimeoutMs));
        sb.append(',');
        sb.append("mapreduceReduceShuffleInputBufferPercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleInputBufferPercent == null)?"<null>":this.mapreduceReduceShuffleInputBufferPercent));
        sb.append(',');
        sb.append("mapreduceReduceShuffleMergePercent");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleMergePercent == null)?"<null>":this.mapreduceReduceShuffleMergePercent));
        sb.append(',');
        sb.append("mapreduceReduceShuffleParallelcopies");
        sb.append('=');
        sb.append(((this.mapreduceReduceShuffleParallelcopies == null)?"<null>":this.mapreduceReduceShuffleParallelcopies));
        sb.append(',');
        sb.append("mapreduceReduceSpeculative");
        sb.append('=');
        sb.append(((this.mapreduceReduceSpeculative == null)?"<null>":this.mapreduceReduceSpeculative));
        sb.append(',');
        sb.append("mapreduceShufflePort");
        sb.append('=');
        sb.append(((this.mapreduceShufflePort == null)?"<null>":this.mapreduceShufflePort));
        sb.append(',');
        sb.append("mapreduceTaskIoSortFactor");
        sb.append('=');
        sb.append(((this.mapreduceTaskIoSortFactor == null)?"<null>":this.mapreduceTaskIoSortFactor));
        sb.append(',');
        sb.append("mapreduceTaskIoSortMb");
        sb.append('=');
        sb.append(((this.mapreduceTaskIoSortMb == null)?"<null>":this.mapreduceTaskIoSortMb));
        sb.append(',');
        sb.append("mapreduceTaskTimeout");
        sb.append('=');
        sb.append(((this.mapreduceTaskTimeout == null)?"<null>":this.mapreduceTaskTimeout));
        sb.append(',');
        sb.append("yarnAppMapreduceAmAdminCommandOpts");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmAdminCommandOpts == null)?"<null>":this.yarnAppMapreduceAmAdminCommandOpts));
        sb.append(',');
        sb.append("yarnAppMapreduceAmCommandOpts");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmCommandOpts == null)?"<null>":this.yarnAppMapreduceAmCommandOpts));
        sb.append(',');
        sb.append("yarnAppMapreduceAmLogLevel");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmLogLevel == null)?"<null>":this.yarnAppMapreduceAmLogLevel));
        sb.append(',');
        sb.append("yarnAppMapreduceAmResourceMb");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmResourceMb == null)?"<null>":this.yarnAppMapreduceAmResourceMb));
        sb.append(',');
        sb.append("yarnAppMapreduceAmStagingDir");
        sb.append('=');
        sb.append(((this.yarnAppMapreduceAmStagingDir == null)?"<null>":this.yarnAppMapreduceAmStagingDir));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.mapredUserNofileLimit == null)? 0 :this.mapredUserNofileLimit.hashCode()));
        result = ((result* 31)+((this.mapreduceOutputFileoutputformatCompressType == null)? 0 :this.mapreduceOutputFileoutputformatCompressType.hashCode()));
        result = ((result* 31)+((this.mapredLocalDir == null)? 0 :this.mapredLocalDir.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskTimeout == null)? 0 :this.mapreduceTaskTimeout.hashCode()));
        result = ((result* 31)+((this.mapreduceJobEmitTimelineData == null)? 0 :this.mapreduceJobEmitTimelineData.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryIntervalMs == null)? 0 :this.mapreduceReduceShuffleFetchRetryIntervalMs.hashCode()));
        result = ((result* 31)+((this.mapreduceOutputFileoutputformatCompress == null)? 0 :this.mapreduceOutputFileoutputformatCompress.hashCode()));
        result = ((result* 31)+((this.mapreduceMapMemoryMb == null)? 0 :this.mapreduceMapMemoryMb.hashCode()));
        result = ((result* 31)+((this.mapreduceAmMaxAttempts == null)? 0 :this.mapreduceAmMaxAttempts.hashCode()));
        result = ((result* 31)+((this.mapreduceClusterAdministrators == null)? 0 :this.mapreduceClusterAdministrators.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminUserEnv == null)? 0 :this.mapreduceAdminUserEnv.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskIoSortMb == null)? 0 :this.mapreduceTaskIoSortMb.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceJavaOpts == null)? 0 :this.mapreduceReduceJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleMergePercent == null)? 0 :this.mapreduceReduceShuffleMergePercent.hashCode()));
        result = ((result* 31)+((this.mapreduceApplicationClasspath == null)? 0 :this.mapreduceApplicationClasspath.hashCode()));
        result = ((result* 31)+((this.mapreduceJobAclModifyJob == null)? 0 :this.mapreduceJobAclModifyJob.hashCode()));
        result = ((result* 31)+((this.mapreduceMapSortSpillPercent == null)? 0 :this.mapreduceMapSortSpillPercent.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryEnable == null)? 0 :this.mapreduceJobhistoryRecoveryEnable.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmStagingDir == null)? 0 :this.yarnAppMapreduceAmStagingDir.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryAddress == null)? 0 :this.mapreduceJobhistoryAddress.hashCode()));
        result = ((result* 31)+((this.mapreduceMapOutputCompress == null)? 0 :this.mapreduceMapOutputCompress.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmCommandOpts == null)? 0 :this.yarnAppMapreduceAmCommandOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceJobReduceSlowstartCompletedmaps == null)? 0 :this.mapreduceJobReduceSlowstartCompletedmaps.hashCode()));
        result = ((result* 31)+((this.mapredPidDirPrefix == null)? 0 :this.mapredPidDirPrefix.hashCode()));
        result = ((result* 31)+((this.mapreduceJobAclViewJob == null)? 0 :this.mapreduceJobAclViewJob.hashCode()));
        result = ((result* 31)+((this.mapreduceMapJavaOpts == null)? 0 :this.mapreduceMapJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceSpeculative == null)? 0 :this.mapreduceReduceSpeculative.hashCode()));
        result = ((result* 31)+((this.mapreduceClusterAclsEnabled == null)? 0 :this.mapreduceClusterAclsEnabled.hashCode()));
        result = ((result* 31)+((this.mapreduceJobCountersMax == null)? 0 :this.mapreduceJobCountersMax.hashCode()));
        result = ((result* 31)+((this.mapreduceApplicationFrameworkPath == null)? 0 :this.mapreduceApplicationFrameworkPath.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminReduceChildJavaOpts == null)? 0 :this.mapreduceAdminReduceChildJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceMapSpeculative == null)? 0 :this.mapreduceMapSpeculative.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryIntermediateDoneDir == null)? 0 :this.mapreduceJobhistoryIntermediateDoneDir.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryTimeoutMs == null)? 0 :this.mapreduceReduceShuffleFetchRetryTimeoutMs.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmLogLevel == null)? 0 :this.yarnAppMapreduceAmLogLevel.hashCode()));
        result = ((result* 31)+((this.content == null)? 0 :this.content.hashCode()));
        result = ((result* 31)+((this.mapredUserNprocLimit == null)? 0 :this.mapredUserNprocLimit.hashCode()));
        result = ((result* 31)+((this.mapreduceTaskIoSortFactor == null)? 0 :this.mapreduceTaskIoSortFactor.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleFetchRetryEnabled == null)? 0 :this.mapreduceReduceShuffleFetchRetryEnabled.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryAdminAcl == null)? 0 :this.mapreduceJobhistoryAdminAcl.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryHttpPolicy == null)? 0 :this.mapreduceJobhistoryHttpPolicy.hashCode()));
        result = ((result* 31)+((this.mapreduceAdminMapChildJavaOpts == null)? 0 :this.mapreduceAdminMapChildJavaOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceMapLogLevel == null)? 0 :this.mapreduceMapLogLevel.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceMemoryMb == null)? 0 :this.mapreduceReduceMemoryMb.hashCode()));
        result = ((result* 31)+((this.mapredLogDirPrefix == null)? 0 :this.mapredLogDirPrefix.hashCode()));
        result = ((result* 31)+((this.mapredUser == null)? 0 :this.mapredUser.hashCode()));
        result = ((result* 31)+((this.mapreduceJobQueuename == null)? 0 :this.mapreduceJobQueuename.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryDoneDir == null)? 0 :this.mapreduceJobhistoryDoneDir.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryBindHost == null)? 0 :this.mapreduceJobhistoryBindHost.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryWebappAddress == null)? 0 :this.mapreduceJobhistoryWebappAddress.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmResourceMb == null)? 0 :this.yarnAppMapreduceAmResourceMb.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceInputBufferPercent == null)? 0 :this.mapreduceReduceInputBufferPercent.hashCode()));
        result = ((result* 31)+((this.jobhistoryHeapsize == null)? 0 :this.jobhistoryHeapsize.hashCode()));
        result = ((result* 31)+((this.mapreduceFrameworkName == null)? 0 :this.mapreduceFrameworkName.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleParallelcopies == null)? 0 :this.mapreduceReduceShuffleParallelcopies.hashCode()));
        result = ((result* 31)+((this.yarnAppMapreduceAmAdminCommandOpts == null)? 0 :this.yarnAppMapreduceAmAdminCommandOpts.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceShuffleInputBufferPercent == null)? 0 :this.mapreduceReduceShuffleInputBufferPercent.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryStoreClass == null)? 0 :this.mapreduceJobhistoryRecoveryStoreClass.hashCode()));
        result = ((result* 31)+((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == null)? 0 :this.mapreduceJobhistoryRecoveryStoreLeveldbPath.hashCode()));
        result = ((result* 31)+((this.mapreduceReduceLogLevel == null)? 0 :this.mapreduceReduceLogLevel.hashCode()));
        result = ((result* 31)+((this.mapreduceShufflePort == null)? 0 :this.mapreduceShufflePort.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Properties) == false) {
            return false;
        }
        Properties rhs = ((Properties) other);
        return ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((this.mapredUserNofileLimit == rhs.mapredUserNofileLimit)||((this.mapredUserNofileLimit!= null)&&this.mapredUserNofileLimit.equals(rhs.mapredUserNofileLimit)))&&((this.mapreduceOutputFileoutputformatCompressType == rhs.mapreduceOutputFileoutputformatCompressType)||((this.mapreduceOutputFileoutputformatCompressType!= null)&&this.mapreduceOutputFileoutputformatCompressType.equals(rhs.mapreduceOutputFileoutputformatCompressType))))&&((this.mapredLocalDir == rhs.mapredLocalDir)||((this.mapredLocalDir!= null)&&this.mapredLocalDir.equals(rhs.mapredLocalDir))))&&((this.mapreduceTaskTimeout == rhs.mapreduceTaskTimeout)||((this.mapreduceTaskTimeout!= null)&&this.mapreduceTaskTimeout.equals(rhs.mapreduceTaskTimeout))))&&((this.mapreduceJobEmitTimelineData == rhs.mapreduceJobEmitTimelineData)||((this.mapreduceJobEmitTimelineData!= null)&&this.mapreduceJobEmitTimelineData.equals(rhs.mapreduceJobEmitTimelineData))))&&((this.mapreduceReduceShuffleFetchRetryIntervalMs == rhs.mapreduceReduceShuffleFetchRetryIntervalMs)||((this.mapreduceReduceShuffleFetchRetryIntervalMs!= null)&&this.mapreduceReduceShuffleFetchRetryIntervalMs.equals(rhs.mapreduceReduceShuffleFetchRetryIntervalMs))))&&((this.mapreduceOutputFileoutputformatCompress == rhs.mapreduceOutputFileoutputformatCompress)||((this.mapreduceOutputFileoutputformatCompress!= null)&&this.mapreduceOutputFileoutputformatCompress.equals(rhs.mapreduceOutputFileoutputformatCompress))))&&((this.mapreduceMapMemoryMb == rhs.mapreduceMapMemoryMb)||((this.mapreduceMapMemoryMb!= null)&&this.mapreduceMapMemoryMb.equals(rhs.mapreduceMapMemoryMb))))&&((this.mapreduceAmMaxAttempts == rhs.mapreduceAmMaxAttempts)||((this.mapreduceAmMaxAttempts!= null)&&this.mapreduceAmMaxAttempts.equals(rhs.mapreduceAmMaxAttempts))))&&((this.mapreduceClusterAdministrators == rhs.mapreduceClusterAdministrators)||((this.mapreduceClusterAdministrators!= null)&&this.mapreduceClusterAdministrators.equals(rhs.mapreduceClusterAdministrators))))&&((this.mapreduceAdminUserEnv == rhs.mapreduceAdminUserEnv)||((this.mapreduceAdminUserEnv!= null)&&this.mapreduceAdminUserEnv.equals(rhs.mapreduceAdminUserEnv))))&&((this.mapreduceTaskIoSortMb == rhs.mapreduceTaskIoSortMb)||((this.mapreduceTaskIoSortMb!= null)&&this.mapreduceTaskIoSortMb.equals(rhs.mapreduceTaskIoSortMb))))&&((this.mapreduceReduceJavaOpts == rhs.mapreduceReduceJavaOpts)||((this.mapreduceReduceJavaOpts!= null)&&this.mapreduceReduceJavaOpts.equals(rhs.mapreduceReduceJavaOpts))))&&((this.mapreduceReduceShuffleMergePercent == rhs.mapreduceReduceShuffleMergePercent)||((this.mapreduceReduceShuffleMergePercent!= null)&&this.mapreduceReduceShuffleMergePercent.equals(rhs.mapreduceReduceShuffleMergePercent))))&&((this.mapreduceApplicationClasspath == rhs.mapreduceApplicationClasspath)||((this.mapreduceApplicationClasspath!= null)&&this.mapreduceApplicationClasspath.equals(rhs.mapreduceApplicationClasspath))))&&((this.mapreduceJobAclModifyJob == rhs.mapreduceJobAclModifyJob)||((this.mapreduceJobAclModifyJob!= null)&&this.mapreduceJobAclModifyJob.equals(rhs.mapreduceJobAclModifyJob))))&&((this.mapreduceMapSortSpillPercent == rhs.mapreduceMapSortSpillPercent)||((this.mapreduceMapSortSpillPercent!= null)&&this.mapreduceMapSortSpillPercent.equals(rhs.mapreduceMapSortSpillPercent))))&&((this.mapreduceJobhistoryRecoveryEnable == rhs.mapreduceJobhistoryRecoveryEnable)||((this.mapreduceJobhistoryRecoveryEnable!= null)&&this.mapreduceJobhistoryRecoveryEnable.equals(rhs.mapreduceJobhistoryRecoveryEnable))))&&((this.yarnAppMapreduceAmStagingDir == rhs.yarnAppMapreduceAmStagingDir)||((this.yarnAppMapreduceAmStagingDir!= null)&&this.yarnAppMapreduceAmStagingDir.equals(rhs.yarnAppMapreduceAmStagingDir))))&&((this.mapreduceJobhistoryAddress == rhs.mapreduceJobhistoryAddress)||((this.mapreduceJobhistoryAddress!= null)&&this.mapreduceJobhistoryAddress.equals(rhs.mapreduceJobhistoryAddress))))&&((this.mapreduceMapOutputCompress == rhs.mapreduceMapOutputCompress)||((this.mapreduceMapOutputCompress!= null)&&this.mapreduceMapOutputCompress.equals(rhs.mapreduceMapOutputCompress))))&&((this.yarnAppMapreduceAmCommandOpts == rhs.yarnAppMapreduceAmCommandOpts)||((this.yarnAppMapreduceAmCommandOpts!= null)&&this.yarnAppMapreduceAmCommandOpts.equals(rhs.yarnAppMapreduceAmCommandOpts))))&&((this.mapreduceJobReduceSlowstartCompletedmaps == rhs.mapreduceJobReduceSlowstartCompletedmaps)||((this.mapreduceJobReduceSlowstartCompletedmaps!= null)&&this.mapreduceJobReduceSlowstartCompletedmaps.equals(rhs.mapreduceJobReduceSlowstartCompletedmaps))))&&((this.mapredPidDirPrefix == rhs.mapredPidDirPrefix)||((this.mapredPidDirPrefix!= null)&&this.mapredPidDirPrefix.equals(rhs.mapredPidDirPrefix))))&&((this.mapreduceJobAclViewJob == rhs.mapreduceJobAclViewJob)||((this.mapreduceJobAclViewJob!= null)&&this.mapreduceJobAclViewJob.equals(rhs.mapreduceJobAclViewJob))))&&((this.mapreduceMapJavaOpts == rhs.mapreduceMapJavaOpts)||((this.mapreduceMapJavaOpts!= null)&&this.mapreduceMapJavaOpts.equals(rhs.mapreduceMapJavaOpts))))&&((this.mapreduceReduceSpeculative == rhs.mapreduceReduceSpeculative)||((this.mapreduceReduceSpeculative!= null)&&this.mapreduceReduceSpeculative.equals(rhs.mapreduceReduceSpeculative))))&&((this.mapreduceClusterAclsEnabled == rhs.mapreduceClusterAclsEnabled)||((this.mapreduceClusterAclsEnabled!= null)&&this.mapreduceClusterAclsEnabled.equals(rhs.mapreduceClusterAclsEnabled))))&&((this.mapreduceJobCountersMax == rhs.mapreduceJobCountersMax)||((this.mapreduceJobCountersMax!= null)&&this.mapreduceJobCountersMax.equals(rhs.mapreduceJobCountersMax))))&&((this.mapreduceApplicationFrameworkPath == rhs.mapreduceApplicationFrameworkPath)||((this.mapreduceApplicationFrameworkPath!= null)&&this.mapreduceApplicationFrameworkPath.equals(rhs.mapreduceApplicationFrameworkPath))))&&((this.mapreduceAdminReduceChildJavaOpts == rhs.mapreduceAdminReduceChildJavaOpts)||((this.mapreduceAdminReduceChildJavaOpts!= null)&&this.mapreduceAdminReduceChildJavaOpts.equals(rhs.mapreduceAdminReduceChildJavaOpts))))&&((this.mapreduceMapSpeculative == rhs.mapreduceMapSpeculative)||((this.mapreduceMapSpeculative!= null)&&this.mapreduceMapSpeculative.equals(rhs.mapreduceMapSpeculative))))&&((this.mapreduceJobhistoryIntermediateDoneDir == rhs.mapreduceJobhistoryIntermediateDoneDir)||((this.mapreduceJobhistoryIntermediateDoneDir!= null)&&this.mapreduceJobhistoryIntermediateDoneDir.equals(rhs.mapreduceJobhistoryIntermediateDoneDir))))&&((this.mapreduceReduceShuffleFetchRetryTimeoutMs == rhs.mapreduceReduceShuffleFetchRetryTimeoutMs)||((this.mapreduceReduceShuffleFetchRetryTimeoutMs!= null)&&this.mapreduceReduceShuffleFetchRetryTimeoutMs.equals(rhs.mapreduceReduceShuffleFetchRetryTimeoutMs))))&&((this.yarnAppMapreduceAmLogLevel == rhs.yarnAppMapreduceAmLogLevel)||((this.yarnAppMapreduceAmLogLevel!= null)&&this.yarnAppMapreduceAmLogLevel.equals(rhs.yarnAppMapreduceAmLogLevel))))&&((this.content == rhs.content)||((this.content!= null)&&this.content.equals(rhs.content))))&&((this.mapredUserNprocLimit == rhs.mapredUserNprocLimit)||((this.mapredUserNprocLimit!= null)&&this.mapredUserNprocLimit.equals(rhs.mapredUserNprocLimit))))&&((this.mapreduceTaskIoSortFactor == rhs.mapreduceTaskIoSortFactor)||((this.mapreduceTaskIoSortFactor!= null)&&this.mapreduceTaskIoSortFactor.equals(rhs.mapreduceTaskIoSortFactor))))&&((this.mapreduceReduceShuffleFetchRetryEnabled == rhs.mapreduceReduceShuffleFetchRetryEnabled)||((this.mapreduceReduceShuffleFetchRetryEnabled!= null)&&this.mapreduceReduceShuffleFetchRetryEnabled.equals(rhs.mapreduceReduceShuffleFetchRetryEnabled))))&&((this.mapreduceJobhistoryAdminAcl == rhs.mapreduceJobhistoryAdminAcl)||((this.mapreduceJobhistoryAdminAcl!= null)&&this.mapreduceJobhistoryAdminAcl.equals(rhs.mapreduceJobhistoryAdminAcl))))&&((this.mapreduceJobhistoryHttpPolicy == rhs.mapreduceJobhistoryHttpPolicy)||((this.mapreduceJobhistoryHttpPolicy!= null)&&this.mapreduceJobhistoryHttpPolicy.equals(rhs.mapreduceJobhistoryHttpPolicy))))&&((this.mapreduceAdminMapChildJavaOpts == rhs.mapreduceAdminMapChildJavaOpts)||((this.mapreduceAdminMapChildJavaOpts!= null)&&this.mapreduceAdminMapChildJavaOpts.equals(rhs.mapreduceAdminMapChildJavaOpts))))&&((this.mapreduceMapLogLevel == rhs.mapreduceMapLogLevel)||((this.mapreduceMapLogLevel!= null)&&this.mapreduceMapLogLevel.equals(rhs.mapreduceMapLogLevel))))&&((this.mapreduceReduceMemoryMb == rhs.mapreduceReduceMemoryMb)||((this.mapreduceReduceMemoryMb!= null)&&this.mapreduceReduceMemoryMb.equals(rhs.mapreduceReduceMemoryMb))))&&((this.mapredLogDirPrefix == rhs.mapredLogDirPrefix)||((this.mapredLogDirPrefix!= null)&&this.mapredLogDirPrefix.equals(rhs.mapredLogDirPrefix))))&&((this.mapredUser == rhs.mapredUser)||((this.mapredUser!= null)&&this.mapredUser.equals(rhs.mapredUser))))&&((this.mapreduceJobQueuename == rhs.mapreduceJobQueuename)||((this.mapreduceJobQueuename!= null)&&this.mapreduceJobQueuename.equals(rhs.mapreduceJobQueuename))))&&((this.mapreduceJobhistoryDoneDir == rhs.mapreduceJobhistoryDoneDir)||((this.mapreduceJobhistoryDoneDir!= null)&&this.mapreduceJobhistoryDoneDir.equals(rhs.mapreduceJobhistoryDoneDir))))&&((this.mapreduceJobhistoryBindHost == rhs.mapreduceJobhistoryBindHost)||((this.mapreduceJobhistoryBindHost!= null)&&this.mapreduceJobhistoryBindHost.equals(rhs.mapreduceJobhistoryBindHost))))&&((this.mapreduceJobhistoryWebappAddress == rhs.mapreduceJobhistoryWebappAddress)||((this.mapreduceJobhistoryWebappAddress!= null)&&this.mapreduceJobhistoryWebappAddress.equals(rhs.mapreduceJobhistoryWebappAddress))))&&((this.yarnAppMapreduceAmResourceMb == rhs.yarnAppMapreduceAmResourceMb)||((this.yarnAppMapreduceAmResourceMb!= null)&&this.yarnAppMapreduceAmResourceMb.equals(rhs.yarnAppMapreduceAmResourceMb))))&&((this.mapreduceReduceInputBufferPercent == rhs.mapreduceReduceInputBufferPercent)||((this.mapreduceReduceInputBufferPercent!= null)&&this.mapreduceReduceInputBufferPercent.equals(rhs.mapreduceReduceInputBufferPercent))))&&((this.jobhistoryHeapsize == rhs.jobhistoryHeapsize)||((this.jobhistoryHeapsize!= null)&&this.jobhistoryHeapsize.equals(rhs.jobhistoryHeapsize))))&&((this.mapreduceFrameworkName == rhs.mapreduceFrameworkName)||((this.mapreduceFrameworkName!= null)&&this.mapreduceFrameworkName.equals(rhs.mapreduceFrameworkName))))&&((this.mapreduceReduceShuffleParallelcopies == rhs.mapreduceReduceShuffleParallelcopies)||((this.mapreduceReduceShuffleParallelcopies!= null)&&this.mapreduceReduceShuffleParallelcopies.equals(rhs.mapreduceReduceShuffleParallelcopies))))&&((this.yarnAppMapreduceAmAdminCommandOpts == rhs.yarnAppMapreduceAmAdminCommandOpts)||((this.yarnAppMapreduceAmAdminCommandOpts!= null)&&this.yarnAppMapreduceAmAdminCommandOpts.equals(rhs.yarnAppMapreduceAmAdminCommandOpts))))&&((this.mapreduceReduceShuffleInputBufferPercent == rhs.mapreduceReduceShuffleInputBufferPercent)||((this.mapreduceReduceShuffleInputBufferPercent!= null)&&this.mapreduceReduceShuffleInputBufferPercent.equals(rhs.mapreduceReduceShuffleInputBufferPercent))))&&((this.mapreduceJobhistoryRecoveryStoreClass == rhs.mapreduceJobhistoryRecoveryStoreClass)||((this.mapreduceJobhistoryRecoveryStoreClass!= null)&&this.mapreduceJobhistoryRecoveryStoreClass.equals(rhs.mapreduceJobhistoryRecoveryStoreClass))))&&((this.mapreduceJobhistoryRecoveryStoreLeveldbPath == rhs.mapreduceJobhistoryRecoveryStoreLeveldbPath)||((this.mapreduceJobhistoryRecoveryStoreLeveldbPath!= null)&&this.mapreduceJobhistoryRecoveryStoreLeveldbPath.equals(rhs.mapreduceJobhistoryRecoveryStoreLeveldbPath))))&&((this.mapreduceReduceLogLevel == rhs.mapreduceReduceLogLevel)||((this.mapreduceReduceLogLevel!= null)&&this.mapreduceReduceLogLevel.equals(rhs.mapreduceReduceLogLevel))))&&((this.mapreduceShufflePort == rhs.mapreduceShufflePort)||((this.mapreduceShufflePort!= null)&&this.mapreduceShufflePort.equals(rhs.mapreduceShufflePort))));
    }

}
